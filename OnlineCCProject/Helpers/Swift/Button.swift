//
//  Button.swift
//  OnlineCCProject
//
//  Created by Idrees on 2017/10/23.
//  Copyright © 2017 CheapSender. All rights reserved.
//

import UIKit

@IBDesignable
class Button: UIButton {

    @IBInspectable var borderColor: UIColor? {
        didSet {
            layer.borderColor = borderColor?.cgColor
        }
    }
    
    @IBInspectable var borderRadious: CGFloat = 0 {
        didSet {
            layer.cornerRadius = borderRadious
        }
    }
    
    @IBInspectable var borderWidth: CGFloat = 0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }


}
