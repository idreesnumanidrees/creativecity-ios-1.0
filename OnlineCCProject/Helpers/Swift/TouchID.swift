//
//  TouchID.swift
//
//  Created by Gabriel Theodoropoulos.
//  Copyright (c) 2014 Gabriel Theodoropoulos. All rights reserved.
//
//  E-mail:     gabrielth.devel@gmail.com
//  Website:    http://gtiapps.com
//  Google+:    http://plus.google.com/+GabrielTheodoropoulos
//

import UIKit
import LocalAuthentication


enum TouchIDErrorCode {
    case TouchID_AuthenticationFailed   // When the authentication fails (e.g. wrong finger)
    case TouchID_PasscodeNotSet         // No passcode was set in the device's Settings
    case TouchID_CanceledByTheSystem    // The system cancels the authentication because another app became active
    case TouchID_TouchIDNotAvailable    // The TouchID authentication mechanism is not supported by the device
    case TouchID_TouchIDNotEnrolled     // The TouchID authentication is supported but no finger was enrolled
    case TouchID_CanceledByTheUser      // The user cancels the authentication
    case TouchID_UserFallback           // The user selects to fall back to a custom authentication method
}


protocol TouchIDDelegate{
    
    func touchIDAuthenticationWasSuccessful();
    
    func touchIDAuthenticationFailed( errorCode: TouchIDErrorCode);
}

class TouchID: NSObject {
   
    var delegate : TouchIDDelegate!
    
    var touchIDReasonString : String?
    
    func touchIDAuthentication(){
        // Get the local authentication context.
        let context = LAContext()
        
        // Declare a NSError variable.
        var error: NSError?
        
        // Set the reason string that will appear on the authentication alert.
        var reasonString = "Authentication is needed to access this application."
        if let reason = touchIDReasonString {
            reasonString = reason
        }
        
        // Declare and initialize a custom error variable to represent a LocalAuthentication error (LAError).
        var customErrorCode : TouchIDErrorCode?
        
        if context.canEvaluatePolicy(LAPolicy.deviceOwnerAuthenticationWithBiometrics, error:&error) {
            
            context.evaluatePolicy(LAPolicy.deviceOwnerAuthenticationWithBiometrics,
                                   localizedReason: reasonString,
                                   reply: {(wasSuccessful, Error) in
                                    
                                    if wasSuccessful {
                                        // This is the case of a successful authentication.
                                        OperationQueue.main.addOperation({ () -> Void in
                                            self.delegate.touchIDAuthenticationWasSuccessful()
                                        })
                                    }
                                    else {
   
                                    }
            })
        }
        else{
            // The policy cannot be evaluated.
            switch error!.code{
                
            case LAError.touchIDNotEnrolled.rawValue:
                customErrorCode = .TouchID_TouchIDNotEnrolled
                
            case LAError.passcodeNotSet.rawValue:
                customErrorCode = .TouchID_PasscodeNotSet
                
            default:
                customErrorCode = .TouchID_TouchIDNotAvailable
            }
            
            delegate.touchIDAuthenticationFailed(errorCode: customErrorCode!)
        }
    }
}
