//
//  Label.swift
//  OnlineCCProject
//
//  Created by apple on 5/20/18.
//  Copyright © 2018 CheapSender. All rights reserved.
//

import UIKit

@IBDesignable
class Label: UILabel {
    
    override public func drawText(in rect: CGRect) {
        let insets = UIEdgeInsets.init(top: 15, left: 15, bottom: 15, right: 15)
        super.drawText(in: UIEdgeInsetsInsetRect(rect, insets))
    }

    @IBInspectable var borderColor: UIColor? {
        didSet {
            layer.borderColor = borderColor?.cgColor
        }
    }
    
    @IBInspectable var borderRadious: CGFloat = 0 {
        didSet {
            layer.cornerRadius = borderRadious
        }
    }
    
    @IBInspectable var borderWidth: CGFloat = 0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
}
