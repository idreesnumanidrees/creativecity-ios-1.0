//
//  AppController.swift
//  OnlineCCProject
//
//  Created by Idrees on 2017/10/11.
//  Copyright © 2017 CheapSender. All rights reserved.
//

import UIKit

class AppController: NSObject {
    
    var appWindow:UIWindow?
    var mainStroyBoard:UIStoryboard?
    var navController:UINavigationController?
    var tabbarController:UITabBarController?
    
    //StartUp window and storyboard setup
    func initWithWindow(_ window:UIWindow) {
        
        appWindow = window
        getStoryBoard()
    }
    
    func loadApplication() {
        
        getStoryBoard()
    }
    
    func getStoryBoard() {
        
        mainStroyBoard = getMainStoryBoard()
    }
    
    func getMainStoryBoard() -> UIStoryboard {
        
//        let userInterface = UIDevice.current.userInterfaceIdiom
        let storyboard = UIStoryboard(name : "Main", bundle:nil)
        
//        if userInterface == .pad {
//            storyboard = UIStoryboard(name : "Main_IPad", bundle:nil)
//        }
        return storyboard;
    }
    
    //Navigation Controller setup
    func navigationController() {
        
        navController = getNavigationController()
        setRootViewControllerViewForWindow(navController!)
    }
    
    func getNavigationController() -> UINavigationController {
        
        let navigationController:UINavigationController = getMainStoryBoard().instantiateViewController(withIdentifier: "MainNavigationController") as! UINavigationController
        return navigationController
    }

    func setRootViewControllerViewForWindow(_ navController: UINavigationController) {
        
        appWindow?.rootViewController = navController
        appWindow?.isHidden = false
    }
    
    //Load Tabbar
    func loadTabbar() {
        
        tabbarController = mainStroyBoard?.instantiateViewController(withIdentifier: "tabBarController") as? UITabBarController
        tabbarController?.selectedIndex = 0
        navController?.pushViewController(tabbarController!, animated: true)
    }
    
    //Controller methods
    func loadSplashView() {
        
        let splashViewController:SplashViewController = mainStroyBoard?.instantiateViewController(withIdentifier: "SplashViewController") as! SplashViewController
        navController?.pushViewController(splashViewController, animated: true)
    }
    
    func loadStartUpView() {
        
        let startupView:StartupView = mainStroyBoard?.instantiateViewController(withIdentifier: "StartupView") as! StartupView
        navController?.pushViewController(startupView, animated: true)
    }
    
    func loadLoginView() {
        
        let loginView:LoginView = mainStroyBoard?.instantiateViewController(withIdentifier: "LoginView") as! LoginView
        navController?.pushViewController(loginView, animated: true)
    }
    
    func loadSignUpStep1View() {
        
        let signUpStep1:SignUpStep1 = mainStroyBoard?.instantiateViewController(withIdentifier: "SignUpStep1") as! SignUpStep1
        navController?.pushViewController(signUpStep1, animated: true)
    }
    
    func loadSignUpStep2View(response:Dictionary<String, AnyObject>?) {
        
        let signUpStep2:SignUpStep2 = mainStroyBoard?.instantiateViewController(withIdentifier: "SignUpStep2") as! SignUpStep2
        signUpStep2.dictionaryData = response!
        navController?.pushViewController(signUpStep2, animated: true)
    }
    
    func loadRequestFprEmailView(response:Dictionary<String, AnyObject>?) {
        
        let requestForEmailView:RequestForEmailView = mainStroyBoard?.instantiateViewController(withIdentifier: "RequestForEmailView") as! RequestForEmailView
        requestForEmailView.dictionaryData = response!
        navController?.pushViewController(requestForEmailView, animated: true)
    }
    
    func loadVerifyEmailView(response:Dictionary<String, AnyObject>?) {
        
        let verifyEmailView:VerifyEmailView = mainStroyBoard?.instantiateViewController(withIdentifier: "VerifyEmailView") as! VerifyEmailView
        verifyEmailView.dictionaryData = response!
        navController?.pushViewController(verifyEmailView, animated: true)
    }
    
    func loadThankForSigning(response:Dictionary<String, AnyObject>?) {
        
        let thanksSignUpView:ThanksSignUpView = mainStroyBoard?.instantiateViewController(withIdentifier: "ThanksSignUpView") as! ThanksSignUpView
        thanksSignUpView.responseDict = response!
        navController?.pushViewController(thanksSignUpView, animated: true)
    }
    
    func loadCheckCompanyAvailView() {
        
        let companyAvailView:CompanyAvailabilityView = mainStroyBoard?.instantiateViewController(withIdentifier: "CompanyAvailabilityView") as! CompanyAvailabilityView
        navController?.pushViewController(companyAvailView, animated: true)
    }
    
    func loadCompanyRequestView() {
        
        let companyRequestView:CompanyRequestView = mainStroyBoard?.instantiateViewController(withIdentifier: "CompanyRequestView") as! CompanyRequestView
        navController?.pushViewController(companyRequestView, animated: true)
    }
    
    func loadPersonalInfoView() {
        
        let personalInfoView:PersonalInfoView = mainStroyBoard?.instantiateViewController(withIdentifier: "PersonalInfoView") as! PersonalInfoView
        navController?.pushViewController(personalInfoView, animated: true)
    }
    
    func loadHomeCompanyView() {
        
        let homeCompanyView:HomeCompanyView = mainStroyBoard?.instantiateViewController(withIdentifier: "HomeCompanyView") as! HomeCompanyView
        navController?.pushViewController(homeCompanyView, animated: true)
    }
    
    func loadOtherView() {
        
        let othersHomeView:OthersHomeView = mainStroyBoard?.instantiateViewController(withIdentifier: "OthersHomeView") as! OthersHomeView
        navController?.pushViewController(othersHomeView, animated: true)
    }
    
    func loadVisaHomeView(companyID: String?) {
        
        let visaHomeView:VisaHomeView = mainStroyBoard?.instantiateViewController(withIdentifier: "VisaHomeView") as! VisaHomeView
        visaHomeView.companyId = companyID
        navController?.pushViewController(visaHomeView, animated: true)
    }
    
    func loadVisaApplicationView() {
        
        let visaApplicationList:VisaAppsListView = mainStroyBoard?.instantiateViewController(withIdentifier: "VisaAppsListView") as! VisaAppsListView
        navController?.pushViewController(visaApplicationList, animated: true)
    }
    
    func loadPDFViewerView(folderName:String?, fileName:String?, pdfUrl:String?, navTitle:String?) {
        
        let pdfViewerView:PDFViewerView = mainStroyBoard?.instantiateViewController(withIdentifier: "PDFViewerView") as! PDFViewerView
        pdfViewerView.folderName = folderName
        pdfViewerView.fileName = fileName
        pdfViewerView.documentPathToLoad = pdfUrl
        pdfViewerView.navTitle = navTitle
        navController?.pushViewController(pdfViewerView, animated: true)
    }
    
    //MARK: Company views

    func loadPCompanyLicenseView(companyID: String?) {
        
        let companyViewLicense:CompanyViewLicense = mainStroyBoard?.instantiateViewController(withIdentifier: "CompanyViewLicense") as! CompanyViewLicense
        companyViewLicense.companyId  = companyID
        navController?.pushViewController(companyViewLicense, animated: true)
    }
    
    func loadCompanyStatuesView(companyID: String?) {
        
        let companyViewLicense:CompanyStatusesView = mainStroyBoard?.instantiateViewController(withIdentifier: "CompanyStatusesView") as! CompanyStatusesView
        companyViewLicense.companyId  = companyID
        navController?.pushViewController(companyViewLicense, animated: true)
    }
    
    func loadCompanyShareCertsView(companyID: String?) {
        
        let companyViewLicense:CompanyShareCertView = mainStroyBoard?.instantiateViewController(withIdentifier: "CompanyShareCertView") as! CompanyShareCertView
        companyViewLicense.companyId  = companyID
        navController?.pushViewController(companyViewLicense, animated: true)
    }
    
    func loadCompanyIncorporationView(companyID: String?) {
        
        let companyIncorporationView:CompanyIncorporationView = mainStroyBoard?.instantiateViewController(withIdentifier: "CompanyIncorporationView") as! CompanyIncorporationView
        companyIncorporationView.companyId  = companyID
        navController?.pushViewController(companyIncorporationView, animated: true)
    }
    
    func loadCompanyArticlesView(companyID: String?) {
        
        let companyArticalesView:CompanyArticalesView = mainStroyBoard?.instantiateViewController(withIdentifier: "CompanyArticalesView") as! CompanyArticalesView
        companyArticalesView.companyId  = companyID
        navController?.pushViewController(companyArticalesView, animated: true)
    }
    
    func loadCompanyAddendumsView(companyID: String?) {
        
        let companyAddendumsView:CompanyAddendumsView = mainStroyBoard?.instantiateViewController(withIdentifier: "CompanyAddendumsView") as! CompanyAddendumsView
        companyAddendumsView.companyId  = companyID
        navController?.pushViewController(companyAddendumsView, animated: true)
    }
    
    func loadOfficeLeaseAgreementView(companyID: String?) {
        
        let officeLeaseAgreement:OfficeLeaseAgreement = mainStroyBoard?.instantiateViewController(withIdentifier: "OfficeLeaseAgreement") as! OfficeLeaseAgreement
        officeLeaseAgreement.companyId  = companyID
        navController?.pushViewController(officeLeaseAgreement, animated: true)
    }
    
    //MARK: Version(1.3)
    func loadAllCompApps(companyID: String?) {
        
        let companyApplicationsView:CompanyApplicationsView = mainStroyBoard?.instantiateViewController(withIdentifier: "CompanyApplicationsView") as! CompanyApplicationsView
        companyApplicationsView.companyId  = companyID
        navController?.pushViewController(companyApplicationsView, animated: true)
    }
    
    //Renewal
    func loadRenewalCompStep1() {
        
        let renewalCompStep1View:RenewalCompStep1View = mainStroyBoard?.instantiateViewController(withIdentifier: "RenewalCompStep1View") as! RenewalCompStep1View
        navController?.pushViewController(renewalCompStep1View, animated: true)
    }
    
    
    //MARK: Immigration views
    func loadImiHistoryView(companyID: String?) {
        
        let imiHistoryView:ImiHistoryView = mainStroyBoard?.instantiateViewController(withIdentifier: "ImiHistoryView") as! ImiHistoryView
        imiHistoryView.companyId  = companyID
        navController?.pushViewController(imiHistoryView, animated: true)
    }
    
    //MARK: Visas views
    
    func loadVisaAppsListView(companyID: String?) {
        
        let visaAppsListView:VisaAppsListView = mainStroyBoard?.instantiateViewController(withIdentifier: "VisaAppsListView") as! VisaAppsListView
        visaAppsListView.companyId  = companyID
        navController?.pushViewController(visaAppsListView, animated: true)
    }
    
    
    //MARK: Common views
    func loadPackagesView(controller: UIViewController?, isFromPackages: Bool) {
        
        let packagesView:PackagesView = mainStroyBoard?.instantiateViewController(withIdentifier: "PackagesView") as! PackagesView
        packagesView.delegate  = controller as? PackagesViewDelegate
        packagesView.isComingFromPackages = isFromPackages
        navController?.pushViewController(packagesView, animated: true)
    }
    
    func loadDownloadView() {
        
        let packagesView:FAQListView = mainStroyBoard?.instantiateViewController(withIdentifier: "FAQListView") as! FAQListView
        navController?.pushViewController(packagesView, animated: true)
    }
    
    func loadActivitiesView(controller: UIViewController?) {
        
        let activitiesView:SelectMultipleActivitiesView = mainStroyBoard?.instantiateViewController(withIdentifier: "SelectMultipleActivitiesView") as! SelectMultipleActivitiesView
        activitiesView.delegate  = controller as? ActivitiesViewDelegate
        navController?.pushViewController(activitiesView, animated: true)
    }
    
    func loadChangePasswordView() {
        let changePasswordView:ChangePasswordView = mainStroyBoard?.instantiateViewController(withIdentifier: "ChangePasswordView") as! ChangePasswordView
        navController?.pushViewController(changePasswordView, animated: true)
    }
    
    func loadEditProfileView() {
        
        let editProfileView:EditProfileView = mainStroyBoard?.instantiateViewController(withIdentifier: "EditProfileView") as! EditProfileView
        navController?.pushViewController(editProfileView, animated: true)
    }
    
    func loadResetPasswordView() {
        
        let resetPasswordView:ResetPasswordView = mainStroyBoard?.instantiateViewController(withIdentifier: "ResetPasswordView") as! ResetPasswordView
        navController?.pushViewController(resetPasswordView, animated: true)
    }
    
    //MARK: Visas views
    func loadFileManagerView() {
        
        let fileManager:FileManagerFolders = mainStroyBoard?.instantiateViewController(withIdentifier: "FileManagerFolders") as! FileManagerFolders
        navController?.pushViewController(fileManager, animated: true)
    }
    
    //MARK: 1.4 Version
    func loadPCompanyNOCS(companyID: String?) {
        
        let companyNOCView:CompanyNOCView = mainStroyBoard?.instantiateViewController(withIdentifier: "CompanyNOCView") as! CompanyNOCView
        companyNOCView.companyId  = companyID
        navController?.pushViewController(companyNOCView, animated: true)
    }
    
    func loadCompanyAllAttestations(companyID: String?) {
        
        let companyAttestationView:CompanyAttestationView = mainStroyBoard?.instantiateViewController(withIdentifier: "CompanyAttestationView") as! CompanyAttestationView
        companyAttestationView.companyId  = companyID
        navController?.pushViewController(companyAttestationView, animated: true)
    }
    
    
    func loadWhatsNewView() {
        
        let whatsNewView:WhatsNewView = mainStroyBoard?.instantiateViewController(withIdentifier: "WhatsNewView") as! WhatsNewView
        navController?.present(whatsNewView, animated: true, completion: nil)
    }
    
    func loadPDFView(fileName: String?) {
        
        let pdfViewerView:PDFViewerController = mainStroyBoard?.instantiateViewController(withIdentifier: "PDFViewerController") as! PDFViewerController
        pdfViewerView.fileName = fileName
        navController?.pushViewController(pdfViewerView, animated: true)
    }


    
}
