//
//  UtilHelper.swift
//  OnlineCCProject
//
//  Created by Idrees on 2017/10/10.
//  Copyright © 2017 CheapSender. All rights reserved.
//

import UIKit

class UtilHelper: NSObject {
    
    class func saveAndUpdateNSUserDefault (withObject object: Any, forKey key:String) {
        
        UserDefaults.standard.set(object, forKey: key)
        UserDefaults.standard.synchronize()
    }
    
    class func getValueFromNSUserDefault(forkey key: String) -> Any {
        
        var value = UserDefaults.standard.object(forKey: key)
        
        if value == nil {
            value = "-1"
        }
        return value ?? "-1"
    }
    
    class func getValueFromNSUserDefaultForSignUp(forkey key: String) -> Any {
        
        var value = UserDefaults.standard.object(forKey: key)
        
        if value == nil {
            value = ""
        }
        return value ?? ""
    }
    
    class func makeCornerCurved(withView view:UIView, withRadius radius:CGFloat, withWidth width:CGFloat, withColor color:UIColor) -> UIView {
        
        let curvedView = view
        curvedView.layer.cornerRadius = radius
        curvedView.layer.masksToBounds = true
        curvedView.layer.borderWidth = width
        curvedView.layer.borderColor = color.cgColor
        return curvedView
    }
    
    class func makeImageRouded(withImageView imageView:UIImageView) -> UIImageView {
        
        let roundedImageView = imageView
        roundedImageView.layer.cornerRadius = roundedImageView.frame.size.height/2
        roundedImageView.layer.masksToBounds = true
        return roundedImageView
    }
    
    class func isEmptyString(withText text:String) -> Bool {
        
        return (nil == text || true == (self.trimWhiteSpacesOfaText(withText: text) == "")) ? true : false
    }
    
    class func trimWhiteSpacesOfaText(withText text:String) -> String {
        return text.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
    }
    
    class func isMatchPasswordLength(_ password: String) -> Bool
    {
        if (password.count) > 6 {
            return false
        }
        else {
            return true
        }
    }
    
    class func validateEmail(withEmail eMail: String) -> Bool {
        
        var isValidEmail = false
        
        if (eMail != .EMPTY_STRING) {
            
            var searchtext = "\\"
            
            if ((eMail as NSString).range(of: searchtext)).location != NSNotFound {
                return false
            }
            searchtext = "\""
            if ((eMail as NSString).range(of: searchtext)).location != NSNotFound {
                return false
            }
            let regExPattern = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
            var regEx = try? NSRegularExpression(pattern: regExPattern, options: .caseInsensitive)
            let regExMatches: Int? = regEx?.numberOfMatches(in: eMail, options: [], range: NSRange(location: 0, length: (eMail.count)))
            regEx = nil
            isValidEmail = (regExMatches == 0) ? false : true
        }
        return isValidEmail
    }
    
    //MARK: Related dates
    class func getDateFromString(stringData:String) -> Date {
        
        let issueFormatter = DateFormatter()
        issueFormatter.dateFormat = "YYYY-MM-dd"
        issueFormatter.timeZone = NSTimeZone(abbreviation: "UTC") as TimeZone?
        let issueDate = issueFormatter.date(from: stringData )
        return issueDate!
    }
    
    class func getDateTimeFromString(stringData:String) -> Date {
        
        let issueFormatter = DateFormatter()
        issueFormatter.dateFormat = "YYYY-MM-dd HH:mm:ss"
        issueFormatter.timeZone = NSTimeZone(abbreviation: "UTC") as TimeZone?
        let issueDate = issueFormatter.date(from: stringData )
        return issueDate!
    }
    
    class func getDateStringFromDate(stringDate:Date) -> String {
        
        let issueFormatter = DateFormatter()
        issueFormatter.dateFormat = "YYYY-MM-dd"
        issueFormatter.timeZone = NSTimeZone(abbreviation: "UTC") as TimeZone?
        let issueDate = issueFormatter.string(from: stringDate)
        return issueDate
    }
    
    class func getDateStringFromDates(expirydate:Date) -> String {
        
        let formatter = DateComponentsFormatter()
        formatter.unitsStyle = .brief // May delete the word brief to let Xcode show you the other options
        formatter.allowedUnits = [.year, .month, .day]
        let stringDate = formatter.string(from: Date(), to: expirydate)
        return stringDate!
    }
}

extension String {
    
    enum RegularExpressions: String {
        case phone = "^\\s*(?:\\+?(\\d{1,3}))?([-. (]*(\\d{3})[-. )]*)?((\\d{3})[-. ]*(\\d{2,4})(?:[-.x ]*(\\d+))?)\\s*$"
    }
    
    func isValid(regex: RegularExpressions) -> Bool {
        return isValid(regex: regex.rawValue)
    }
    
    func isValid(regex: String) -> Bool {
        let matches = range(of: regex, options: .regularExpression)
        return matches != nil
    }
    
    func onlyDigits() -> String {
        let filtredUnicodeScalars = unicodeScalars.filter{CharacterSet.decimalDigits.contains($0)}
        return String(String.UnicodeScalarView(filtredUnicodeScalars))
    }
    
    func makeACall() {
        if isValid(regex: .phone) {
            if let url = URL(string: "tel://\(self.onlyDigits())"), UIApplication.shared.canOpenURL(url) {
                if #available(iOS 10, *) {
                    UIApplication.shared.open(url)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
        }
    }
}
