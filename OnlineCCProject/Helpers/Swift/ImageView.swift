//
//  ImageView.swift
//  OnlineCCProject
//
//  Created by apple on 5/20/18.
//  Copyright © 2018 CheapSender. All rights reserved.
//

import UIKit

@IBDesignable
class ImageView: UIImageView {

    @IBInspectable var borderColor: UIColor? {
        didSet {
            layer.borderColor = borderColor?.cgColor
        }
    }
    
    @IBInspectable var borderRadious: CGFloat = 0 {
        didSet {
            layer.cornerRadius = borderRadious
        }
    }
    
    @IBInspectable var borderWidth: CGFloat = 0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }

}
