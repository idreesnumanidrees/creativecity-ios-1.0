//
//  PDFCreator.h
//  FileManagerCustom
//
//  Created by Idrees on 2017/10/17.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface PDFCreator : NSObject

+(NSString *)generatePDFWithPages:(NSArray *)pages  path:(NSString *)destinationUrl fileName:(NSString *)string;

+ (UIImage *)buildThumbnailImage:(NSString *)filePath;

@end
