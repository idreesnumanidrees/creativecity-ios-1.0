//
//  PDFCreator.m
//  FileManagerCustom
//
//  Created by Idrees on 2017/10/17.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import "PDFCreator.h"
#import "FCFileManager.h"

@implementation PDFCreator

+ (NSString *)generatePDFWithPages:(NSArray *)pages path:(NSString *)destinationUrl fileName:(NSString *)string {
    
    NSString *filepath = [[NSTemporaryDirectory() stringByAppendingPathComponent:string] stringByAppendingPathExtension:@"pdf"];
    UIGraphicsBeginPDFContextToFile(filepath, CGRectZero, nil);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    for (UIView *page in pages) {
        [self drawPage:page withContext:context];
    }
    UIGraphicsEndPDFContext();
    
    NSString *copyFile = [destinationUrl stringByAppendingPathExtension:@"pdf"];
    [FCFileManager copyItemAtPath:filepath toPath:copyFile];
    
    return filepath;
}

+(void)drawPage:(UIView *)page withContext:(CGContextRef)context {
    
    UIGraphicsBeginPDFPageWithInfo(CGRectMake(0.0, 0.0, page.bounds.size.width, page.bounds.size.height), nil);
    
    for (UIView *subview in [self allSubViewsForPage:page]) {
        
        if ([subview isKindOfClass:[UIImageView class]]) {
            
            UIImageView *imageView = (UIImageView *)subview;
            
            CGSize imageSize = imageView.image.size;
            float hfactor = imageSize.width / imageView.frame.size.width;
            float vfactor = imageSize.height / imageView.frame.size.height;

            float factor = fmax(hfactor, vfactor);

            // Divide the size by the greater of the vertical or horizontal shrinkage factor
            float newWidth = imageSize.width / factor;
            float newHeight = imageSize.height / factor;
            
            CGRect newRect = CGRectMake((page.frame.size.width - newWidth)/2, (page.frame.size.height - newHeight)/2, newWidth, newHeight);
            [imageView.image drawInRect:newRect];
            
        } else if ([subview isKindOfClass:[UILabel class]]) {
            
            UILabel *label = (UILabel *)subview;
            
            NSMutableParagraphStyle *paragraphStyle = [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
            paragraphStyle.lineBreakMode = label.lineBreakMode;
            paragraphStyle.alignment = label.textAlignment;
            
            [label.text drawInRect:label.frame
                    withAttributes:@{
                                     NSFontAttributeName:label.font,
                                     NSParagraphStyleAttributeName:paragraphStyle,
                                     NSForegroundColorAttributeName:label.textColor
                                     }];
            
        } else if ([subview isKindOfClass:[UIView class]]) {
            
            [self drawLinesUsingUIView:subview
                     withLineThickness:1.0
                     inGraphicsContext:context
                              fillView:subview.tag];
            
        }
    }
}

+(void)drawLinesUsingUIView:(UIView *)view withLineThickness:(float)thickness inGraphicsContext:(CGContextRef)context fillView:(BOOL)fillView
{
    CGContextSaveGState(context);
    CGContextSetStrokeColorWithColor(context, view.backgroundColor.CGColor);
    CGContextSetFillColorWithColor(context, view.backgroundColor.CGColor);
    CGContextSetLineWidth(context, thickness);
    
    if (view.frame.size.width > 1 && view.frame.size.height == 1)
    {
        // view represents a horizontal line so draw one based on the top edge
        CGContextMoveToPoint(context, view.frame.origin.x-0.5, view.frame.origin.y);
        CGContextAddLineToPoint(context, view.frame.origin.x+view.frame.size.width-0.5, view.frame.origin.y);
        CGContextStrokePath(context);
    }
    else if (view.frame.size.width == 1 && view.frame.size.height > 1)
    {
        // view represents a vertical line so draw one based on the left edge
        CGContextMoveToPoint(context, view.frame.origin.x, view.frame.origin.y-0.5);
        CGContextAddLineToPoint(context, view.frame.origin.x, view.frame.origin.y+view.frame.size.height+0.5);
        CGContextStrokePath(context);
    }
    else if (view.frame.size.width > 1 && view.frame.size.height > 1)
    {
        // both the width and height of the view are greater than 1, so draw a rect around the frame
        if (fillView) {
            CGContextSetFillColorWithColor(context, view.backgroundColor.CGColor);
            CGContextFillRect(context, view.frame);
        }
        CGContextStrokeRect(context, view.frame);
    }
    CGContextRestoreGState(context);
}

+(NSArray*)allSubViewsForPage:(UIView *)page
{
    NSMutableArray *array = [@[] mutableCopy];
    
    [array addObject:page];
    
    for (UIView *subview in page.subviews)
    {
        if ([subview isKindOfClass:[UILabel class]])
        {
            UILabel *label = (UILabel *)subview;
            [label sizeToFit];
            [label layoutIfNeeded];
        }
        
        CGPoint origin = [subview.superview convertPoint:subview.frame.origin toView:subview.superview.superview];
        subview.frame = CGRectMake(origin.x, origin.y, subview.frame.size.width, subview.frame.size.height);
        [array addObjectsFromArray:[self allSubViewsForPage:subview]];
    }
    
    return [array copy];
}

+ (UIImage *)buildThumbnailImage:(NSString *)filePath
{
    BOOL hasRetinaDisplay = FALSE;  // by default
    CGFloat pixelsPerPoint = 1.0;  // by default (pixelsPerPoint is just the "scale" property of the screen)
    
    if ([UIScreen instancesRespondToSelector:@selector(scale)])  // the "scale" property is only present in iOS 4.0 and later
    {
        // we are running iOS 4.0 or later, so we may be on a Retina display;  we need to check further...
        if ((pixelsPerPoint = [[UIScreen mainScreen] scale]) == 1.0)
            hasRetinaDisplay = FALSE;
        else
            hasRetinaDisplay = TRUE;
    }
    else
    {
        // we are NOT running iOS 4.0 or later, so we can be sure that we are NOT on a Retina display
        pixelsPerPoint = 1.0;
        hasRetinaDisplay = FALSE;
    }
    
    size_t imageWidth = 173;  // width of thumbnail in points
    size_t imageHeight = 250;  // height of thumbnail in points
    
    if (hasRetinaDisplay)
    {
        imageWidth *= pixelsPerPoint;
        imageHeight *= pixelsPerPoint;
    }
    
    size_t bytesPerPixel = 4;  // RGBA
    size_t bitsPerComponent = 8;
    size_t bytesPerRow = bytesPerPixel * imageWidth;
    
    void *bitmapData = malloc(imageWidth * imageHeight * bytesPerPixel);
    
    // in the event that we were unable to mallocate the heap memory for the bitmap,
    // we just abort and preemptively return nil:
    if (bitmapData == NULL)
        return nil;
    
    // remember to zero the buffer before handing it off to the bitmap context:
    bzero(bitmapData, imageWidth * imageHeight * bytesPerPixel);
    
    CGContextRef theContext = CGBitmapContextCreate(bitmapData, imageWidth, imageHeight, bitsPerComponent, bytesPerRow,
                                                    CGColorSpaceCreateDeviceRGB(), kCGImageAlphaPremultipliedLast);
    
    CGPDFDocumentRef pdfDocument = MyGetPDFDocumentRef(filePath);  // NOTE: you will need to modify this line to supply the CGPDFDocumentRef for your file here...
    CGPDFPageRef pdfPage = CGPDFDocumentGetPage(pdfDocument, 1);  // get the first page for your thumbnail
    
    CGAffineTransform shrinkingTransform =
    CGPDFPageGetDrawingTransform(pdfPage, kCGPDFMediaBox, CGRectMake(0, 0, imageWidth, imageHeight), 0, YES);
    
    CGContextConcatCTM(theContext, shrinkingTransform);
    
    CGContextDrawPDFPage(theContext, pdfPage);  // draw the pdfPage into the bitmap context
    CGPDFDocumentRelease(pdfDocument);
    
    //
    // create the CGImageRef (and thence the UIImage) from the context (with its bitmap of the pdf page):
    //
    CGImageRef theCGImageRef = CGBitmapContextCreateImage(theContext);
    free(CGBitmapContextGetData(theContext));  // this frees the bitmapData we malloc'ed earlier
    CGContextRelease(theContext);
    
    UIImage *theUIImage;
    
    // CAUTION: the method imageWithCGImage:scale:orientation: only exists on iOS 4.0 or later!!!
    if ([UIImage respondsToSelector:@selector(imageWithCGImage:scale:orientation:)])
    {
        theUIImage = [UIImage imageWithCGImage:theCGImageRef scale:pixelsPerPoint orientation:UIImageOrientationUp];
    }
    else
    {
        theUIImage = [UIImage imageWithCGImage:theCGImageRef];
    }
    
    CFRelease(theCGImageRef);
    return theUIImage;
}

CGPDFDocumentRef MyGetPDFDocumentRef(NSString* filePath)
{
//    NSString *inputPDFFile = [[[NSBundle mainBundle] bundlePath] stringByAppendingPathComponent:@"test.pdf"];
    
    const char *inputPDFFileAsCString = [filePath cStringUsingEncoding:NSASCIIStringEncoding];
    //NSLog(@"expecting pdf file to exist at this pathname: \"%s\"", inputPDFFileAsCString);
    
    CFStringRef path = CFStringCreateWithCString(NULL, inputPDFFileAsCString, kCFStringEncodingUTF8);
    CFURLRef url = CFURLCreateWithFileSystemPath(NULL, path, kCFURLPOSIXPathStyle, 0);
    CFRelease (path);
    
    CGPDFDocumentRef document = CGPDFDocumentCreateWithURL(url);
    CFRelease(url);
    
    if (CGPDFDocumentGetNumberOfPages(document) == 0)
    {
        printf("Warning: No pages in pdf file \"%s\" or pdf file does not exist at this path\n", inputPDFFileAsCString);
        return NULL;
    }
    
    return document;
}

@end
