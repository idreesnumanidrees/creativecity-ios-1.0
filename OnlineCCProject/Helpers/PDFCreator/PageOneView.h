//
//  PageOneView.h
//  XIB_2_PDF
//
//  Created by Robert Phillips on 16/12/2013.
//  Copyright (c) 2013 Intohand. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PageOneView : UIView
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@end
