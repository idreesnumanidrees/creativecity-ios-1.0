//
//  WebServicesClient.swift
//  OnlineCCProject
//
//  Created by Idrees on 2017/10/10.
//  Copyright © 2017 CheapSender. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class WebServicesClient: NSObject {

    
    class func postRequest(parameters: Parameters, methodName:String, controller:String, completion:@escaping (_ jsonResponse:JSON?) -> Void)
    {
        //Custom Headers
        let headers: HTTPHeaders = [
            "API_USR": "test",
            "API_KEY": "9gZSNo6lPGd0CUlqabT87fxyK1dBs1bx"
        ]
        
        Alamofire.request("https://sys.ccfz.ae:4423/api/\(controller)\(methodName)", method: .post, parameters:parameters, headers: headers).responseJSON { (response) in
            
            switch response.result {
                
            case .success(let value):
                
                let json = JSON(value)
//                print("JSON: \(json)")
                completion(json)
                
            case .failure(let error):
                print(error)
                completion(nil)
            }
        }
    }
    
    class func signUp(parameters: Parameters, completion:@escaping (_ jsonResponse:JSON?) -> Void)
    {
        //Custom Headers
        let headers: HTTPHeaders = [
            "API_USR": "test",
            "API_KEY": "9gZSNo6lPGd0CUlqabT87fxyK1dBs1bx"
        ]
        
        Alamofire.request("http://ccfz.ae/api/index.php", method: .post, parameters:parameters, headers: headers).responseJSON { (response) in
            
            switch response.result {
                
            case .success(let value):
                
                let json = JSON(value)
                //   print("JSON: \(json)")
                completion(json)
                
            case .failure(let error):
                print(error)
                completion(nil)
            }
        }
    }
    
    class func getRequest(methodName:String, controller:String, completion:@escaping (_ jsonResponse:JSON?) -> Void)
    {
        //Custom Headers
        let headers: HTTPHeaders = [
            "API_USR": "test",
            "API_KEY": "9gZSNo6lPGd0CUlqabT87fxyK1dBs1bx"
        ]
        
        Alamofire.request("https://sys.ccfz.ae:4423/api/\(controller)\(methodName)", method: .get, parameters:nil, headers: headers).responseJSON { (response) in
            
            switch response.result {
                
            case .success(let value):
                
                let json = JSON(value)
                // print("JSON: \(json)")
                completion(json)
                
            case .failure(let error):
                print(error)
                completion(nil)
            }
        }
    }
}
