//
//  CompanyShareCertsCell.swift
//  OnlineCCProject
//
//  Created by apple on 5/16/18.
//  Copyright © 2018 CheapSender. All rights reserved.
//

import UIKit

class CompanyShareCertsCell: UITableViewCell {

    @IBOutlet weak var dateTimeLbl: UILabel!
    @IBOutlet weak var verionLbl: UILabel!
    
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var typeLbl: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
