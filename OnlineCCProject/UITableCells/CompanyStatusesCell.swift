//
//  CompanyStatusesCell.swift
//  OnlineCCProject
//
//  Created by apple on 5/16/18.
//  Copyright © 2018 CheapSender. All rights reserved.
//

import UIKit

class CompanyStatusesCell: UITableViewCell {

    @IBOutlet weak var lblType: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblActions: UILabel!
    
    @IBOutlet weak var actionButton: Button!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
