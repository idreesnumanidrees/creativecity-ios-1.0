//
//  OtherHomeCell.swift
//  OnlineCCProject
//
//  Created by apple on 5/9/18.
//  Copyright © 2018 CheapSender. All rights reserved.
//

import UIKit

class OtherHomeCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var iconImageView: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
