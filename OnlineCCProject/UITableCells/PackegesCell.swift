//
//  PackegesCell.swift
//  OnlineCCProject
//
//  Created by apple on 5/30/18.
//  Copyright © 2018 CheapSender. All rights reserved.
//

import UIKit

class PackegesCell: UITableViewCell {
    
    @IBOutlet weak var lblPackageName: UILabel!
    @IBOutlet weak var lblPackageDetails: UILabel!
    @IBOutlet weak var selectButton: UIButton!

    @IBOutlet weak var selectButtonHeightConstraint: NSLayoutConstraint!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
