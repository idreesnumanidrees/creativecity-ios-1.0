//
//  ApplicationHomeCell.swift
//  OnlineCCProject
//
//  Created by Idrees on 2017/10/15.
//  Copyright © 2017 CheapSender. All rights reserved.
//

import UIKit

class ApplicationListCell: UITableViewCell {

    @IBOutlet weak var activeLabel: UILabel!
    @IBOutlet weak var companyNameLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        activeLabel = UtilHelper.makeCornerCurved(withView: activeLabel, withRadius: 5, withWidth: 0.5, withColor:.EMPTY_COLOR) as! UILabel

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
