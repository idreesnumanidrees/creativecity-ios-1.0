//
//  VisaAppsListCell.swift
//  OnlineCCProject
//
//  Created by Idrees on 2017/11/15.
//  Copyright © 2017 CheapSender. All rights reserved.
//

import UIKit

class VisaAppsListCell: UITableViewCell {
    
    @IBOutlet weak var activeLabel: UILabel!
    @IBOutlet weak var appHolderNameLabel: UILabel!
    @IBOutlet weak var processLabel: UILabel!
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var passportLabel: UILabel!
    @IBOutlet weak var appNoLabel: UILabel!

    let fontNames = ["SFUIText-Medium", "ArialHebrew-Bold", "AmericanTypewriter-Bold", "Noteworthy-Bold", "Optima-Bold"]
    let colorArray = [UIColor.red, UIColor.green, UIColor.blue, UIColor.cyan, UIColor.magenta]
    
    var appObject:Dictionary<String, AnyObject> = [:]

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        activeLabel = UtilHelper.makeCornerCurved(withView: activeLabel, withRadius: 5, withWidth: 0.5, withColor:.EMPTY_COLOR) as! UILabel

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setupForInitial(tag:Int, company:Dictionary<String, AnyObject>) {
        
        appObject = company
        
        appHolderNameLabel.font = UIFont(name: fontNames[0], size: 20)
        appHolderNameLabel.text = company["full_name_en"] as? String
        activeLabel.text = company["_status"] as? String
        passportLabel.text = company["passport_no_now"] as? String
        typeLabel.text = company["app_type"] as? String
        processLabel.text = company["service_type"] as? String
        appNoLabel.text = company["ID"] as? String

        if activeLabel.text == "Processing" {
            
            activeLabel.backgroundColor = .PROCESSING_COLOR
            
        } else if activeLabel.text == "Done" {
            
            activeLabel.backgroundColor = .ACTIVE_COLOR
            
        }else if activeLabel.text == "Cancelled" {
            activeLabel.backgroundColor = .BLOCKED_COLOR
        }
    }

}
