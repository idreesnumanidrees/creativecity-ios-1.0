//
//  HomeCell.swift
//  OnlineCCProject
//
//  Created by Idrees on 2017/10/15.
//  Copyright © 2017 CheapSender. All rights reserved.
//

import UIKit
import PDFReader

class HomeCell: UITableViewCell {
    
    //MARK: Company related outlets
    @IBOutlet weak var activeLabel: UILabel!
    @IBOutlet weak var companyNameLabel: UILabel!
    @IBOutlet weak var companyExpiryLabel: UILabel!
    @IBOutlet weak var companyCodeLabel: UILabel!
    @IBOutlet weak var companyLicNoLabel: UILabel!
    @IBOutlet weak var companyEstDate: UILabel!
    @IBOutlet weak var companyIssueDate: UILabel!
    
    @IBOutlet weak var compViewLicButton: UIButton!
    
    @IBOutlet weak var copyLicenseButton: UIButton!
    @IBOutlet weak var compAppButton: UIButton!
    @IBOutlet weak var compCertIncorporationButton: UIButton!
    @IBOutlet weak var compArticlesIncorporationButton: UIButton!
    @IBOutlet weak var compShareCertButton: UIButton!
    @IBOutlet weak var compAddendumsButton: UIButton!
    @IBOutlet weak var compOfficeLeaseButton: UIButton!
    
    @IBOutlet weak var allAttestationButton: UIButton!
    
    @IBOutlet weak var companyViewCounter: SRCountdownTimer!
    
    //Version(1.3)
    @IBOutlet weak var allApplicationsButton: UIButton!
    
    
    //MARK: Immigration Crad related outlets
    @IBOutlet weak var immigrationCardCounter: SRCountdownTimer!
    @IBOutlet weak var imiCardIssueDateLbl: UILabel!
    @IBOutlet weak var imiCardLbl: UILabel!
    @IBOutlet weak var imiCardExpiryDateLbl: UILabel!
    
    @IBOutlet weak var viewImiCardButton: UIButton!
    @IBOutlet weak var imiAppButton: UIButton!
    
    //MARK: Visa Crad related outlets
    @IBOutlet weak var visasCounter: SRCountdownTimer!
    
    @IBOutlet weak var visaLimitLbl: UILabel!
    @IBOutlet weak var issuedVisasLbl: UILabel!
    @IBOutlet weak var visaLeftLbl: UILabel!
    
    @IBOutlet weak var allVisasButton: UIButton!
    @IBOutlet weak var visaApplicationButton: UIButton!
    @IBOutlet weak var newVisas: UILabel!
    
    let fontNames = ["SFUIText-Medium", "ArialHebrew-Bold", "AmericanTypewriter-Bold", "Noteworthy-Bold", "Optima-Bold"]
    let colorArray = [UIColor.red, UIColor.green, UIColor.blue, UIColor.cyan, UIColor.magenta]
    
    var companyObject:Dictionary<String, AnyObject> = [:]
    var navTitle:String = ""
    
    
    //MARK: -- Version 1.4
    @IBOutlet weak var nocsButton: UIButton!
    
    //Constraint
    @IBOutlet weak var imiCardHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var visaCardheightConstraint: NSLayoutConstraint!
    
    let userId = UtilHelper.getValueFromNSUserDefault(forkey: "ID") as! String
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func setupForInitial(tag:Int, company:Dictionary<String, AnyObject>) {
        
        //MARK: IBActions Setup
        companyObject = company
        
        //Company related
        companyNameLabel.text = company["name_en"] as? String
        companyLicNoLabel.text = String(format: "Lic No:%@", company["license_no"]! as! CVarArg)
        companyCodeLabel.text = String(format: "Agency:%@", company["agency"]! as! CVarArg)
        activeLabel.text = company["status_text"] as? String
        companyEstDate.text = String(format: "Est Date:%@", company["establish_date"]! as! CVarArg)
        companyIssueDate.text = String(format: "Issue Date:%@", company["issue_date"]! as! CVarArg)
        companyExpiryLabel.text = String(format: "Expiry Date:%@", company["expiry_date"]! as! CVarArg)
        
        if activeLabel.text == "Processing" {
            
            activeLabel.backgroundColor = .PROCESSING_COLOR
            //            companyNameLabel.textColor = .PROCESSING_COLOR
            companyNameLabel.font = UIFont(name: fontNames[2], size: 25)
            
            
        } else if activeLabel.text == "Active" {
            
            activeLabel.backgroundColor = .ACTIVE_COLOR
            //            companyNameLabel.textColor = .ACTIVE_COLOR
            companyNameLabel.font = UIFont(name: fontNames[0], size: 25)
            
        } else if activeLabel.text == "Blocked" {
            
            activeLabel.backgroundColor = .BLOCKED_COLOR
            //            companyNameLabel.textColor = .BLOCKED_COLOR
            companyNameLabel.font = UIFont(name: fontNames[3], size: 25)
        }
        
        let companyIssueDate1 = UtilHelper.getDateFromString(stringData: company["issue_date"] as! String)
        let companyExpiryDate1 = UtilHelper.getDateFromString(stringData: company["expiry_date"] as! String)
        
        var companyStringDate = UtilHelper.getDateStringFromDates(expirydate: companyExpiryDate1);
        
        let companyCalendar = NSCalendar.current
        // Replace the hour (time) of both dates with 00:00
        let companyTotalTime =  companyCalendar.dateComponents([.day], from: companyIssueDate1, to: companyExpiryDate1)
        let companyElapsedTime =  companyCalendar.dateComponents([.day], from: Date(), to: companyExpiryDate1)
        
        let totalTimeInteval = companyTotalTime.day
        let elapseTimeInteval = companyElapsedTime.day
        let finalLeftTime = totalTimeInteval! - elapseTimeInteval!
        
        companyViewCounter.totalTime = TimeInterval((companyTotalTime.day)!)
        companyViewCounter.elapsedTime = TimeInterval((0))
        
        //Company Counter
        companyViewCounter.setNeedsDisplay()
        companyViewCounter.labelFont = UIFont(name: "SFUIText-Regular", size: 12.0)
        companyViewCounter.labelTextColor = .BLOCKED_COLOR
        companyViewCounter.timerFinishingText = "Expired"
        companyViewCounter.lineWidth = 6
        companyViewCounter.lineColor = UIColor(red: 10/255, green: 117/255, blue: 16/255, alpha: 1.0)
        companyViewCounter.totalTime = TimeInterval((companyTotalTime.day)!)
        companyViewCounter.counterLabel.textColor = UIColor(red: 10/255, green: 117/255, blue: 16/255, alpha: 1.0)
        companyViewCounter.counterLabel.text = String (format: "%@ Left", companyStringDate)
        companyViewCounter.elapsedTime = TimeInterval((finalLeftTime))
        
        if elapseTimeInteval! < 30 {
            
            companyViewCounter.lineColor = .BLOCKED_COLOR
            companyViewCounter.counterLabel.textColor = .BLOCKED_COLOR
        }
        
        if elapseTimeInteval == 0 {
            
            companyViewCounter.lineColor = .BLOCKED_COLOR
            companyViewCounter.counterLabel.textColor = .BLOCKED_COLOR
            companyViewCounter.elapsedTime = TimeInterval((0))
        }
        
        if elapseTimeInteval! <  0 {
            
            companyStringDate = companyStringDate.replacingOccurrences(of: "-", with: "", options:NSString.CompareOptions.literal, range: nil)
            
            companyViewCounter.lineColor = .BLOCKED_COLOR
            companyViewCounter.counterLabel.text = String (format: "Expired: %@ Ago", companyStringDate)
            companyViewCounter.elapsedTime = TimeInterval((0))
            companyViewCounter.counterLabel.textColor = .BLOCKED_COLOR
        }
        
        //MARK: Company related Actions
        allApplicationsButton.addTarget(self, action: #selector(self.allApplicationsTapped(_:)) , for: .touchUpInside)
        allApplicationsButton.tag = tag
        compViewLicButton.addTarget(self, action: #selector(self.companyViewLicTapped(_:)) , for: .touchUpInside)
        compViewLicButton.tag = tag
        copyLicenseButton.addTarget(self, action: #selector(self.copyLicenseTapped(_:)) , for: .touchUpInside)
        compViewLicButton.tag = tag
        compAppButton.addTarget(self, action: #selector(self.compAppTapped(_:)) , for: .touchUpInside)
        compAppButton.tag = tag
        compCertIncorporationButton.addTarget(self, action: #selector(self.compCertIncorporationTapped(_:)) , for: .touchUpInside)
        compCertIncorporationButton.tag = tag
        compArticlesIncorporationButton.addTarget(self, action: #selector(self.compArticlesIncorporationTapped(_:)) , for: .touchUpInside)
        compArticlesIncorporationButton.tag = tag
        compShareCertButton.addTarget(self, action: #selector(self.compShareCertTapped(_:)) , for: .touchUpInside)
        compShareCertButton.tag = tag
        compAddendumsButton.addTarget(self, action: #selector(self.compAddendumsTapped(_:)) , for: .touchUpInside)
        compAddendumsButton.tag = tag
        compOfficeLeaseButton.addTarget(self, action: #selector(self.compOfficeLeaseTapped(_:)) , for: .touchUpInside)
        compOfficeLeaseButton.tag = tag
        
        //MARK: Version 1.4
        nocsButton.addTarget(self, action: #selector(self.nocsButtonTapped(_:)) , for: .touchUpInside)
        nocsButton.tag = tag
        
        allAttestationButton.addTarget(self, action: #selector(self.allAttestationTapped(_:)) , for: .touchUpInside)
        allAttestationButton.tag = tag
        
        
        //Company related End *********************************************************************************************
        
        if let imgCard = company["card"]  {
            
            imiCardHeightConstraint.constant = 304
            
            imiCardLbl.text = String(format: "IMI Card:%@", imgCard["card_no"]! as! CVarArg)
            imiCardIssueDateLbl.text = String(format: "Issue Date:%@", imgCard["issue_date"]! as! CVarArg)
            imiCardExpiryDateLbl.text = String(format: "Expiry Date:%@", imgCard["expiry_date"]! as! CVarArg)
            
            let imiIssueDate1 = UtilHelper.getDateFromString(stringData: imgCard["issue_date"] as! String)
            let imiExpiryDate1 = UtilHelper.getDateFromString(stringData: imgCard["expiry_date"] as! String)
            
            var imiStringDate = UtilHelper.getDateStringFromDates(expirydate: imiExpiryDate1);
            
            let imiCalendar = NSCalendar.current
            // Replace the hour (time) of both dates with 00:00
            let imiTotalTime =  imiCalendar.dateComponents([.day], from: imiIssueDate1, to: imiExpiryDate1)
            let imiElapsedTime =  imiCalendar.dateComponents([.day], from: Date(), to: imiExpiryDate1)
            
            let imiTotalTimeInteval = imiTotalTime.day
            let imiTlapseTimeInteval = imiElapsedTime.day
            
            let imiFinalLeftTime = imiTotalTimeInteval! - imiTlapseTimeInteval!
            
            //Immigration Card Counter
            immigrationCardCounter.setNeedsDisplay()
            immigrationCardCounter.labelFont = UIFont(name: "SFUIText-Regular", size: 12.0)
            immigrationCardCounter.labelTextColor = .BLOCKED_COLOR
            immigrationCardCounter.timerFinishingText = "Expired"
            immigrationCardCounter.lineWidth = 6
            immigrationCardCounter.lineColor = UIColor(red: 10/255, green: 117/255, blue: 16/255, alpha: 1.0)
            immigrationCardCounter.totalTime = TimeInterval((imiTotalTime.day)!)
            immigrationCardCounter.counterLabel.textColor = UIColor(red: 10/255, green: 117/255, blue: 16/255, alpha: 1.0)
            immigrationCardCounter.counterLabel.text = String (format: "%@ Left", imiStringDate)
            immigrationCardCounter.elapsedTime = TimeInterval((imiFinalLeftTime))
            
            if imiTlapseTimeInteval! < 30 {
                
                immigrationCardCounter.lineColor = .BLOCKED_COLOR
                immigrationCardCounter.counterLabel.textColor = .BLOCKED_COLOR
            }
            
            if imiTlapseTimeInteval == 0 {
                
                immigrationCardCounter.lineColor = .BLOCKED_COLOR
                immigrationCardCounter.counterLabel.textColor = .BLOCKED_COLOR
                immigrationCardCounter.elapsedTime = TimeInterval((0))
            }
            
            
            if imiTlapseTimeInteval! <  0 {
                
                imiStringDate = imiStringDate.replacingOccurrences(of: "-", with: "", options:NSString.CompareOptions.literal, range: nil)
                
                immigrationCardCounter.lineColor = .BLOCKED_COLOR
                immigrationCardCounter.counterLabel.text = String (format: "Expired: %@ Ago", imiStringDate)
                immigrationCardCounter.elapsedTime = TimeInterval((0))
                immigrationCardCounter.counterLabel.textColor = .BLOCKED_COLOR
            }
            
            //MARK: Immigration Card related Actions
            viewImiCardButton.addTarget(self, action: #selector(self.viewImiCardTapped(_:)) , for: .touchUpInside)
            viewImiCardButton.tag = tag
            imiAppButton.addTarget(self, action: #selector(self.imiAppTapped(_:)) , for: .touchUpInside)
            imiAppButton.tag = tag
            
        }else{
            
            imiCardHeightConstraint.constant = 0
        }
        
        //Immigration Card related End *********************************************************************************************
        
        if let visas = company["useage"]  {
            
            visaCardheightConstraint.constant = 320
            
            visaLimitLbl.text = String(format: "Limit:%@", company["vLimit"]! as! CVarArg)
            issuedVisasLbl.text = String(format: "Issued:%@", visas["vtotal"]! as! CVarArg)
            visaLeftLbl.text = String(format: "%@/%@", visas["total"]! as! CVarArg,company["vLimit"]! as! CVarArg)
            newVisas.text = String(format: "New:%@", visas["atotal"]! as! CVarArg)
            
            let visaLimit:Int? = (company["vLimit"] as! NSString).integerValue
            let totalVisas:Int? = visas["total"] as? Int
            
            let visasLeft = visaLimit! - totalVisas!
            
            //Visas Counter
            visasCounter.setNeedsDisplay()
            visasCounter.labelFont = UIFont(name: "SFUIText-Regular", size: 12.0)
            visasCounter.labelTextColor = .BLOCKED_COLOR
            visasCounter.timerFinishingText = "Expired"
            visasCounter.lineWidth = 6
            visasCounter.lineColor = UIColor(red: 10/255, green: 117/255, blue: 16/255, alpha: 1.0)
            visasCounter.totalTime = TimeInterval((company["vLimit"] as! NSString).integerValue)
            visasCounter.counterLabel.textColor = UIColor(red: 10/255, green: 117/255, blue: 16/255, alpha: 1.0)
            visasCounter.counterLabel.text = String (format: "%i visas Left", visasLeft)
            visasCounter.elapsedTime = visas["vtotal"]! as! TimeInterval
            
            if visasLeft < 2 {
                
                visasCounter.lineColor = .BLOCKED_COLOR
                visasCounter.counterLabel.textColor = .BLOCKED_COLOR
                visasCounter.counterLabel.text = String (format: "1 visa Left", visasLeft)
            }
            
            if visasLeft == 0 {
                
                visasCounter.lineColor = .BLOCKED_COLOR
                visasCounter.counterLabel.textColor = .BLOCKED_COLOR
                visasCounter.elapsedTime = TimeInterval((0))
                visasCounter.counterLabel.text = String (format: "No visa Left", visasLeft)
            }
            
            //MARK: Visas related Actions
            allVisasButton.addTarget(self, action: #selector(self.allVisasTapped(_:)) , for: .touchUpInside)
            allVisasButton.tag = tag
            visaApplicationButton.addTarget(self, action: #selector(self.visaApplicationTapped(_:)) , for: .touchUpInside)
            visaApplicationButton.tag = tag
            
        }else {
            
            //Visas Counter
            visaCardheightConstraint.constant = 0
        }
    }
    
    //MARK: Company related Actions
    
    //MARK: Company related Actions
    @objc func allApplicationsTapped(_ sender:UIButton) {
        
        AppDelegate.sharedAppDelegate().appController?.loadAllCompApps(companyID: companyObject["ID"] as? String)
    }
    
    @objc func companyViewLicTapped(_ sender:UIButton) {
        
        AppDelegate.sharedAppDelegate().appController?.loadPCompanyLicenseView(companyID: companyObject["ID"] as? String)
    }
    
    @objc func copyLicenseTapped(_ sender:UIButton) {
        
        let remotePDFDocumentURLPath = companyObject["lic_pdf"] as? String
        let licNo = companyObject["name_en"] as? String
        
        AppDelegate.sharedAppDelegate().appController?.loadPDFViewerView(folderName:"/creativecity/\(userId)/License", fileName: licNo, pdfUrl: remotePDFDocumentURLPath, navTitle:"Company License")
    }
    
    @objc func compAppTapped(_ sender:UIButton) {
        
        AppDelegate.sharedAppDelegate().appController?.loadCompanyStatuesView(companyID: companyObject["ID"] as? String)
    }
    
    @objc func compCertIncorporationTapped(_ sender:UIButton) {
        
        AppDelegate.sharedAppDelegate().appController?.loadCompanyIncorporationView(companyID: companyObject["ID"] as? String)
    }
    
    @objc func compArticlesIncorporationTapped(_ sender:UIButton) {
        
        AppDelegate.sharedAppDelegate().appController?.loadCompanyArticlesView(companyID: companyObject["ID"] as? String)
    }
    
    @objc func compShareCertTapped(_ sender:UIButton) {
        
        AppDelegate.sharedAppDelegate().appController?.loadCompanyShareCertsView(companyID: companyObject["ID"] as? String)
    }
    
    @objc func compOfficeLeaseTapped(_ sender:UIButton) {
        
        AppDelegate.sharedAppDelegate().appController?.loadOfficeLeaseAgreementView(companyID: companyObject["ID"] as? String)
    }
    
    @objc func compAddendumsTapped(_ sender:UIButton) {
        
        AppDelegate.sharedAppDelegate().appController?.loadCompanyAddendumsView(companyID: companyObject["ID"] as? String)
    }
    
    //MARK: Version 1.4
    @objc func nocsButtonTapped(_ sender:UIButton) {
        
        AppDelegate.sharedAppDelegate().appController?.loadPCompanyNOCS(companyID: companyObject["ID"] as? String)
    }
    
    @objc func allAttestationTapped(_ sender:UIButton) {
        
        AppDelegate.sharedAppDelegate().appController?.loadCompanyAllAttestations(companyID: companyObject["ID"] as? String)
    }
    
    //MARK: Immigration Card related Actions
    @objc func viewImiCardTapped(_ sender:UIButton) {
        
        if let imgCard = companyObject["card"]  {
            
            let remotePDFDocumentURLPath = imgCard["pdf"] as? String
            let licNo = companyObject["name_en"] as? String
            
            AppDelegate.sharedAppDelegate().appController?.loadPDFViewerView(folderName:"/creativecity/\(userId)/IMI Cards", fileName: licNo, pdfUrl: remotePDFDocumentURLPath, navTitle:"Immigration Card")
            
//            let remotePDFDocumentURLPath = imgCard["pdf"] as? String
//            if let remotePDFDocumentURL = URL(string: remotePDFDocumentURLPath!), let doc = document(remotePDFDocumentURL) {
//                showDocument(doc)
//
//            } else {
//                print("Document named \(String(describing: remotePDFDocumentURLPath)) not found")
//            }
        }
    }
    
    @objc func imiAppTapped(_ sender:UIButton) {
        
        AppDelegate.sharedAppDelegate().appController?.loadImiHistoryView(companyID: companyObject["ID"] as? String)
    }
    
    //MARK: Visas related Actions
    @objc func allVisasTapped(_ sender:UIButton) {
        
        AppDelegate.sharedAppDelegate().appController?.loadVisaHomeView(companyID: companyObject["ID"] as? String)
    }
    
    @objc func visaApplicationTapped(_ sender:UIButton) {
        
        AppDelegate.sharedAppDelegate().appController?.loadVisaAppsListView(companyID: companyObject["ID"] as? String)
    }
    
    
    //MARK: - PDF Reader View
    /// Initializes a document with the remote url of the pdf
    private func document(_ remoteURL: URL) -> PDFDocument? {
        return PDFDocument(url: remoteURL)
    }
    
    /// Presents a document
    ///
    /// - parameter document: document to present
    ///
    /// Add `thumbnailsEnabled:false` to `createNew` to not load the thumbnails in the controller.
    private func showDocument(_ document: PDFDocument) {
        let image = UIImage(named: "")
        let controller = PDFViewController.createNew(with: document, title: "", actionButtonImage: image, actionStyle: .activitySheet)
        controller.title = navTitle
        AppDelegate.sharedAppDelegate().appController?.navController?.pushViewController(controller, animated: true)
    }
}
