//
//  VisaHomeCell.swift
//  OnlineCCProject
//
//  Created by Idrees on 2017/10/15.
//  Copyright © 2017 CheapSender. All rights reserved.
//
import UIKit
import PDFReader

class VisaHomeCell: UITableViewCell {
    
    @IBOutlet weak var activeLabel: UILabel!
    @IBOutlet weak var visaHolderNameLabel: UILabel!
    @IBOutlet weak var visaExpiryDateLabel: UILabel!
    @IBOutlet weak var visaUIDLabel: UILabel!
    @IBOutlet weak var visaTypeLabel: UILabel!
    @IBOutlet weak var jobTitleLabel: UILabel!
    @IBOutlet weak var issueDateLabel: UILabel!
    @IBOutlet weak var passportNoLabel: UILabel!
    
    @IBOutlet weak var visaExpiryCountDown: SRCountdownTimer!
    @IBOutlet weak var viewVisaButton: UIButton!
    @IBOutlet weak var viewPassportButton: UIButton!
    
    let fontNames = ["SFUIText-Medium", "ArialHebrew-Bold", "AmericanTypewriter-Bold", "Noteworthy-Bold", "Optima-Bold"]
    let colorArray = [UIColor.red, UIColor.green, UIColor.blue, UIColor.cyan, UIColor.magenta]
    
    var visaObject:Dictionary<String, AnyObject> = [:]
    
    let userId = UtilHelper.getValueFromNSUserDefault(forkey: "ID") as! String
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        activeLabel = UtilHelper.makeCornerCurved(withView: activeLabel, withRadius: 5, withWidth: 0.5, withColor:.EMPTY_COLOR) as? UILabel
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func setupForInitial(tag:Int, company:Dictionary<String, AnyObject>) {
        
        visaObject = company
        
        visaHolderNameLabel.font = UIFont(name: fontNames[0], size: 20)
        visaHolderNameLabel.text = company["employee_full_name"] as? String
        activeLabel.text = company["_status"] as? String
        visaUIDLabel.text = company["visa_UID"] as? String
        passportNoLabel.text = company["passport_no_now"] as? String
        visaTypeLabel.text = company["visa_type"] as? String
        jobTitleLabel.text = company["job_title_en"] as? String
        issueDateLabel.text = company["issue_date"] as? String
        visaExpiryDateLabel.text = String(format: "Expiry Date:%@", company["expiry_date"]! as! CVarArg)
        
        if visaTypeLabel.text == "resident" {
            
            visaTypeLabel.text = "Resident"
        } else if visaTypeLabel.text == "entry" {
            visaTypeLabel.text = "Entry"
        }
        
        if activeLabel.text == "RES Transfer" {
            
            activeLabel.backgroundColor = .PROCESSING_COLOR
            
        } else if activeLabel.text == "Active" {
            
            activeLabel.backgroundColor = .ACTIVE_COLOR
            
        }else if activeLabel.text == "Cancelled" {
            activeLabel.backgroundColor = .BLOCKED_COLOR
        }
        
        //Showing Count Down Status
        let issueDate = UtilHelper.getDateFromString(stringData: company["issue_date"] as! String)
        let expiryDate = UtilHelper.getDateFromString(stringData: company["expiry_date"] as! String)
        
        var stringDate = UtilHelper.getDateStringFromDates(expirydate: expiryDate);
        
        let visaCalendar = NSCalendar.current
        // Replace the hour (time) of both dates with 00:00
        let visaTotalTime =  visaCalendar.dateComponents([.day], from: issueDate, to: expiryDate)
        let visaElapsedTime =  visaCalendar.dateComponents([.day], from: Date(), to: expiryDate)
        
        let totalTimeInteval = visaTotalTime.day
        let elapseTimeInteval = visaElapsedTime.day
        let finalLeftTime = totalTimeInteval! - elapseTimeInteval!
        
        visaExpiryCountDown.totalTime = TimeInterval((visaTotalTime.day)!)
        visaExpiryCountDown.elapsedTime = TimeInterval((0))
        
        //Company Counter
        visaExpiryCountDown.setNeedsDisplay()
        visaExpiryCountDown.labelFont = UIFont(name: "SFUIText-Regular", size: 12.0)
        visaExpiryCountDown.labelTextColor = .BLOCKED_COLOR
        visaExpiryCountDown.timerFinishingText = "Expired"
        visaExpiryCountDown.lineWidth = 6
        visaExpiryCountDown.lineColor = UIColor(red: 10/255, green: 117/255, blue: 16/255, alpha: 1.0)
        visaExpiryCountDown.totalTime = TimeInterval((visaTotalTime.day)!)
        visaExpiryCountDown.counterLabel.textColor = UIColor(red: 10/255, green: 117/255, blue: 16/255, alpha: 1.0)
        visaExpiryCountDown.counterLabel.text = String (format: "%@ Left", stringDate)
        visaExpiryCountDown.elapsedTime = TimeInterval((finalLeftTime))
        
        if elapseTimeInteval! < 30 {
            
            visaExpiryCountDown.lineColor = .BLOCKED_COLOR
            visaExpiryCountDown.counterLabel.textColor = .BLOCKED_COLOR
        }
        
        if elapseTimeInteval == 0 {
            
            visaExpiryCountDown.lineColor = .BLOCKED_COLOR
            visaExpiryCountDown.counterLabel.textColor = .BLOCKED_COLOR
            visaExpiryCountDown.elapsedTime = TimeInterval((0))
        }
        
        if elapseTimeInteval! <  0 {
            
            stringDate = stringDate.replacingOccurrences(of: "-", with: "", options:NSString.CompareOptions.literal, range: nil)
            
            visaExpiryCountDown.lineColor = .BLOCKED_COLOR
            visaExpiryCountDown.counterLabel.text = String (format: "Expired: %@ Ago", stringDate)
            visaExpiryCountDown.elapsedTime = TimeInterval((0))
            visaExpiryCountDown.counterLabel.textColor = .BLOCKED_COLOR
        }
        
        //MARK: -IBActions
        viewVisaButton.addTarget(self, action: #selector(self.visaCopyTapped(_:)) , for: .touchUpInside)
        viewVisaButton.tag = tag
        
        viewPassportButton.addTarget(self, action: #selector(self.viewPassportTapped(_:)) , for: .touchUpInside)
        viewPassportButton.tag = tag
    }
    
    @objc func visaCopyTapped(_ sender:UIButton) {
        
        let remotePDFDocumentURLPath = visaObject["visa_pdf"] as? String
        let licNo = visaObject["passport_no_now"] as? String
        
        AppDelegate.sharedAppDelegate().appController?.loadPDFViewerView(folderName:"/creativecity/\(userId)/Visas", fileName: licNo, pdfUrl: remotePDFDocumentURLPath, navTitle:"Visa Copy")
        
        //        let remotePDFDocumentURLPath = visaObject["visa_pdf"] as? String
        //        if let remotePDFDocumentURL = URL(string: remotePDFDocumentURLPath!), let doc = document(remotePDFDocumentURL) {
        //            showDocument(doc)
        //        } else {
        //            print("Document named \(String(describing: remotePDFDocumentURLPath)) not found")
        //        }
    }
    
    @objc func viewPassportTapped(_ sender:UIButton) {
        
        let remotePDFDocumentURLPath = visaObject["pass_pdf"] as? String
        let licNo = visaObject["passport_no_now"] as? String
        
        AppDelegate.sharedAppDelegate().appController?.loadPDFViewerView(folderName:"/creativecity/\(userId)/Passports", fileName: licNo, pdfUrl: remotePDFDocumentURLPath, navTitle:"Passport Copy")
        
        //        let remotePDFDocumentURLPath = visaObject["visa_pdf"] as? String
        //        if let remotePDFDocumentURL = URL(string: remotePDFDocumentURLPath!), let doc = document(remotePDFDocumentURL) {
        //            showDocument(doc)
        //        } else {
        //            print("Document named \(String(describing: remotePDFDocumentURLPath)) not found")
        //        }
    }
    
    
    
    //MARK: - PDF Reader View
    /// Initializes a document with the remote url of the pdf
    private func document(_ remoteURL: URL) -> PDFDocument? {
        return PDFDocument(url: remoteURL)
    }
    
    /// Presents a document
    ///
    /// - parameter document: document to present
    ///
    /// Add `thumbnailsEnabled:false` to `createNew` to not load the thumbnails in the controller.
    private func showDocument(_ document: PDFDocument) {
        let image = UIImage(named: "")
        let controller = PDFViewController.createNew(with: document, title: "", actionButtonImage: image, actionStyle: .activitySheet)
        controller.title = "Visa Copy"
        AppDelegate.sharedAppDelegate().appController?.navController?.pushViewController(controller, animated: true)
    }
}
