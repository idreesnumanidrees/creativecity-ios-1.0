//
//  CompanyNOCCell.swift
//  OnlineCCProject
//
//  Created by apple on 3/26/19.
//  Copyright © 2019 CheapSender. All rights reserved.
//

import UIKit

class CompanyNOCCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var passportNoLabel: UILabel!
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var issueDateLabel: UILabel!
    @IBOutlet weak var issueTimeLabel: UILabel!
    @IBOutlet weak var expiryDateLabel: UILabel!
    
    @IBOutlet weak var nocExpiryCountDown: SRCountdownTimer!
    @IBOutlet weak var viewNOCButton: UIButton!
    
    var nocObject:Dictionary<String, AnyObject> = [:]
    
    let userId = UtilHelper.getValueFromNSUserDefault(forkey: "ID") as! String
    
    //Constraint
    @IBOutlet weak var nocPDFViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var passportHeightConstaint: NSLayoutConstraint!
    
    @IBOutlet weak var nameHeightContraint: NSLayoutConstraint!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setupForInitial(tag:Int, NOCObject:Dictionary<String, AnyObject>) {
        
        nocObject = NOCObject
        
        titleLabel.text = NOCObject["title"] as? String
        nameLabel.text = NOCObject["emp_name"] as? String
        passportNoLabel.text = NOCObject["emp_passport"] as? String
        typeLabel.text = NOCObject["type"] as? String
        issueDateLabel.text = NOCObject["pdate"] as? String
        issueTimeLabel.text = NOCObject["ctime"] as? String
        
        expiryDateLabel.text = String(format: "Expiry Date:%@", NOCObject["exdate"]! as! CVarArg)
        
        //If passport is not found
        let passport = NOCObject["emp_passport"] as? String
        if (passport?.isEmpty)! {
            
            passportHeightConstaint.constant = 0
        }else {
            
            passportHeightConstaint.constant = 18
        }
    
        //If name is not found
        let name = NOCObject["emp_name"] as? String
        if (name?.isEmpty)! {
            
            nameHeightContraint.constant = 0
        }else {
            
            nameHeightContraint.constant = 18
        }
        
        //If PDF is not found
        let pdf = NOCObject["pdf"] as? String
        if (pdf?.isEmpty)! {
            
            nocPDFViewHeightConstraint.constant = 0
        }else {
            
            nocPDFViewHeightConstraint.constant = 50
        }
        
        //Showing Count Down Status
        let issueDate = UtilHelper.getDateFromString(stringData: NOCObject["pdate"] as! String)
        let expiryDate = UtilHelper.getDateFromString(stringData: NOCObject["exdate"] as! String)
        
        var stringDate = UtilHelper.getDateStringFromDates(expirydate: expiryDate);
        
        let visaCalendar = NSCalendar.current
        // Replace the hour (time) of both dates with 00:00
        let visaTotalTime =  visaCalendar.dateComponents([.day], from: issueDate, to: expiryDate)
        let visaElapsedTime =  visaCalendar.dateComponents([.day], from: Date(), to: expiryDate)
        
        let totalTimeInteval = visaTotalTime.day
        let elapseTimeInteval = visaElapsedTime.day
        let finalLeftTime = totalTimeInteval! - elapseTimeInteval!
        
        nocExpiryCountDown.totalTime = TimeInterval((visaTotalTime.day)!)
        nocExpiryCountDown.elapsedTime = TimeInterval((0))
        
        //Company Counter
        nocExpiryCountDown.setNeedsDisplay()
        nocExpiryCountDown.labelFont = UIFont(name: "SFUIText-Regular", size: 12.0)
        nocExpiryCountDown.labelTextColor = .BLOCKED_COLOR
        nocExpiryCountDown.timerFinishingText = "Expired"
        nocExpiryCountDown.lineWidth = 6
        nocExpiryCountDown.lineColor = UIColor(red: 10/255, green: 117/255, blue: 16/255, alpha: 1.0)
        nocExpiryCountDown.totalTime = TimeInterval((visaTotalTime.day)!)
        nocExpiryCountDown.counterLabel.textColor = UIColor(red: 10/255, green: 117/255, blue: 16/255, alpha: 1.0)
        nocExpiryCountDown.counterLabel.text = String (format: "%@ Left", stringDate)
        nocExpiryCountDown.elapsedTime = TimeInterval((finalLeftTime))
        
        if elapseTimeInteval! < 15 {
            
            nocExpiryCountDown.lineColor = .BLOCKED_COLOR
            nocExpiryCountDown.counterLabel.textColor = .BLOCKED_COLOR
        }
        
        if elapseTimeInteval == 0 {
            
            nocExpiryCountDown.lineColor = .BLOCKED_COLOR
            nocExpiryCountDown.counterLabel.textColor = .BLOCKED_COLOR
            nocExpiryCountDown.elapsedTime = TimeInterval((0))
        }
        
        if elapseTimeInteval! <  0 {
            
            stringDate = stringDate.replacingOccurrences(of: "-", with: "", options:NSString.CompareOptions.literal, range: nil)
            
            nocExpiryCountDown.lineColor = .BLOCKED_COLOR
            nocExpiryCountDown.counterLabel.text = String (format: "Expired: %@ Ago", stringDate)
            nocExpiryCountDown.elapsedTime = TimeInterval((0))
            nocExpiryCountDown.counterLabel.textColor = .BLOCKED_COLOR
        }
        
        //MARK: -IBActions
        viewNOCButton.addTarget(self, action: #selector(self.nocCopyTapped(_:)) , for: .touchUpInside)
        viewNOCButton.tag = tag
        
    }
    
    @objc func nocCopyTapped(_ sender:UIButton) {
        
        let remotePDFDocumentURLPath = nocObject["pdf"] as? String
        let licNo = nocObject["ID"] as? String
        
        AppDelegate.sharedAppDelegate().appController?.loadPDFViewerView(folderName:"/creativecity/\(userId)/NOCs", fileName: licNo, pdfUrl: remotePDFDocumentURLPath, navTitle:"Visa Copy")
    }
    

}
