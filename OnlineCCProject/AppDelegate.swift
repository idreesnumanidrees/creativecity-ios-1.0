//
//  AppDelegate.swift
//  OnlineCCProject
//
//  Created by Idrees on 2017/10/10.
//  Copyright © 2017 CheapSender. All rights reserved.
//

import UIKit
import CoreData
import UserNotifications
import IQKeyboardManagerSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate {
    
    var window: UIWindow?
    var iosDeviceToken:String?
    
    var licOptions:Dictionary<String, Any>?
    public var appController:AppController?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        // Override point for customization after application launch.
        
        //Removing back title 
        // let BarButtonItemAppearance = UIBarButtonItem.appearance()
        // BarButtonItemAppearance.setTitleTextAttributes([NSAttributedStringKey.foregroundColor: UIColor.white], for: .normal)
        
        IQKeyboardManager.shared.enable = true
        
        //settings for window
        window = UIWindow(frame:UIScreen.main.bounds)
        appController = AppController()
        appController?.initWithWindow(window!)
        appController?.navigationController()
        
        let userId = UtilHelper.getValueFromNSUserDefault(forkey: "ID") as! String
        
        if userId != "-1" && userId != "0" {
            
            AppDelegate.sharedAppDelegate().appController?.loadTabbar()
            
        } else {
            
            appController?.loadSplashView()
        }
        
        iosDeviceToken = UtilHelper.getValueFromNSUserDefault(forkey: "token") as? String
        
        //Push Notification settings
        if #available(iOS 10.0, *) {
            let center = UNUserNotificationCenter.current()
            center.delegate = self
            center.requestAuthorization(options:[.sound, .alert, .badge]) { (_ granted: Bool, _ error:Error?) in
                
                if error == nil {
                    
                    DispatchQueue.main.async(execute: {
                        UIApplication.shared.registerForRemoteNotifications()
                    })
                }
            }
        } else {
            
            // Fallback on earlier versions
            application.registerForRemoteNotifications()
            if application.responds(to: #selector(application.registerUserNotificationSettings)) {
                #if __IPHONE_8_0
                var settings = UIUserNotificationSettings(types: ([.alert, .badge, .sound]), categories: nil)
                //Registers your preferred options for notifying the user.
                application.registerUserNotificationSettings(settings)
                #endif
            }
        }
        
        //Navigation Bar color
        appController?.getNavigationController().navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor(red: 56/255.0, green: 98/255.0, blue: 172/255.0, alpha: 1.0)]
        
        //Creating Directory
        FCFileManager.createDirectories(forPath: "creativecity")
        return true
    }
    
    //******************Shared Delegate Method***********************
    class func sharedAppDelegate() -> AppDelegate {
        
        return UIApplication.shared.delegate as! AppDelegate
    }
    
    //******************Push Notification Methods***********************
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
        if iosDeviceToken == nil || iosDeviceToken == "-1" {
            
            iosDeviceToken = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
            UtilHelper.saveAndUpdateNSUserDefault(withObject: iosDeviceToken ?? "-1", forKey: "token")
        }
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        
        iosDeviceToken = UtilHelper.getValueFromNSUserDefault(forkey: "token") as? String
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        
    }
    
    //******************End Push Notification Methods***********************
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        // self.saveContext()
    }
    
    //    lazy var persistentContainer: NSPersistentContainer = {
    //        /*
    //         The persistent container for the application. This implementation
    //         creates and returns a container, having loaded the store for the
    //         application to it. This property is optional since there are legitimate
    //         error conditions that could cause the creation of the store to fail.
    //        */
    //        let container = NSPersistentContainer(name: "OnlineCCProject")
    //        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
    //            if let error = error as NSError? {
    //                // Replace this implementation with code to handle the error appropriately.
    //                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
    //
    //                /*
    //                 Typical reasons for an error here include:
    //                 * The parent directory does not exist, cannot be created, or disallows writing.
    //                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
    //                 * The device is out of space.
    //                 * The store could not be migrated to the current model version.
    //                 Check the error message to determine what the actual problem was.
    //                 */
    //                fatalError("Unresolved error \(error), \(error.userInfo)")
    //            }
    //        })
    //        return container
    //    }()
    //
    //    // MARK: - Core Data Saving support
    //    func saveContext () {
    //        let context = persistentContainer.viewContext
    //        if context.hasChanges {
    //            do {
    //                try context.save()
    //            } catch {
    //                // Replace this implementation with code to handle the error appropriately.
    //                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
    //                let nserror = error as NSError
    //                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
    //            }
    //        }
    //    }
}

