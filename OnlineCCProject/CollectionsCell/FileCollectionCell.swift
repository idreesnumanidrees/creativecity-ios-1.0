//
//  FileCollectionCell.swift
//  FileManagerCustom
//
//  Created by Idrees on 2017/10/16.
//  Copyright © 2017 CheapSender. All rights reserved.
//

import UIKit

class FileCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var fileName: UILabel!
    @IBOutlet weak var deleteButton: UIButton!
    @IBOutlet weak var fileImageView: UIImageView!
    
    //MARK: Constraints
    @IBOutlet weak var imgLeftConstraint: NSLayoutConstraint!
    
}
