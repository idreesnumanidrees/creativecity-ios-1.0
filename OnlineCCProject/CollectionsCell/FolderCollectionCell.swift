//
//  FolderCollectionCell.swift
//  FileManagerCustom
//
//  Created by Idrees on 2017/10/16.
//  Copyright © 2017 CheapSender. All rights reserved.
//

import UIKit

class FolderCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var folderName: UILabel!
    @IBOutlet weak var deleteButton: UIButton!
    

    //Constraints
    @IBOutlet weak var folderImageViewLeftConstraint: NSLayoutConstraint!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
