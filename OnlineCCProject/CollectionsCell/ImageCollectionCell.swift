//
//  ImageCollectionCell.swift
//  FileManagerCustom
//
//  Created by Idrees on 2017/10/17.
//  Copyright © 2017 CheapSender. All rights reserved.
//

import UIKit

class ImageCollectionCell: UICollectionViewCell {
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var deleteButton: UIButton!
    
}
