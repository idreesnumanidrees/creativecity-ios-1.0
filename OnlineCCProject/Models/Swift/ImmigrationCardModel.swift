//
//  ImmigrationCardModel.swift
//  OnlineCCProject
//
//  Created by apple on 6/24/18.
//  Copyright © 2018 CheapSender. All rights reserved.
//

import UIKit

class ImmigrationCardModel: BaseModel {

    var cardNo:String?
    var imiPDF:String?
    var establishDate:String?
    var issueDate:String?
    var expiryDate:String?

    init(cardNo:String?,imiPDF:String?, establishDate:String?, issueDate:String?, expiryDate:String?) {
        
        self.cardNo = cardNo
        self.imiPDF = imiPDF
        self.establishDate = establishDate
        self.issueDate = issueDate
        self.expiryDate = expiryDate
    }
}
