//
//  VisaModel.swift
//  OnlineCCProject
//
//  Created by apple on 6/24/18.
//  Copyright © 2018 CheapSender. All rights reserved.
//

import UIKit

class VisaModel: BaseModel {


    var vtotal:Int?
    var total:Int?
    var atotal:Int?
    
    init(vtotal:Int?, total:Int?, atotal:Int?) {
        
        self.vtotal = vtotal
        self.total = total
        self.atotal = atotal
    }
}
