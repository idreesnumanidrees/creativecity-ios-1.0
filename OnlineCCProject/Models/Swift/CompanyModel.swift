//
//  CompanyModel.swift
//  OnlineCCProject
//
//  Created by apple on 6/24/18.
//  Copyright © 2018 CheapSender. All rights reserved.
//

import UIKit

class CompanyModel: BaseModel {
    
    var id:String?
    var nameEn:String?
    var nameAr:String?
    var licNo:String?
    var agency:String?

    var vLimit:String?
    var statusText:String?
    var licPDF:String?
    
    var establishDate:String?
    var issueDate:String?
    var expiryDate:String?
    var card:ImmigrationCardModel?
    var visa:VisaModel?

    init(id:String?, nameEn:String?, nameAr:String?, licNo:String?, agency:String?, vLimit:String?, statusText:String?,licPDF:String?, establishDate:String?, issueDate:String?, expiryDate:String?, card:ImmigrationCardModel?, visa:VisaModel?) {
        
        self.id = id
        self.nameEn = nameEn
        self.nameAr = nameAr
        self.licNo = licNo
        self.agency = agency
        self.vLimit = vLimit
        self.licPDF = licPDF
        self.statusText = statusText
        self.establishDate = establishDate
        self.issueDate = issueDate
        self.expiryDate = expiryDate
        
        self.card = card
        self.visa = visa
    }


}
