//
//  StartupView.swift
//  OnlineCCProject
//
//  Created by Idrees on 2017/10/11.
//  Copyright © 2017 CheapSender. All rights reserved.
//

import UIKit
import SVProgressHUD
import MessageUI
import AVFoundation

class StartupView: BaseViewController, URLSessionDownloadDelegate, QRCodeReaderViewControllerDelegate {
    
    //Buttons Outlets
    @IBOutlet weak var langButton: UIButton!
    
    //RoundedViews
    @IBOutlet weak var roundView1: UIView!
    @IBOutlet weak var roundView2: UIView!
    @IBOutlet weak var roundView3: UIView!
    @IBOutlet weak var roundView4: UIView!
    
    var downloadTask: URLSessionDownloadTask!
    var backgroundSession: URLSession!
    
    @IBOutlet weak var containerView: UIView!
    var fileName:String = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        prepareUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        UIApplication.shared.statusBarStyle = .default
        
        if !Reachability.isConnectedToNetwork(){
            self.simpleAlertWithMessage(message: "OOPS! Please check your internet connection properly.")
            return
        }
        
        if AppDelegate.sharedAppDelegate().licOptions == nil {
            
            requestForLicOptions()
        }
        
        //        UIView.appearance().semanticContentAttribute = .forceRightToLeft
        
        //        containerView.semanticContentAttribute = .forceRightToLeft
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        showNavigationBar()
    }
    
    func prepareUI() {
        
        
        let whatsNew = UtilHelper.getValueFromNSUserDefaultForSignUp(forkey: "WhatsNew") as? String
        
        if whatsNew == "" {
            
            AppDelegate.sharedAppDelegate().appController?.loadWhatsNewView()
            UtilHelper.saveAndUpdateNSUserDefault(withObject:"Loaded", forKey: "WhatsNew")
        }
        
        //MARK: --URLSessionConfiguration
        let backgroundSessionConfiguration = URLSessionConfiguration.background(withIdentifier: "StartUpBackgroundSession")
        backgroundSession = Foundation.URLSession(configuration: backgroundSessionConfiguration, delegate: self, delegateQueue: OperationQueue.main)
    }
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    
    @IBAction func callTapped(_ sender: Any) {
        
        "+97192077666".makeACall()
    }
    
    @IBAction func emailTapped(_ sender: Any) {
        
        let emailTitle = ""
        let messageBody = "Dear Sir/Madam,"
        let toRecipents = ["info@ccfz.ae"]
        let mc: MFMailComposeViewController = MFMailComposeViewController()
        mc.mailComposeDelegate = self
        mc.setSubject(emailTitle)
        mc.setMessageBody(messageBody, isHTML: false)
        mc.setToRecipients(toRecipents)
        
        guard MFMailComposeViewController.canSendMail() else {
            
            print("Email can not be sent")
            return
        }
        
        self.present(mc, animated: true, completion: nil)
    }
    
    //MARK: -- IBActions
    @IBAction func loginTapped(_ sender: Any) {
        
        AppDelegate.sharedAppDelegate().appController?.loadLoginView()
    }
    
    @IBAction func signUpTapped(_ sender: Any) {
        
        AppDelegate.sharedAppDelegate().appController?.loadSignUpStep1View()
    }
    
    @IBAction func applyNewLicenseTapped(_ sender: Any) {
        
        if AppDelegate.sharedAppDelegate().licOptions == nil {
            
            if !Reachability.isConnectedToNetwork(){
                self.simpleAlertWithMessage(message: "OOPS! Please check your internet connection properly.")
                return
            }
            requestForLicOptions()
        } else {
            
            AppDelegate.sharedAppDelegate().appController?.loadPersonalInfoView()
        } 
    }
    
    @IBAction func checkCompanyNameTapped(_ sender: Any) {
        
        AppDelegate.sharedAppDelegate().appController?.loadCheckCompanyAvailView()
    }
    
    @IBAction func packagesTapped(_ sender: Any) {
        
        AppDelegate.sharedAppDelegate().appController?.loadPackagesView(controller: nil, isFromPackages: false)
    }
    
    @IBAction func policyTapped(_ sender: Any) {
        
        if AppDelegate.sharedAppDelegate().licOptions == nil {
            
            if !Reachability.isConnectedToNetwork(){
                self.simpleAlertWithMessage(message: "OOPS! Please check your internet connection properly.")
                return
            }
            requestForLicOptions()
            
        } else {
            
            let termCondition = AppDelegate.sharedAppDelegate().licOptions!["termsConditions"] as? String
            if termCondition != nil {
                
                fileName = "Terms and Conditions.pdf"
                
                let path:String = FCFileManager.pathForDocumentsDirectory()
                let destinationURLForFile = URL(fileURLWithPath: path.appendingFormat("/\(fileName)"))
                
                if FCFileManager.existsItem(atPath: destinationURLForFile.path){
                    showFileWithPath(path: destinationURLForFile.path)
                }
                else {
                    
                    showProgress(title: "Downlaoding...")
                    
                    let url = URL(string: termCondition!)
                    downloadTask = backgroundSession.downloadTask(with: url!)
                    downloadTask.resume()
                }
            }
        }
    }
    
    @IBAction func FAQTapped(_ sender: Any) {
        
        if AppDelegate.sharedAppDelegate().licOptions == nil {
            
            if !Reachability.isConnectedToNetwork(){
                self.simpleAlertWithMessage(message: "OOPS! Please check your internet connection properly.")
                return
            }
            requestForLicOptions()
        }else{
            
            AppDelegate.sharedAppDelegate().appController?.loadDownloadView()
        }
    }
    
    //MARK: URLSessionDownloadDelegate
    // 1
    func urlSession(_ session: URLSession,
                    downloadTask: URLSessionDownloadTask,
                    didFinishDownloadingTo location: URL){
        
        dismissProgress()
        let pathOfDirectory:String = FCFileManager.pathForDocumentsDirectory()
        
        let fileManager = FileManager()
        let destinationURLForFile = URL(fileURLWithPath: pathOfDirectory.appendingFormat("/\(fileName)"))
        
        do {
            try fileManager.moveItem(at: location, to: destinationURLForFile)
            // show file
            showFileWithPath(path: destinationURLForFile.path)
        }catch{
            print("An error occurred while moving file to destination url")
        }
    }
    
    
    //QR Code Reader *****************************************Start from **********************************************************
    lazy var reader: QRCodeReader = QRCodeReader()
    lazy var readerVC: QRCodeReaderViewController = {
        let builder = QRCodeReaderViewControllerBuilder {
            $0.reader                  = QRCodeReader(metadataObjectTypes: [.qr], captureDevicePosition: .back)
            $0.showTorchButton         = true
            $0.preferredStatusBarStyle = .lightContent
            $0.showOverlayView        = true
            $0.rectOfInterest          = CGRect(x: 0.1, y: 0.2, width: 0.8, height: 0.6)
            
            $0.reader.stopScanningWhenCodeIsFound = false
        }
        
        return QRCodeReaderViewController(builder: builder)
    }()
    
    @IBAction func scanInModalAction(_ sender: AnyObject) {
        guard checkScanPermissions() else { return }
        
        readerVC.modalPresentationStyle = .formSheet
        readerVC.delegate               = self
        
        readerVC.completionBlock = { (result: QRCodeReaderResult?) in
            if let result = result {
                print("Completion with result: \(result.value) of type \(result.metadataType)")
            }
        }
        
        present(readerVC, animated: true, completion: nil)
    }
    
    // MARK: - Actions
    private func checkScanPermissions() -> Bool {
        do {
            return try QRCodeReader.supportsMetadataObjectTypes()
        } catch let error as NSError {
            let alert: UIAlertController
            
            switch error.code {
            case -11852:
                alert = UIAlertController(title: "Error", message: "This app is not authorized to use Back Camera.", preferredStyle: .alert)
                
                alert.addAction(UIAlertAction(title: "Setting", style: .default, handler: { (_) in
                    DispatchQueue.main.async {
                        if let settingsURL = URL(string: UIApplicationOpenSettingsURLString) {
                            UIApplication.shared.open(settingsURL, options: [:], completionHandler: nil)
                        }
                    }
                }))
                
                alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            default:
                alert = UIAlertController(title: "Error", message: "Reader not supported by the current device", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
            }
            
            present(alert, animated: true, completion: nil)
            return false
        }
    }
    
    // MARK: - QRCodeReader Delegate Methods
    func reader(_ reader: QRCodeReaderViewController, didScanResult result: QRCodeReaderResult) {
        reader.stopScanning()
        
        dismiss(animated: true) { [weak self] in
            
            let urlString = String (format:"%@", result.value)
            let url = URL(string: urlString)
            let domain = url?.host
            
            if domain == "sys.ccfz.ae" {
                
                let myVC = self?.storyboard?.instantiateViewController(withIdentifier: "PDFViewerController") as! PDFViewerController
                myVC.fileName = urlString
                self?.navigationController?.pushViewController(myVC, animated: true)
                
            } else {
                
                let alert = UIAlertController(
                    title: "Creative City",
                    message: "Sorry, We are unable to verify your document.",
                    preferredStyle: .alert
                )
                alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
                self?.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    //MARK: QRCodeReaderViewControllerDelegate
    func reader(_ reader: QRCodeReaderViewController, didSwitchCamera newCaptureDevice: AVCaptureDeviceInput) {
        print("Switching capture to: \(newCaptureDevice.device.localizedName)")
    }
    
    func readerDidCancel(_ reader: QRCodeReaderViewController) {
        reader.stopScanning()
        dismiss(animated: true, completion: nil)
    }
    
    //QR Code Reader ***************************************** End from **********************************************************
}

//MARK --Mail Delegates
extension StartupView:MFMailComposeViewControllerDelegate {
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        switch result.rawValue {
        case MFMailComposeResult.cancelled.rawValue:
            print("Cancelled")
        case MFMailComposeResult.saved.rawValue:
            print("Saved")
        case MFMailComposeResult.sent.rawValue:
            print("Sent")
        case MFMailComposeResult.failed.rawValue:
            print("Error: \(String(describing: error?.localizedDescription))")
        default:
            break
        }
        controller.dismiss(animated: true, completion: nil)
    }
}

