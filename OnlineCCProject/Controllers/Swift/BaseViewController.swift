//
//  BaseViewController.swift
//  OnlineCCProject
//
//  Created by Idrees on 2017/10/10.
//  Copyright © 2017 CheapSender. All rights reserved.
//

import UIKit
import PDFReader
import Alamofire
import SwiftyJSON
import SVProgressHUD

class BaseViewController: UIViewController, UIDocumentInteractionControllerDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    //MARK: Server Handling
    func requestForLicOptions()  {
        
        DispatchQueue.global(qos: .userInitiated).async {
            
            WebServicesClient.getRequest(methodName: "licoption", controller:.Index) { (_ jsonResponse:JSON?) in
                
                // Bounce back to the main thread to update the UI
                DispatchQueue.main.async {
                    
                    guard jsonResponse != nil else {
                        return
                    }
                    
                    print(jsonResponse ?? "Dictionary is nil")
                    
                    if jsonResponse?["error"].int == 0 {
                        
                        guard let data = jsonResponse?["response"].dictionaryObject else {
                            return
                        }
                        
                        AppDelegate.sharedAppDelegate().licOptions = data
                    }
                }
            }
        }
    }
    
    func showProgress(title:String?) {
        
        if title == "" {
            
            SVProgressHUD.show()
        } else {
            SVProgressHUD.show(withStatus: title)
        }
        SVProgressHUD.setBackgroundColor(.white)
        SVProgressHUD.setForegroundColor(.black)
        SVProgressHUD.setDefaultMaskType(.black)
    }
    
    func dismissProgress() {
        
        SVProgressHUD.dismiss()
    }
    
    //MARK: Alert Message
    func alertWithMessage(message: String) {
        
        let alertController =  UIAlertController(title: "Creative City", message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { _ in
            
            self.navigationController?.popViewController(animated: true)
        }))
        self.present(alertController, animated: true, completion: nil)
    }
    
    func simpleAlertWithMessage(message: String) {
        let alertController =  UIAlertController(title: "Creative City", message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        self.present(alertController, animated: true, completion: nil)
    }
    
    //MARK: - PDF Reader View
    /// Initializes a document with the remote url of the pdf
    func document(_ remoteURL: URL) -> PDFDocument? {
        return PDFDocument(url: remoteURL)
    }
    
    /// Presents a document
    ///
    /// - parameter document: document to present
    ///
    /// Add `thumbnailsEnabled:false` to `createNew` to not load the thumbnails in the controller.
    func showDocument(_ document: PDFDocument, navTitle:String?) {
        let image = UIImage(named: "")
        let controller = PDFViewController.createNew(with: document, title: navTitle, actionButtonImage: image, actionStyle: .activitySheet)
        //        controller.title = navTitle
        //        controller.scrollDirection = .vertical
        AppDelegate.sharedAppDelegate().appController?.navController?.pushViewController(controller, animated: true)
    }
    
    //MARK: Showing pdf file
    func showFileWithPath(path: String) {
        
        let isFileFound:Bool? = FileManager.default.fileExists(atPath: path)
        if isFileFound == true{
            let viewer = UIDocumentInteractionController(url: URL(fileURLWithPath: path))
            viewer.delegate = self
            viewer.presentPreview(animated: true)
        }
    }
    
    //MARK: UIDocumentInteractionControllerDelegate
    func documentInteractionControllerViewControllerForPreview(_ controller: UIDocumentInteractionController) -> UIViewController
    {
        return (AppDelegate.sharedAppDelegate().window?.rootViewController)!
    }
    
    func showNavigationBar() {
        
        UIApplication.shared.statusBarStyle = .lightContent
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        self.navigationController?.setNavigationBarHidden(false, animated: true)

    }
    
    func hideNavigationBar() {
        
        UIApplication.shared.statusBarStyle = .lightContent
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
}

