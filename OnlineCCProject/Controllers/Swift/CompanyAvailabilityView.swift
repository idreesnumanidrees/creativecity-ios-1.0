//
//  CompanyAvailabilityView.swift
//  OnlineCCProject
//
//  Created by Idrees on 2017/10/11.
//  Copyright © 2017 CheapSender. All rights reserved.
//

import UIKit
import Alamofire
import SVProgressHUD
import SwiftyJSON

class CompanyAvailabilityView: BaseViewController {
    
    @IBOutlet weak var nameCheckTF: UITextField!
    @IBOutlet weak var checkNoButton: UIButton!
    
    @IBOutlet weak var outerViewForTextfield: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var successLabel: UILabel!
    @IBOutlet weak var successBGImage: UIImageView!
    @IBOutlet weak var successView: UIView!
    
    @IBOutlet weak var tableView: UITableView!
    var similarResults = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        prepareUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        showNavigationBar()
    }
    
    //Setup Initial View
    func prepareUI() {
        
        outerViewForTextfield = UtilHelper.makeCornerCurved(withView: outerViewForTextfield, withRadius: 5, withWidth: 0.5, withColor:.BORDER_COLOR)
        successBGImage = UtilHelper.makeCornerCurved(withView: successBGImage, withRadius: 5, withWidth: 1, withColor: .EMPTY_COLOR) as? UIImageView
        tableView = UtilHelper.makeCornerCurved(withView: tableView, withRadius: 5, withWidth: 1, withColor: .EMPTY_COLOR) as? UITableView
        
        checkNoButton.isEnabled = false
        checkNoButton.backgroundColor = .gray
        successView.isHidden = true
        
        tableView.estimatedRowHeight = 44.0
        tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.isHidden = true
    }
    
    //IBActions
    @IBAction func checkNoButton(_ sender: Any) {
        self.view.endEditing(true)
        postRequestForNameCheck()
    }
    
    //MARK: Server Side Handling
    func postRequestForNameCheck() {
        
        if !Reachability.isConnectedToNetwork(){
            self.simpleAlertWithMessage(message: "OOPS! Please check your internet connection properly")
            return
        }
        
        showProgress(title: "Loading...")
        
        //Post
        let parameters: Parameters = [
            .name : nameCheckTF.text ?? "test"]
        
        // Move to a background thread to do some long running work
        DispatchQueue.global(qos: .userInitiated).async {
            
            WebServicesClient.postRequest(parameters: parameters, methodName: .COMP_NAME_CHECK, controller: .Index) { (_ jsonResponse:JSON?) in
                
                // Bounce back to the main thread to update the UI
                DispatchQueue.main.async {
                    
                    self.dismissProgress()
                    
                    guard jsonResponse != nil else {
                        self.simpleAlertWithMessage(message: "OOPS! Something went wrong")
                        return
                    }
                    
                    print(jsonResponse ?? "Dictionary is nil")
                    
                    if jsonResponse?["error"].int == 0 {
                        
                        guard let data = jsonResponse?["response"].dictionaryObject else {
                            self.simpleAlertWithMessage(message: "OOPS! Something went wrong")
                            return
                        }
                        guard let summary = data["summary"] as? String else {
                            
                            self.simpleAlertWithMessage(message: "OOPS! No summary found")
                            return
                        }
                        
                        self.successView.isHidden = false
                        self.tableView.isHidden = true
                        
                        self.scrollView.contentSize = self.scrollView.frame.size
                        self.scrollView.sizeToFit()
                        
                        if summary == "notavailable" {
                            
                            self.successViewSettings(text: "Oh snap! Name not Available", textColor: UIColor(red: 156/255, green: 74/255, blue: 70/255, alpha: 1.0), backViewColor: UIColor(red: 238/255, green: 223/255, blue: 222/255, alpha: 1.0))
                            
                        } else if summary == "cancelled-now" {
                            
                            self.successViewSettings(text: "Success! Company is already cancelled. You can use the name now.", textColor: UIColor(red: 75/255, green: 116/255, blue: 67/255, alpha: 1.0), backViewColor: UIColor(red: 226/255, green: 239/255, blue: 218/255, alpha: 1.0))
                            
                        }  else if summary == "cancelled-date" {
                            
                            self.successViewSettings(text: "Warning! Company is already cancelled. You can use this name after date \(String(describing: data["date"]))", textColor: UIColor(red: 133/255, green: 110/255, blue: 66/255, alpha: 1.0), backViewColor: UIColor(red: 251/255, green: 248/255, blue: 229/255, alpha: 1.0))
                            
                        } else if summary == "restricted" {
                            
                            self.successViewSettings(text: "Oh snap! Restricted name found! You cannot use the name.", textColor: UIColor(red: 156/255, green: 74/255, blue: 70/255, alpha: 1.0), backViewColor: UIColor(red: 238/255, green: 223/255, blue: 222/255, alpha: 1.0))
                            
                        } else if summary == "non-action" {
                            
                            self.successViewSettings(text: "Warning! Company name is in non-action! You can use this name after some approvales.", textColor: UIColor(red: 133/255, green: 110/255, blue: 66/255, alpha: 1.0), backViewColor: UIColor(red: 251/255, green: 248/255, blue: 229/255, alpha: 1.0))
                            
                        } else if summary == "available" {
                            
                            self.successViewSettings(text: "Success! Name is available.", textColor: UIColor(red: 75/255, green: 116/255, blue: 67/255, alpha: 1.0), backViewColor: UIColor(red: 226/255, green: 239/255, blue: 218/255, alpha: 1.0))
                            
                        }else if summary == "entername" {
                            
                            self.successViewSettings(text: "Oh snap! Enter Name first.", textColor: UIColor(red: 156/255, green: 74/255, blue: 70/255, alpha: 1.0), backViewColor: UIColor(red: 238/255, green: 223/255, blue: 222/255, alpha: 1.0))
                            
                        } else if summary == "similar" {
                            
                            self.successViewSettings(text: "Warning! We found some similar names.", textColor: UIColor(red: 133/255, green: 110/255, blue: 66/255, alpha: 1.0), backViewColor: UIColor(red: 251/255, green: 248/255, blue: 229/255, alpha: 1.0))
                            
                            guard let similarValues = data["similar"] as? [String] else {
                                
                                return
                            }
                            self.similarResults = similarValues
                            self.tableView.isHidden = false
                            self.tableView.reloadData()
                        }
                    }
                    else
                    {
                        print(jsonResponse ?? "Error")
                        self.simpleAlertWithMessage(message: "OOPS! Something went wrong")
                    }
                }
            }
        }
    }
    
    func successViewSettings(text: String, textColor:UIColor, backViewColor:UIColor)  {
        
        self.successLabel.text = text
        self.successLabel.textColor = textColor
        self.successBGImage.backgroundColor = backViewColor
    }
    
    //TouchView
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        self.view.endEditing(true)
    }
}

//Extention For TextField Delegates
extension CompanyAvailabilityView: UITextFieldDelegate {
    
    //TextField Delegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        postRequestForNameCheck()
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let updatedString = (textField.text as NSString?)?.replacingCharacters(in: range, with: string)
        
        if (updatedString?.isEmpty)! {
            checkNoButton.isEnabled = false
            checkNoButton.backgroundColor = .gray
            
        } else {
            checkNoButton.isEnabled = true
            checkNoButton.backgroundColor = .BORDER_COLOR
        }
        return true
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        
        checkNoButton.isEnabled = false
        checkNoButton.backgroundColor = .gray
        return true
    }
}

//Extension For UITableView DataSource and Delegate
extension CompanyAvailabilityView: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.similarResults.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellIdentifier = "CompanyAvailabilityCheckCell"
        
        guard let tableviewcell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? CompanyAvailabilityCheckCell else {
            
            fatalError("The dequeued cell is not instance of HomeCell.")
        }
        
        tableviewcell.similarLabel.text = self.similarResults[indexPath.row]
        
        //Update scrollView
        var tableFrame = self.tableView.frame
        tableFrame.size.height = self.tableView.contentSize.height
        self.tableView.frame = tableFrame
        
        var scrollFrame = self.scrollView.contentSize
        scrollFrame.height = self.tableView.frame.height + 320
        
        scrollView.contentSize = scrollFrame
        scrollView.sizeToFit()
        return tableviewcell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {}
}
