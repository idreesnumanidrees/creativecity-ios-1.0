//
//  VisaAppsListView.swift
//  OnlineCCProject
//
//  Created by Idrees on 2017/11/15.
//  Copyright © 2017 CheapSender. All rights reserved.
//

import UIKit
import KRPullLoader
import Alamofire
import SVProgressHUD
import SwiftyJSON

class VisaAppsListView: BaseViewController, UITableViewDelegate, UITableViewDataSource,UITextFieldDelegate, UISearchBarDelegate {

    var companyId:String?
    
    @IBOutlet weak var appsTableView: UITableView!
    
    var appsArray: [Any] = []
    
    //MAKR: Search related
    var searchedFiles:Array<Any>?
    var isSearchActive : Bool? = false
    @IBOutlet weak var searchBar: UISearchBar!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        prepareUI()
    }
    
    func prepareUI() {
        
        showNavigationBar()
        
        showProgress(title: "")
        requestForAllApps()
    }
    
    //MARK: -TableView Delegates and DataSource
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return 8.0;
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if isSearchActive == true {
            
            return searchedFiles?.count ?? 0
            
        } else {
            
            return appsArray.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellIdentifier = "VisaAppsListCell"
        
        guard let tableviewcell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? VisaAppsListCell else {
            
            fatalError("The dequeued cell is not instance of HomeCell.")
        }
        
        var visa = appsArray[indexPath.row] as! Dictionary<String, AnyObject>
        
        if isSearchActive == true {
            visa = searchedFiles?[indexPath.row] as! Dictionary<String, AnyObject>
        }
        
        tableviewcell.setupForInitial(tag: indexPath.row, company: visa)
        

        return tableviewcell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 170
    }
    
    //TextField Delegates
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return true
    }
    
    //SearchBar Delegates
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        
        isSearchActive = false
        appsTableView.reloadData()
        searchBar.text = ""
        searchedFiles?.removeAll()
        searchBar.resignFirstResponder()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
        if (searchBar.text?.isEmpty)! {
            isSearchActive = false
            appsTableView.reloadData()
        }
        searchBar.resignFirstResponder()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        if ((searchBar.text?.count)! > 1) {
            
            let searchPredicate = NSPredicate(format: "full_name_en contains[c] %@ OR passport_no_now contains[c] %@", argumentArray: [searchBar.text!, searchBar.text!])
            
            searchedFiles = (appsArray as NSArray).filtered(using: searchPredicate)
            appsTableView.reloadData()
        }
    }
    
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        
        isSearchActive = true
        return true
    }
    
    
    
    //MARK: Server Handling
    func requestForAllApps() {
        
        let requestUrl = String(format: "%@/%@", "visa/apps", companyId!)
        
        // Move to a background thread to do some long running work
        // Move to a background thread to do some long running work
        DispatchQueue.global(qos: .userInitiated).async {
            
            WebServicesClient.getRequest(methodName: requestUrl, controller:"") { (_ jsonResponse:JSON?) in
                
                // Bounce back to the main thread to update the UI
                DispatchQueue.main.async {
                    
                    self.dismissProgress()
                    
                    guard jsonResponse != nil else {
                        return
                    }
                    print(jsonResponse ?? "Dictionary is nil")
                    
                    if jsonResponse?["error"].int == 0 {
                        
                        guard let data = jsonResponse?["response"].arrayObject else {
                            return
                        }
                        
                        self.appsArray = data
                        if self.appsArray.count == 0 {
                            self.alertWithMessage(message: "Record not found.")
                            
                        }else{
                            self.appsTableView.reloadData()
                        }
                    }
                    else
                    {
                        print(jsonResponse?.stringValue ?? "Error")
                        self.alertWithMessage(message: "Something went wrong!")
                    }
                }
            }
        }
    }

}
