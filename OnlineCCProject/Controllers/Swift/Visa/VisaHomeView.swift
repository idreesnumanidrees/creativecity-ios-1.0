//
//  VisaHomeView.swift
//  OnlineCCProject
//
//  Created by Idrees on 2017/10/15.
//  Copyright © 2017 CheapSender. All rights reserved.
//

import UIKit
import KRPullLoader
import Alamofire
import SVProgressHUD
import SwiftyJSON

class VisaHomeView: BaseViewController, UITableViewDelegate, UITableViewDataSource,UITextFieldDelegate, UISearchBarDelegate {
    
    var companyId:String?

    @IBOutlet weak var visaHomeTableView: UITableView!
    
    var visasArray: [Any] = []
    
    //MAKR: Search related
    var searchedFiles:Array<Any>?
    var isSearchActive : Bool? = false
    @IBOutlet weak var searchBar: UISearchBar!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        prepareUI()
    }
    
    func prepareUI() {
        
        showNavigationBar()
        showProgress(title: "")
        requestForAllVisas()
    }

    
    //UItableView datasource and delegate
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return 8.0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if isSearchActive == true {
            
            return searchedFiles?.count ?? 0
            
        } else {
            
            return visasArray.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellIdentifier = "VisaHomeCell"
        
        guard let tableviewcell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? VisaHomeCell else {
            
            fatalError("The dequeued cell is not instance of HomeCell.")
        }
        
        var visa = visasArray[indexPath.row] as! Dictionary<String, AnyObject>
        
        if isSearchActive == true {
            visa = searchedFiles?[indexPath.row] as! Dictionary<String, AnyObject>
        }
        
        tableviewcell.setupForInitial(tag: indexPath.row, company: visa)

        return tableviewcell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 382 + 47
    }
    
    //TextField Delegates
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return true
    }
    
    //SearchBar Delegates
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        
        isSearchActive = false
        visaHomeTableView.reloadData()
        searchBar.text = ""
        searchedFiles?.removeAll()
        searchBar.resignFirstResponder()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
        if (searchBar.text?.isEmpty)! {
            isSearchActive = false
            visaHomeTableView.reloadData()
        }
        searchBar.resignFirstResponder()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        if ((searchBar.text?.count)! > 1) {
            
            let searchPredicate = NSPredicate(format: "employee_full_name contains[c] %@ OR passport_no_now contains[c] %@", argumentArray: [searchBar.text!, searchBar.text!])
            
            searchedFiles = (visasArray as NSArray).filtered(using: searchPredicate)
            visaHomeTableView.reloadData()
        }
    }
    
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        
        isSearchActive = true
        return true
    }
    
    
    //MARK: Server Handling
    func requestForAllVisas() {
        
        let requestUrl = String(format: "%@/%@", "visa/all", companyId!)
        
        // Move to a background thread to do some long running work
        // Move to a background thread to do some long running work
        DispatchQueue.global(qos: .userInitiated).async {
            
            WebServicesClient.getRequest(methodName: requestUrl, controller:"") { (_ jsonResponse:JSON?) in
                
                // Bounce back to the main thread to update the UI
                DispatchQueue.main.async {
                    
                    self.dismissProgress()
                    
                    guard jsonResponse != nil else {
                        return
                    }
                    print(jsonResponse ?? "Dictionary is nil")
                    
                    if jsonResponse?["error"].int == 0 {
                        
                        guard let data = jsonResponse?["response"].arrayObject else {
                            return
                        }
                        
                        self.visasArray = data
                        if self.visasArray.count == 0 {
                            self.alertWithMessage(message: "Record not found.")
                            
                        }else{
                            self.visaHomeTableView.reloadData()
                        }
                    }
                    else
                    {
                        print(jsonResponse?.stringValue ?? "Error")
                        self.alertWithMessage(message: "Something went wrong!")
                    }
                }
            }
        }
    }
}
