//
//  OthersHomeView.swift
//  OnlineCCProject
//
//  Created by Idrees on 2017/10/15.
//  Copyright © 2017 CheapSender. All rights reserved.
//

import UIKit
import Alamofire
import SVProgressHUD
import SwiftyJSON
import MessageUI
import AVFoundation

class OthersHomeView: BaseViewController, URLSessionDownloadDelegate, QRCodeReaderViewControllerDelegate {
    
    var downloadTask: URLSessionDownloadTask!
    var backgroundSession: URLSession!
    
    var fileName:String = String()
    
    //    let contents = ["Settings", "Logout"]
    //    let images = ["actionsIcon-iPhone", "Logout"]
    
    let contents = ["Setup New Business","Verify Documents", "Name Availability","Packages","Terms and conditions","Downloads","Edit Profile","Change Password","What's New (Version 1.4)","Logout"]
    let images = ["business-setup","QR-ScanCode", "CheckName-Iphone","Packages-Iphone","Policy-IPhone","download-icon","profile_icon","changePassword","FAQ-IPhone","Logout"]
    
    @IBOutlet weak var ownerName: UILabel!
    @IBOutlet weak var ownerNameLabel: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        prepareUI()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        
        backgroundSession.invalidateAndCancel()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        //MARK: --URLSessionConfiguration
        
        let backgroundSessionConfiguration = URLSessionConfiguration.background(withIdentifier: "OtherBackgroundSession")
        backgroundSession = Foundation.URLSession(configuration: backgroundSessionConfiguration, delegate: self, delegateQueue: OperationQueue.main)
        hideNavigationBar()
        profileUpdatedValues()
    }
    
    func prepareUI() {
        
        self.navigationController!.navigationBar.topItem!.title = ""
    }
    
    func profileUpdatedValues() {
        
        ownerName.text = UtilHelper.getValueFromNSUserDefault(forkey: "displayName") as? String
        let firstName = UtilHelper.getValueFromNSUserDefault(forkey: "user_fname") as? String
        let lastName = UtilHelper.getValueFromNSUserDefault(forkey: "user_lname") as? String
        ownerNameLabel.text = String(firstName?.first?.description ?? "") + String(lastName?.first?.description ?? "")
    }

    //MARK: IBActions
    @IBAction func leftArrowTapped(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func callTapped(_ sender: Any) {
        
        "+97192077666".makeACall()
    }
    
    @IBAction func mailTapped(_ sender: Any) {
        
        let emailTitle = ""
        let messageBody = "Dear Sir/Madam,"
        let toRecipents = ["info@ccfz.ae"]
        let mc: MFMailComposeViewController = MFMailComposeViewController()
        mc.mailComposeDelegate = self
        mc.setSubject(emailTitle)
        mc.setMessageBody(messageBody, isHTML: false)
        mc.setToRecipients(toRecipents)
        
        guard MFMailComposeViewController.canSendMail() else {
            
            print("Email can not be sent")
            return
        }
        self.present(mc, animated: true, completion: nil)
    }
    
    //QR Code Reader *****************************************Start from **********************************************************
    lazy var reader: QRCodeReader = QRCodeReader()
    lazy var readerVC: QRCodeReaderViewController = {
        let builder = QRCodeReaderViewControllerBuilder {
            $0.reader                  = QRCodeReader(metadataObjectTypes: [.qr], captureDevicePosition: .back)
            $0.showTorchButton         = true
            $0.preferredStatusBarStyle = .lightContent
            $0.showOverlayView        = true
            $0.rectOfInterest          = CGRect(x: 0.1, y: 0.2, width: 0.8, height: 0.6)
            
            $0.reader.stopScanningWhenCodeIsFound = false
        }
        
        return QRCodeReaderViewController(builder: builder)
    }()
    
    func scanInModalAction() {
        guard checkScanPermissions() else { return }
        
        readerVC.modalPresentationStyle = .formSheet
        readerVC.delegate               = self
        
        readerVC.completionBlock = { (result: QRCodeReaderResult?) in
            if let result = result {
                print("Completion with result: \(result.value) of type \(result.metadataType)")
            }
        }
        
        present(readerVC, animated: true, completion: nil)
    }
    
    // MARK: - Actions
    
    private func checkScanPermissions() -> Bool {
        do {
            return try QRCodeReader.supportsMetadataObjectTypes()
        } catch let error as NSError {
            let alert: UIAlertController
            
            switch error.code {
            case -11852:
                alert = UIAlertController(title: "Error", message: "This app is not authorized to use Back Camera.", preferredStyle: .alert)
                
                alert.addAction(UIAlertAction(title: "Setting", style: .default, handler: { (_) in
                    DispatchQueue.main.async {
                        if let settingsURL = URL(string: UIApplicationOpenSettingsURLString) {
                            UIApplication.shared.open(settingsURL, options: [:], completionHandler: nil)
                        }
                    }
                }))
                
                alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            default:
                alert = UIAlertController(title: "Error", message: "Reader not supported by the current device", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
            }
            
            present(alert, animated: true, completion: nil)
            return false
        }
    }
    
    // MARK: - QRCodeReader Delegate Methods
    func reader(_ reader: QRCodeReaderViewController, didScanResult result: QRCodeReaderResult) {
        reader.stopScanning()
        
        dismiss(animated: true) { [weak self] in
            
            let urlString = String (format:"%@", result.value)
            let url = URL(string: urlString)
            let domain = url?.host
            
            if domain == "sys.ccfz.ae" {
                
                AppDelegate.sharedAppDelegate().appController?.loadPDFView(fileName: urlString)
                
            } else {
                
                let alert = UIAlertController(
                    title: "Creative City",
                    message: "Sorry, We are unable to verify your document.",
                    preferredStyle: .alert
                )
                alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
                self?.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    //MARK: QRCodeReaderViewControllerDelegate
    func reader(_ reader: QRCodeReaderViewController, didSwitchCamera newCaptureDevice: AVCaptureDeviceInput) {
        print("Switching capture to: \(newCaptureDevice.device.localizedName)")
    }
    
    func readerDidCancel(_ reader: QRCodeReaderViewController) {
        reader.stopScanning()
        dismiss(animated: true, completion: nil)
    }
    
    //QR Code Reader *****************************************End from **********************************************************
}

extension OthersHomeView: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return contents.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellIdentifier = "OtherHomeCell"
        
        guard let tableviewcell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? OtherHomeCell else {
            
            fatalError("The dequeued cell is not instance of OtherHomeCell.")
        }
        
        tableviewcell.selectionStyle = .none
        
        tableviewcell.titleLabel.text = contents[indexPath.row]
        tableviewcell.iconImageView.image = UIImage(named: images[indexPath.row])
        
        return tableviewcell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
         if indexPath.row == 0 {
            
            if AppDelegate.sharedAppDelegate().licOptions == nil {
                
                if !Reachability.isConnectedToNetwork(){
                    self.simpleAlertWithMessage(message: "OOPS! Please check your internet connection properly.")
                    return
                }
                requestForLicOptions()
            } else {
                
                AppDelegate.sharedAppDelegate().appController?.loadPersonalInfoView()
            }
            
            
        } else if indexPath.row == 1 {
            
            scanInModalAction()
        
        } else if indexPath.row == 2 {
            
            AppDelegate.sharedAppDelegate().appController?.loadCheckCompanyAvailView()
            
        } else if indexPath.row == 3 {
            
            AppDelegate.sharedAppDelegate().appController?.loadPackagesView(controller: nil, isFromPackages: false)
            
        } else if indexPath.row == 4 {
            
            privacyPolicy()
            
        } else if indexPath.row == 5 {
            
            if AppDelegate.sharedAppDelegate().licOptions == nil {
                
                if !Reachability.isConnectedToNetwork(){
                    self.simpleAlertWithMessage(message: "OOPS! Please check your internet connection properly.")
                    return
                }
                requestForLicOptions()
            }else{
                
                AppDelegate.sharedAppDelegate().appController?.loadDownloadView()
            }
            
        } else if indexPath.row == 6 {
            
            AppDelegate.sharedAppDelegate().appController?.loadEditProfileView()

        } else if indexPath.row == 7 {
            
            AppDelegate.sharedAppDelegate().appController?.loadChangePasswordView()
            
        } else if indexPath.row == 8 {
            
            AppDelegate.sharedAppDelegate().appController?.loadWhatsNewView()
            
        } else if indexPath.row == 9 {
            
            logout()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50.0
    }
    
    //MARK: Logout function
    func logout() {
        
        requestForLogout()
        UtilHelper.saveAndUpdateNSUserDefault(withObject: "-1", forKey: "ID")
        var isPresentView:Bool = false
        
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: StartupView.self) {
                self.navigationController!.popToViewController(controller, animated: true)
                isPresentView = true
                break
            }else{
                isPresentView = false
            }
        }
        
        if isPresentView == false {
            AppDelegate.sharedAppDelegate().appController?.loadStartUpView()
        }
    }
    
    //MARK: Downloading Privacy Policy
    func privacyPolicy() {
        
        if AppDelegate.sharedAppDelegate().licOptions == nil {
            
            if !Reachability.isConnectedToNetwork(){
                self.simpleAlertWithMessage(message: "OOPS! Please check your internet connection properly.")
                return
            }
            requestForLicOptions()
            
        } else {
            
            let termCondition = AppDelegate.sharedAppDelegate().licOptions!["termsConditions"] as? String
            
            if termCondition != nil {
                
                fileName = "Terms and Conditions.pdf"
                
                let path:String = FCFileManager.pathForDocumentsDirectory()
                let destinationURLForFile = URL(fileURLWithPath: path.appendingFormat("/\(fileName)"))
                
                if FCFileManager.existsItem(atPath: destinationURLForFile.path){
                    showFileWithPath(path: destinationURLForFile.path)
                }
                else {
                    
                    showProgress(title: "Downlaoding...")
                    
                    let url = URL(string: termCondition!)
                    downloadTask = backgroundSession.downloadTask(with: url!)
                    downloadTask.resume()
                }
            }
        }
    }
    
    //MARK: URLSessionDownloadDelegate
    // 1
    func urlSession(_ session: URLSession,
                    downloadTask: URLSessionDownloadTask,
                    didFinishDownloadingTo location: URL){
        
        dismissProgress()
        let pathOfDirectory:String = FCFileManager.pathForDocumentsDirectory()
        
        let fileManager = FileManager()
        let destinationURLForFile = URL(fileURLWithPath: pathOfDirectory.appendingFormat("/\(fileName)"))
        
        do {
            try fileManager.moveItem(at: location, to: destinationURLForFile)
            // show file
            showFileWithPath(path: destinationURLForFile.path)
        }catch{
            print("An error occurred while moving file to destination url")
        }
    }
    
    //MARK: Server Handling
    func requestForLogout() {
        
        let sessionId = UtilHelper.getValueFromNSUserDefault(forkey: "mrSessId")

        let requestUrl = String(format: "%@/%@", "logout", sessionId as! CVarArg)
        
        // Move to a background thread to do some long running work
        // Move to a background thread to do some long running work
        DispatchQueue.global(qos: .userInitiated).async {
            
            WebServicesClient.getRequest(methodName: requestUrl, controller:.Index) { (_ jsonResponse:JSON?) in
                
                // Bounce back to the main thread to update the UI
                DispatchQueue.main.async {
                    
                    SVProgressHUD.dismiss()
                    
                    guard jsonResponse != nil else {
                        return
                    }
                    print(jsonResponse ?? "Dictionary is nil")
                    
                    if jsonResponse?["error"].int == 0 {
                        
                        guard let data = jsonResponse?["response"].string else {
                            return
                        }
                        print(data)
                    }
                    else
                    {}
                }
            }
        }
    }
    
    
    
    
}

//MARK --Mail Delegates
extension OthersHomeView:MFMailComposeViewControllerDelegate {
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        switch result.rawValue {
        case MFMailComposeResult.cancelled.rawValue:
            print("Cancelled")
        case MFMailComposeResult.saved.rawValue:
            print("Saved")
        case MFMailComposeResult.sent.rawValue:
            print("Sent")
        case MFMailComposeResult.failed.rawValue:
            print("Error: \(String(describing: error?.localizedDescription))")
        default:
            break
        }
        controller.dismiss(animated: true, completion: nil)
    }
}

