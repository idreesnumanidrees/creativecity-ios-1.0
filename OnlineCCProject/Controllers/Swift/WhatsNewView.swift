//
//  WhatsNewView.swift
//  OnlineCCProject
//
//  Created by apple on 3/26/19.
//  Copyright © 2019 CheapSender. All rights reserved.
//

import UIKit

class WhatsNewView: BaseViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    let titles = ["Verify Documents", "NOC'S","Document Attestations","Passport Copy In Visa Section", "File Manager design Changes","Search Options", "Other Minor changes"]
    
    let descriptions = ["We have added verify document option. You can verify your document through QR code scanning. You can get this option in More and public home dashboard screen.",
                        "We have added Noc's list, you can see NOC expiry status, view of NOC and you can share NOC as well.",
                        "We have added Document Attestations list, you can view and share attestation copy.",
                        "We have added the option of passport in all visa section, you can view the passport and save into file manager.",
                        "We have changed some design of File Manager screens.",
                        "We have added the search options in Visa and Visa application sections, now you can search visa/visa application by Name/Passport.",
                        "We always care of our respectable clients and give them facilities."]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        prepareUI()
    }
    
    //MARK: Helpers
    func prepareUI() {
        
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 200
    }
    
    //MARK: IBActions
    @IBAction func backTapped(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
        
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
}

extension WhatsNewView: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return titles.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellIdentifier = "CompanyShareCertsCell"
        
        guard let tableviewcell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? CompanyShareCertsCell else {
            
            fatalError("The dequeued cell is not instance of HomeCell.")
        }
        
        tableviewcell.selectionStyle = .none
        
        let title = titles[indexPath.row]
        let description = descriptions[indexPath.row]
        
        tableviewcell.titleLbl.text = title
        tableviewcell.typeLbl.text = description
        
        return tableviewcell
    }
}
