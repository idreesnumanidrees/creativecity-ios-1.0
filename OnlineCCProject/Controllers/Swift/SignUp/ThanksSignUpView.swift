//
//  ThanksSignUpView.swift
//  OnlineCCProject
//
//  Created by apple on 9/16/18.
//  Copyright © 2018 CheapSender. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class ThanksSignUpView: BaseViewController {
    
    var responseDict: Dictionary<String, AnyObject> = [:]


    override func viewDidLoad() {
        super.viewDidLoad()

        hideNavigationBar()
         UIApplication.shared.statusBarStyle = .default
        // Do any additional setup after loading the view.
        requestForUserEmail()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    //MARK: --IBActions
    
    @IBAction func loginTapped(_ sender: Any) {
        
        var isPresentView:Bool = false
        
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: StartupView.self) {
                self.navigationController!.popToViewController(controller, animated: true)
                isPresentView = true
                break
            }else{
                isPresentView = false
            }
        }
        
        if isPresentView == false {
            AppDelegate.sharedAppDelegate().appController?.loadStartUpView()
        }
    }
    
    @IBAction func resendAgainTapped(_ sender: Any) {
        
        requestForUserEmail()
    }
    
    //MARK: -- Request for email
    
    func requestForUserEmail() {
        
        if !Reachability.isConnectedToNetwork(){
            self.simpleAlertWithMessage(message: "OOPS! Please check your internet connection properly.")
            return
        }
        
        showProgress(title: "Email is sending...")
        
        //Post
        let parameters: Parameters = [
            .email : responseDict["pic_email"] ?? "it.CCFZ@gmail.com"]
        
        // Move to a background thread to do some long running work
        DispatchQueue.global(qos: .userInitiated).async {
            
            WebServicesClient.postRequest(parameters: parameters, methodName: .SIGNUP_EMAIL_FOR_ACTIVE, controller: .Index) { (_ jsonResponse:JSON?) in
                
                // Bounce back to the main thread to update the UI
                DispatchQueue.main.async {
                    
                     self.dismissProgress()
                    
                    guard jsonResponse != nil else {
                        return
                    }
                    
                    if jsonResponse?["error"].int == 0 {
                        
                        guard let data = jsonResponse?["response"].string else {
                            return
                        }
                        
                        print(data)
                        
                    }
                    else
                    {
                        print(jsonResponse ?? "Error")
                    }
                }
            }
        }
    }
}
