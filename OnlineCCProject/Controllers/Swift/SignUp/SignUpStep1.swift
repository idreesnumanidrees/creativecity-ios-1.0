//
//  SignUpStep1.swift
//  OnlineCCProject
//
//  Created by apple on 9/9/18.
//  Copyright © 2018 CheapSender. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class SignUpStep1: BaseViewController {
    
    @IBOutlet weak var tfLicenseNumber: FloatLabelTextField!
    
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    //MARK: --IBActions
    @IBAction func searchTapped(_ sender: Any) {
        
        if !Reachability.isConnectedToNetwork(){
            self.simpleAlertWithMessage(message: "OOPS! Please check your internet connection properly.")
            return
        }
        
        if !((tfLicenseNumber.text?.isEmpty)!) {
            
            requestForCompLic();
            
        } else {
            
            self.simpleAlertWithMessage(message: "OOPS! Please enter your license number.")
        }
    }
    
    func requestForCompLic() {
        
        showProgress(title: "Searching...")
        
        //Params
        let parameters: Parameters = [
            .licNo:tfLicenseNumber.text ?? "abc"]
        
        // Move to a background thread to do some long running work
        DispatchQueue.global(qos: .userInitiated).async {
            
            WebServicesClient.postRequest(parameters: parameters, methodName: .SEARCH_LIC, controller: .Index) { (_ jsonResponse:JSON?) in
                
                // Bounce back to the main thread to update the UI
                DispatchQueue.main.async {
                    
                    self.dismissProgress()
                    
                    guard jsonResponse != nil else {
                        self.simpleAlertWithMessage(message: "OOPS! Something went wrong")
                        return
                    }
                    print(jsonResponse ?? "Dictionary is nil")
                    
                    if jsonResponse?["error"].int == 0 {
                        
                        guard let data = jsonResponse?["response"].dictionaryObject else {
                            self.simpleAlertWithMessage(message: "OOPS! Something went wrong")
                            return
                        }
                        
                        guard let emailExist = data["pic_email"] as? String else {
                            
                            return
                        }
                        print(emailExist)
                        
                        if emailExist == "" {
                            
                            AppDelegate.sharedAppDelegate().appController?.loadRequestFprEmailView(response: data as Dictionary<String, AnyObject>)
                        }else {
                            
                            guard let userExist = data["user_exist"] as? Bool else {
                                
                                return
                            }
                            
                            if (userExist) {
                                
                                guard let userStatus = data["user_status"] as? String else {
                                    
                                    return
                                }
                                
                                if userStatus == "1" {
                                    
                                    AppDelegate.sharedAppDelegate().appController?.loadLoginView()

                                } else if userStatus == "2" {
                                    
                                }else if userStatus == "3" {
                                    
                                    AppDelegate.sharedAppDelegate().appController?.loadThankForSigning(response: data as Dictionary<String, AnyObject>)
                                    
                                } else if userStatus == "0" {
                                    
                                    self.simpleAlertWithMessage(message: "OOPS! This company user is blocked or disabled. Please contact to administration.")
                                }else {
                                   self.simpleAlertWithMessage(message: "OOPS! This company user is blocked or disabled. Please contact to administration.")
                                }
                                
                            }else{
                               
                                AppDelegate.sharedAppDelegate().appController?.loadVerifyEmailView(response: data as Dictionary<String, AnyObject>)
                            }
                        }
                    }
                    else
                    {
                        if jsonResponse?["response"].array?.count == 0 {
                            
                            self.simpleAlertWithMessage(message: "OOPS! License number is not valid.")
                        }else {
                            
                            self.simpleAlertWithMessage(message: "OOPS! Error while processing your request. Please try again.")
                        }
                    }
                }
            }
        }
    }
}

//Extention For TextField Delegates
extension SignUpStep1: UITextFieldDelegate {
    
    //TextField Delegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
    
        return true
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        return true
    }
}
