//
//  SignUpStep2.swift
//  OnlineCCProject
//
//  Created by apple on 9/9/18.
//  Copyright © 2018 CheapSender. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import CountryList

class SignUpStep2: BaseViewController {

    @IBOutlet weak var lbCompanyName: UILabel!
    @IBOutlet weak var lbEmail: UILabel!
    
    @IBOutlet weak var errorHintUsernameLabel: UILabel!

    
    @IBOutlet weak var tfFirstName: FloatLabelTextField!
    @IBOutlet weak var tfLastName: FloatLabelTextField!
    @IBOutlet weak var tfUserName: FloatLabelTextField!
    @IBOutlet weak var tfPassword: FloatLabelTextField!
    @IBOutlet weak var tfConfirmPassword: FloatLabelTextField!
    @IBOutlet weak var tfMobileNumber: FloatLabelTextField!
    @IBOutlet weak var tfMCode: FloatLabelTextField!
    
    var countryList = CountryList()
    
    var dictionaryData: Dictionary<String, AnyObject> = [:]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        prepareUI()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func prepareUI()
    {
        
        countryList.delegate = self
        
        if let compName = dictionaryData["name_en"] {
            
            lbCompanyName.text = compName as? String;
        }
        
        if let email = dictionaryData["pic_email"] {
            
            lbEmail.text = email as? String;
        }
    }
    
// MARK: - IBActions
    
    @IBAction func signUpTapped(_ sender: Any) {
        
        if !Reachability.isConnectedToNetwork(){
            self.simpleAlertWithMessage(message: "OOPS! Please check your internet connection properly.")
            return
        }
        
        willValidate()
    }
    
    //Validation
    func willValidate() {
        
        if ((tfFirstName.text?.isEmpty)!) {
            
            self.simpleAlertWithMessage(message: "OOPS! Please enter the First Name.")
        } else if ((tfLastName.text?.isEmpty)!) {
            
            self.simpleAlertWithMessage(message: "OOPS! Please enter the Last Name.")
        } else if ((tfUserName.text?.isEmpty)!) {
            
            self.simpleAlertWithMessage(message: "OOPS! Please enter the User Name.")
        } else if ((tfPassword.text?.isEmpty)!) {
            
            self.simpleAlertWithMessage(message: "OOPS! Please enter the Password.")
        } else if ((tfConfirmPassword.text?.isEmpty)!) {
            
            self.simpleAlertWithMessage(message: "OOPS! Please enter the Confirm Password.")
        } else if ((tfMobileNumber.text?.isEmpty)!) {
            
            self.simpleAlertWithMessage(message: "OOPS! Please enter the Mobile Number.")
        } else if tfPassword.text != tfConfirmPassword.text {
            
            self.simpleAlertWithMessage(message: "OOPS! Password and Confirm Password should same.")
        }
        else if (tfUserName.text?.count)! < 4 {
            
            self.simpleAlertWithMessage(message: "OOPS! Username should be more than 3 characters.")
        }
        else {
            
            requestForSignUp()
        }
    }
    
    func requestForSignUp()
    {
        showProgress(title: "Please wait...")
        
        //Params
        let parameters: Parameters = [
            .compId:dictionaryData["ID"] ?? "abc",
            .email:dictionaryData["pic_email"] ?? "abc",
            .firstName:tfFirstName.text ?? "abc",
            .lastName:tfLastName.text ?? "abc",
            .userName:tfUserName.text ?? "abc",
            .password:tfPassword.text ?? "abc",
            .mobile:tfMCode.text! + tfMobileNumber.text!]
        
        // Move to a background thread to do some long running work
        DispatchQueue.global(qos: .userInitiated).async {
            
            WebServicesClient.postRequest(parameters: parameters, methodName: .SIGN_UP, controller: .Index) { (_ jsonResponse:JSON?) in
                
                // Bounce back to the main thread to update the UI
                DispatchQueue.main.async {
                    
                    self.dismissProgress()
                    
                    guard jsonResponse != nil else {
                        self.simpleAlertWithMessage(message: "OOPS! Something went wrong")
                        return
                    }
                    print(jsonResponse ?? "Dictionary is nil")
                    
                    if jsonResponse?["error"].int == 0 {
                        
                        self.errorHintUsernameLabel.isHidden = true

                        guard let data = jsonResponse?["response"].string else {
                            self.simpleAlertWithMessage(message: "OOPS! Something went wrong")
                            return
                        }
                        print(data)
                        AppDelegate.sharedAppDelegate().appController?.loadThankForSigning(response: self.dictionaryData)
                    }
                    else
                    {
                        guard let data = jsonResponse?["response"].dictionaryObject else {
                            return
                        }
                        self.errorHintUsernameLabel.isHidden = false
                        self.errorHintUsernameLabel.textColor = .BLOCKED_COLOR
                        self.errorHintUsernameLabel.text = data["user_login"] as? String
                    }
                }
            }
        }
    }
    
    func requestForCheckUser() {
        
        self.errorHintUsernameLabel.isHidden = true
        
        //Post
        let parameters: Parameters = [
            .username : tfUserName.text ?? "test"]
        
        // Move to a background thread to do some long running work
        DispatchQueue.global(qos: .userInitiated).async {
            
            WebServicesClient.postRequest(parameters: parameters, methodName: .CHECK_USER, controller: .Index) { (_ jsonResponse:JSON?) in
                
                // Bounce back to the main thread to update the UI
                DispatchQueue.main.async {
                    
                    guard jsonResponse != nil else {
                        return
                    }
                    
                    if jsonResponse?["error"].int == 0 {
                        
                        guard let data = jsonResponse?["response"].string else {
                            return
                        }
                         self.errorHintUsernameLabel.isHidden = false
                        self.errorHintUsernameLabel.textColor = .ACTIVE_COLOR
                         self.errorHintUsernameLabel.text = data
                        
                        if self.tfUserName.text == "" {
                            self.errorHintUsernameLabel.isHidden = true
                        }
                        
                    }
                    else
                    {
                        print(jsonResponse ?? "Error")
                        guard let data = jsonResponse?["response"].string else {
                            return
                        }
                        self.errorHintUsernameLabel.isHidden = false
                        self.errorHintUsernameLabel.textColor = .BLOCKED_COLOR
                        self.errorHintUsernameLabel.text = data
                    }
                }
            }
        }
    }
}

//Extention For TextField Delegates
extension SignUpStep2: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField == tfMCode {
            
            openCountryList()
            textField.resignFirstResponder()
        }
    }
    
    func openCountryList() {
        
        let navController = UINavigationController(rootViewController: countryList)
        navController.navigationBar.tintColor = .white
        navController.navigationBar.barTintColor = .BORDER_COLOR
        
        let textAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white]
        navController.navigationBar.titleTextAttributes = textAttributes
        
        self.present(navController, animated: true, completion: nil)
    }
    
    //TextField Delegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let updatedString = (textField.text as NSString?)?.replacingCharacters(in: range, with: string)
        
        if textField == tfUserName {
            
            self.errorHintUsernameLabel.isHidden = true
            if (textField.text?.count)! > 2 {
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                    
                    self.requestForCheckUser()
                }

                if updatedString == "" {
                }
            }
        }
        return true
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        return true
    }
}

extension SignUpStep2: CountryListDelegate {
    
    func selectedCountry(country: Country) {
        
        tfMCode.text = "+"+country.phoneExtension
    }
}
