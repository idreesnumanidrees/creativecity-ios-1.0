//
//  VerifyEmailView.swift
//  OnlineCCProject
//
//  Created by apple on 9/11/18.
//  Copyright © 2018 CheapSender. All rights reserved.
//

import UIKit

class VerifyEmailView: BaseViewController {
    
    @IBOutlet weak var lblCompanyName: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    
    @IBOutlet weak var tfEmail: FloatLabelTextField!
    
    var dictionaryData: Dictionary<String, AnyObject> = [:]

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        prepareUI()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    func prepareUI() {
        
        if let compName = dictionaryData["name_en"] {
            
            lblCompanyName.text = compName as? String;
        }
        
        if let email = dictionaryData["pic_email_hiden"] {
            
            lblEmail.text = email as? String;
        }
    }
    
    //MARK: -IBActions
    @IBAction func verifyTapped(_ sender: Any) {
        
        
        if let email = dictionaryData["pic_email"] {
            
            let enteredEmail = tfEmail.text
            
            guard email as? String == enteredEmail else {
                
                self.simpleAlertWithMessage(message: "Sorry, email is not same.")
                return
            }
            AppDelegate.sharedAppDelegate().appController?.loadSignUpStep2View(response: dictionaryData)
        }
    }
}
