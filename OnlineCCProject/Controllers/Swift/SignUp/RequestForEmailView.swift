//
//  RequestForEmailView.swift
//  OnlineCCProject
//
//  Created by apple on 9/9/18.
//  Copyright © 2018 CheapSender. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import CountryList

class RequestForEmailView: BaseViewController {
    
    @IBOutlet weak var lbCompanyName: UILabel!
    
    @IBOutlet weak var errorHintUsernameLabel: UILabel!

    @IBOutlet weak var tfFirstName: FloatLabelTextField!
    @IBOutlet weak var tfLastName: FloatLabelTextField!
    @IBOutlet weak var tfUsername: FloatLabelTextField!
    @IBOutlet weak var tfEmail: FloatLabelTextField!
    @IBOutlet weak var tfMobileNumber: FloatLabelTextField!
    @IBOutlet weak var tfMCode: FloatLabelTextField!
    
    var dictionaryData: Dictionary<String, AnyObject> = [:]

    var countryList = CountryList()
    
    @IBOutlet weak var btnSendRequest: Button!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        prepareUI()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func prepareUI()
    {
        
        countryList.delegate = self
        if let compName = dictionaryData["name_en"] {
            
            lbCompanyName.text = compName as? String;
        }
    }
    
    // MARK: - IBActions
    @IBAction func sendRequestTapped(_ sender: Any) {
        
        if !Reachability.isConnectedToNetwork(){
            self.simpleAlertWithMessage(message: "OOPS! Please check your internet connection properly.")
            return
        }
        
        willValidate()
    }
    
    //Validation
    func willValidate() {
        
        if ((tfFirstName.text?.isEmpty)!) {
            
            self.simpleAlertWithMessage(message: "OOPS! Please enter the First Name.")
        } else if ((tfLastName.text?.isEmpty)!) {
            
            self.simpleAlertWithMessage(message: "OOPS! Please enter the Last Name.")
        } else if ((tfUsername.text?.isEmpty)!) {
            
            self.simpleAlertWithMessage(message: "OOPS! Please enter the User Name.")
        } else if ((tfEmail.text?.isEmpty)!) {
            
            self.simpleAlertWithMessage(message: "OOPS! Please enter the Email.")
            
        } else if !UtilHelper.validateEmail(withEmail: tfEmail.text!) {
            
            self.simpleAlertWithMessage(message: "OOPS! Please enter the valid Email.")
        } else if (tfUsername.text?.count)! < 4 {
            
            self.simpleAlertWithMessage(message: "OOPS! Username should be more than 3 characters.")
        }
        else {
            
            requestForEmail()
        }
    }
    
    func requestForEmail()
    {
        showProgress(title: "Please wait...")
        
        //Params
        let parameters: Parameters = [
            .compId:dictionaryData["ID"] ?? "abc",
            .email:tfEmail.text ?? "wrongemail",
            .firstName:tfFirstName.text ?? "abc",
            .lastName:tfLastName.text ?? "abc",
            .userName:tfUsername.text ?? "abc",
            .mobile:tfMCode.text! + tfMobileNumber.text!]
        
        // Move to a background thread to do some long running work
        DispatchQueue.global(qos: .userInitiated).async {
            
            WebServicesClient.postRequest(parameters: parameters, methodName: .REQUEST_USER_EMAIL, controller: .Index) { (_ jsonResponse:JSON?) in
                
                // Bounce back to the main thread to update the UI
                DispatchQueue.main.async {
                    
                    self.dismissProgress()
                    
                    guard jsonResponse != nil else {
                        self.simpleAlertWithMessage(message: "OOPS! Something went wrong")
                        return
                    }
                    print(jsonResponse ?? "Dictionary is nil")
                    
                    if jsonResponse?["error"].int == 0 {
                        
                        self.errorHintUsernameLabel.isHidden = true
                        
                        guard let data = jsonResponse?["response"].string else {
                            self.simpleAlertWithMessage(message: "OOPS! Something went wrong")
                            return
                        }
                        
                        self.alertWithMessage(message: data)
                    }
                    else
                    {
                        guard let data = jsonResponse?["response"].string else {
                            return
                        }
                        self.errorHintUsernameLabel.isHidden = false
                        self.errorHintUsernameLabel.textColor = .BLOCKED_COLOR
                        self.errorHintUsernameLabel.text = data
                    }
                }
            }
        }
    }
    
    func requestForCheckUser() {
        
        self.errorHintUsernameLabel.isHidden = true
        
        //Post
        let parameters: Parameters = [
            .username : tfUsername.text ?? "test"]
        
        // Move to a background thread to do some long running work
        DispatchQueue.global(qos: .userInitiated).async {
            
            WebServicesClient.postRequest(parameters: parameters, methodName: .CHECK_USER, controller: .Index) { (_ jsonResponse:JSON?) in
                
                // Bounce back to the main thread to update the UI
                DispatchQueue.main.async {
                    
                    guard jsonResponse != nil else {
                        return
                    }
                    
                    if jsonResponse?["error"].int == 0 {
                        
                        guard let data = jsonResponse?["response"].string else {
                            return
                        }
                        
                        self.btnSendRequest.isEnabled = true
                        self.errorHintUsernameLabel.isHidden = false
                        self.errorHintUsernameLabel.textColor = .ACTIVE_COLOR
                        self.errorHintUsernameLabel.text = data
                        
                        if self.tfUsername.text == "" {
                            self.errorHintUsernameLabel.isHidden = true
                        }
                    }
                    else
                    {
                        print(jsonResponse ?? "Error")
                        guard let data = jsonResponse?["response"].string else {
                            return
                        }
                        self.btnSendRequest.isEnabled = false
                        self.errorHintUsernameLabel.isHidden = false
                        self.errorHintUsernameLabel.textColor = .BLOCKED_COLOR
                        self.errorHintUsernameLabel.text = data
                    }
                }
            }
        }
    }
}

//Extention For TextField Delegates
extension RequestForEmailView: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField == tfMCode {
            
            openCountryList()
            textField.resignFirstResponder()
        }
    }
    
    func openCountryList() {
        
        let navController = UINavigationController(rootViewController: countryList)
        navController.navigationBar.tintColor = .white
        navController.navigationBar.barTintColor = .BORDER_COLOR
        
        let textAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white]
        navController.navigationBar.titleTextAttributes = textAttributes
        
        self.present(navController, animated: true, completion: nil)
    }
    
    //TextField Delegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let updatedString = (textField.text as NSString?)?.replacingCharacters(in: range, with: string)
        
        if textField == tfUsername {
            
            self.errorHintUsernameLabel.isHidden = true
            if (textField.text?.count)! > 2 {
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                    
                    self.requestForCheckUser()
                }
                
                if updatedString == "" {
                }
            }
        }
        return true
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        return true
    }
}

extension RequestForEmailView: CountryListDelegate {
    
    func selectedCountry(country: Country) {
        
        tfMCode.text = "+"+country.phoneExtension
    }
}

