//
//  LoginView.swift
//  OnlineCCProject
//
//  Created by Idrees on 2017/10/11.
//  Copyright © 2017 CheapSender. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import LocalAuthentication

//Model Object Layer
struct User : Decodable {
    
    let ID: String
    let email: String
    let sessionId: String
    let displayName: String
    let loginType: String
    let userStatus: String
    let userFirstName: String
    let userLastName: String
    
    init(json: [String:Any]) {
        
        ID = json[.ID] as? String ?? "-1"
        email = json[.USER_EMAIL] as? String ?? "demo@etfz.ae";
        sessionId = json[.SESSION_ID] as? String ?? "phiqjqjtb80ni9ec1c5066e1d6"
        displayName = json[.DISPLAY_NAME] as? String ?? "Demo"
        loginType = json[.USER_LOGIN] as? String ?? "admin"
        userStatus = json[.USER_STATUS] as? String ?? "-1"
        userFirstName = json[.USER_FIRST_NAME] as? String ?? "Demo"
        userLastName = json[.USER_LAST_NAME] as? String ?? "Demo"
    }
}

class LoginView: BaseViewController {
    
    @IBOutlet weak var emailTF: FloatLabelTextField!
    @IBOutlet weak var passwordTF: FloatLabelTextField!
    @IBOutlet weak var loginButton: UIButton!
    
    @IBOutlet weak var rememberMeButton: Button!
    
    var textFieldArray = [FloatLabelTextField]()
    
    //Outer Views
    @IBOutlet weak var emailOuterView: UIImageView!
    @IBOutlet weak var passwordOuterView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        prepareUI()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func prepareUI() {
        
        showNavigationBar()
        
        emailOuterView = UtilHelper.makeCornerCurved(withView: emailOuterView, withRadius: 5, withWidth: 0.5, withColor:.BORDER_COLOR) as? UIImageView
        passwordOuterView = UtilHelper.makeCornerCurved(withView: passwordOuterView, withRadius: 5, withWidth: 0.5, withColor:.BORDER_COLOR) as? UIImageView
        
        //  loginButton.setAttributedTitle(NSAttributedString.setFontWithSize(20.0, withWebFontText:"\\uf579" ), for: .normal)
        
        loginButton.isEnabled = false
        loginButton.backgroundColor = .gray
        
        textFieldArray = [emailTF, passwordTF]
        
        let isRemembered = UtilHelper.getValueFromNSUserDefault(forkey: "isRemembered") as? String
        if isRemembered == "1" {
            
            emailTF.text = UtilHelper.getValueFromNSUserDefaultForSignUp(forkey: "username") as? String
            passwordTF.text = UtilHelper.getValueFromNSUserDefaultForSignUp(forkey: "password") as? String
            
            rememberMeButton.isSelected = true
            
//            let touchIDAuth : TouchID = TouchID()
//            touchIDAuth.touchIDReasonString = "To access the app."
//            touchIDAuth.delegate = self
//            touchIDAuth.touchIDAuthentication()
            
            biometricAuthentication()
        }
        willEnableLoginButton("JustForTest");
        
        //Will remove in future
        //        testUser()
    }
    
    private func biometricAuthentication() {
        
        let context = LAContext()
        var error: NSError?
        
        if context.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &error) {
            let reason = "Biometric Authentication"
            
            context.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, localizedReason: reason) {
                [weak self] success, authenticationError in
                
                DispatchQueue.main.async {
                    if success {
                    
                        self!.requestForLogin()
                        
                    } else {
                        // error
                    }
                }
            }
        } else {
            // no biometry
        }
    }
    
    func testUser()  {
        
        emailTF.text = "ccdemo"
        passwordTF.text = "ccdemo"
        willEnableLoginButton("JustForTest");
    }
    
    //MARK: --IBActions
    
    @IBAction func rememberMeTapped(_ sender: Any) {
        
        rememberMeButton.isSelected = !rememberMeButton.isSelected
    }
    
    @IBAction func forgotPasswordTapped(_ sender: Any) {
        
        AppDelegate.sharedAppDelegate().appController?.loadResetPasswordView()
    }
    
    @IBAction func loginTapped(_ sender: Any) {
        
        if !Reachability.isConnectedToNetwork(){
            self.simpleAlertWithMessage(message: "OOPS! Please check your internet connection properly.")
            return
        }
        requestForLogin()
    }
    
    private func requestForLogin()
    {
        showProgress(title: "Logging...")
        
        //Params
        let parameters: Parameters = [
            .username: emailTF.text ?? "test",
            .password: passwordTF.text ?? "test",
            .deviceToken: AppDelegate.sharedAppDelegate().iosDeviceToken ?? "-1",
            .appVersion:"1.0",
            .deviceType:"ios"]
        
        // Move to a background thread to do some long running work
        DispatchQueue.global(qos: .userInitiated).async {
            
            WebServicesClient.postRequest(parameters: parameters, methodName: .LOGIN, controller: .Index) { (_ jsonResponse:JSON?) in
                
                // Bounce back to the main thread to update the UI
                DispatchQueue.main.async {
                    
                    self.dismissProgress()
                    
                    guard jsonResponse != nil else {
                        self.simpleAlertWithMessage(message: "OOPS! Something went wrong")
                        return
                    }
                    print(jsonResponse ?? "Dictionary is nil")
                    
                    if jsonResponse?["error"].int == 0 {
                        
                        guard let data = jsonResponse?["response"].dictionaryObject else {
                            self.simpleAlertWithMessage(message: "OOPS! Something went wrong")
                            return
                        }
                        
                        let user = User(json: data)
                        
                        UtilHelper.saveAndUpdateNSUserDefault(withObject: user.ID, forKey: "ID")
                        UtilHelper.saveAndUpdateNSUserDefault(withObject: user.displayName, forKey: "displayName")
                        UtilHelper.saveAndUpdateNSUserDefault(withObject: user.sessionId, forKey: "mrSessId")
                        UtilHelper.saveAndUpdateNSUserDefault(withObject: user.userFirstName, forKey: "user_fname")
                        UtilHelper.saveAndUpdateNSUserDefault(withObject: user.userLastName, forKey: "user_lname")
                        UtilHelper.saveAndUpdateNSUserDefault(withObject: user.email, forKey: "user_email")
                        UtilHelper.saveAndUpdateNSUserDefault(withObject: self.emailTF.text ?? "", forKey: "username")
                        UtilHelper.saveAndUpdateNSUserDefault(withObject: self.passwordTF.text ?? "", forKey: "password")
                        
                        if self.rememberMeButton.isSelected {
                            
                            UtilHelper.saveAndUpdateNSUserDefault(withObject: "1", forKey: "isRemembered")
                            
                        } else {
                           
                            UtilHelper.saveAndUpdateNSUserDefault(withObject: "-1", forKey: "isRemembered")
                        }
                        
                        AppDelegate.sharedAppDelegate().appController?.loadTabbar()
                    }
                    else
                    {
                        print(jsonResponse ?? "Error")
                        self.simpleAlertWithMessage(message: "OOPS! Please enter correct credentials")
                    }
                }
            }
        }
    }
    
    func willEnableLoginButton(_ updatedString: String?) {
        
        var isActive:Bool = false
        
        for tfTextField: FloatLabelTextField in textFieldArray {
            
            if (tfTextField.text?.isEmpty)! {
                
                isActive = false
                
            } else {
                
                isActive = true
                if (updatedString?.isEmpty)! {
                    
                    isActive = false
                }
            }
        }
        
        if isActive == false {
            loginButton.isEnabled = false
            loginButton.backgroundColor = .gray
        } else {
            loginButton.isEnabled = true
            loginButton.backgroundColor = .BORDER_COLOR
        }
    }
    
    //TouchView
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        self.view.endEditing(true)
    }
}

//Extention For TextField Delegates
extension LoginView: UITextFieldDelegate {
    
    //TextField Delegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let updatedString = (textField.text as NSString?)?.replacingCharacters(in: range, with: string)
        willEnableLoginButton(updatedString);
        return true
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        
        loginButton.isEnabled = false
        loginButton.backgroundColor = .gray
        return true
    }
}

//extension LoginView:TouchIDDelegate {
//
//    func touchIDAuthenticationWasSuccessful() {
//
//        requestForLogin()
//    }
//
//    func touchIDAuthenticationFailed(errorCode: TouchIDErrorCode) {
//
//    }
//}

