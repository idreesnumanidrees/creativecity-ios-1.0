//
//  HomeCompanyView.swift
//  OnlineCCProject
//
//  Created by Idrees on 2017/10/12.
//  Copyright © 2017 CheapSender. All rights reserved.
//

import UIKit
import KRPullLoader
import Alamofire
import SVProgressHUD
import SwiftyJSON

class HomeCompanyView: BaseViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var homeTableView: UITableView!
    @IBOutlet weak var noInternetError: UILabel!
    
    var companiesArray: [Any] = []
    
    var tableCellheight = 1371.0
    
    //KRPullLoadView
    let refreshView = KRPullLoadView()
    
    var refreshControl = UIRefreshControl()
    
    var fiveComplete:Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Do any additional setup after loading the view.
        prepareUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        UIApplication.shared.statusBarStyle = .lightContent
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    func prepareUI() {
        
        noInternetError.isHidden = true
        showProgress(title: "")
        requestForCompanies()
        
        refreshTableSetup()
        
        if AppDelegate.sharedAppDelegate().licOptions == nil {
            
            requestForLicOptions()
        }
    }
    
    func refreshTableSetup() {
        
        refreshView.delegate = self
        homeTableView.addPullLoadableView(refreshView, type: .refresh)
    }
    
    //MARK: IBActions
    //
    //    @IBAction func moreButtonTapped(_ sender: Any) {
    //
    //        AppDelegate.sharedAppDelegate().appController?.loadOtherView()
    //    }
    
    //MARK: UItableView datasource and delegate
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return 8.0;
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return companiesArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellIdentifier = "HomeCell"
        
        guard let tableviewcell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? HomeCell else {
            
            fatalError("The dequeued cell is not instance of HomeCell.")
        }
        
        let company = companiesArray[indexPath.row] as! Dictionary<String, AnyObject>
        
        tableviewcell.setupForInitial(tag: indexPath.row, company: company)
        
        if let imgCard = company["card"]  {
            tableCellheight = 1371.0
        } else {
            tableCellheight = 747.0
        }
        
        //Each
        
        //        if let visa = company["useage"]  {
        //
        //            tableCellheight = 1318
        //
        //        } else {
        //
        //            tableCellheight = 694
        //        }
        
        
        
        return tableviewcell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if tableCellheight < 0.0 {
            
            tableCellheight = 0.0
        }
        return CGFloat(tableCellheight)
    }
    
    //MARK: Server Handling
    func requestForCompanies() {
        
        if !Reachability.isConnectedToNetwork(){
            dismissProgress()
            self.noInternetError.isHidden = false
            self.noInternetError.text = "OOPS! No Internet Connection. Please connect with internet and pull to refresh."
            return
        }
        
        self.noInternetError.isHidden = true
        let sessionId = UtilHelper.getValueFromNSUserDefault(forkey: "mrSessId")
        //Params
        let parameters: Parameters = [
            "mrSessId": sessionId]
        
        // Move to a background thread to do some long running work
        DispatchQueue.global(qos: .userInitiated).async {
            
            WebServicesClient.postRequest(parameters: parameters, methodName: .COMPANIES, controller:"") { (_ jsonResponse:JSON?) in
                
                // Bounce back to the main thread to update the UI
                DispatchQueue.main.async {
                    
                    self.dismissProgress()
                    
                    guard jsonResponse != nil else {
                        return
                    }
                    print(jsonResponse ?? "Dictionary is nil")
                    
                    if jsonResponse?["error"].int == 0 {
                        
                        guard let data = jsonResponse?["response"].arrayObject else {
                            return
                        }
                        
                        self.companiesArray = data
                        
                        if self.companiesArray.count == 0 {
                            
                            self.noInternetError.isHidden = false
                            self.noInternetError.text = "Something went wrong!"
                        } else {
                            
                            self.noInternetError.isHidden = true
                            self.homeTableView.reloadData()
                        }
                    }
                    else
                    {
                        UtilHelper.saveAndUpdateNSUserDefault(withObject: "-1", forKey: "ID")
                        var isPresentView:Bool = false
                        
                        for controller in self.navigationController!.viewControllers as Array {
                            if controller.isKind(of: StartupView.self) {
                                self.navigationController!.popToViewController(controller, animated: true)
                                isPresentView = true
                                break
                            }else{
                                isPresentView = false
                            }
                        }
                        
                        if isPresentView == false {
                            AppDelegate.sharedAppDelegate().appController?.loadStartUpView()
                        }
                        
                        //                        print(jsonResponse ?? "Error")
                    }
                }
            }
        }
    }
}

// MARK: - KRPullLoadView delegate -------------------

extension HomeCompanyView: KRPullLoadViewDelegate {
    
    func pullLoadView(_ pullLoadView: KRPullLoadView, didChangeState state: KRPullLoaderState, viewType type: KRPullLoaderType) {
        if type == .loadMore {
            switch state {
            case let .loading(completionHandler):
                completionHandler()
            default: break
            }
            return
        }
        
        switch state {
        case .none:
            pullLoadView.messageLabel.text = ""
            
        case let .pulling(offset, threshould):
            if offset.y > threshould {
                pullLoadView.messageLabel.text = "Starting update..."
            } else {
                pullLoadView.messageLabel.text = "Starting update..."
            }
            
        case let .loading(completionHandler):
            
            pullLoadView.messageLabel.text = "Updating..."
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+1) {
                self.requestForCompanies()
                completionHandler()
            }
        }
    }
}
