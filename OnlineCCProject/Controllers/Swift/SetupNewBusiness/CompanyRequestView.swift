//
//  CompanyRequestView.swift
//  OnlineCCProject
//
//  Created by Idrees on 2017/10/11.
//  Copyright © 2017 CheapSender. All rights reserved.
//

import UIKit
import GrowingTextView
import Alamofire
import SVProgressHUD
import SwiftyJSON
import RSSelectionMenu
import DropDown

class CompanyRequestView: BaseViewController, UIGestureRecognizerDelegate {
    
    @IBOutlet weak var companyNameTF: FloatLabelTextField!
    @IBOutlet weak var selectPackageTF: FloatLabelTextField!
    @IBOutlet weak var companyTypeFT: FloatLabelTextField!
    
    @IBOutlet weak var commentsView: GrowingTextView!
    @IBOutlet weak var activitySelectionView: GrowingTextView!
    
    @IBOutlet weak var requestButton: UIButton!
    @IBOutlet weak var companyTypeButton: UIButton!
    
    @IBOutlet weak var outerViewOfFields: UIView!
    @IBOutlet weak var container: UIView!
    
    @IBOutlet weak var errorHintLabel: UILabel!
    
    @IBOutlet weak var companyNameOuterView: ImageView!
    
    @IBOutlet weak var companyTypeView: View!
    
    var textFieldArray = [FloatLabelTextField]()
    
    var firstRowSelected = true
    
    let dropDown = DropDown()
    
    var companyTypesArray = [String]()
    
    //    let activitiesArray = ["Actual office space - upto 12 visas", "Baby Business upto 6 Visas (Flexi Desk)", "Freelancer Company upto 3 Visas (Flexi Desk)", "Commercial License - (Multi Owner) - no visas available (Flexi Desk)", "Suresh Raina", "Ravindra Jadeja", "Commercial License - (Signle Owner) - no visas available (Flexi Desk)"]
    //    var selectedActivitiesArray = [String]()
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        prepareUI()
    }
    
    func prepareUI() {
        
        commentsView = UtilHelper.makeCornerCurved(withView: commentsView, withRadius: 5, withWidth: 0.3, withColor: .BORDER_COLOR) as? GrowingTextView
        activitySelectionView = UtilHelper.makeCornerCurved(withView: activitySelectionView, withRadius: 5, withWidth: 0.3, withColor: .BORDER_COLOR) as? GrowingTextView
        
        requestButton.isEnabled = false
        requestButton.backgroundColor = .gray
        
        textFieldArray = [companyNameTF, selectPackageTF, companyTypeFT]
        
        if AppDelegate.sharedAppDelegate().licOptions == nil {
            
        } else {
            
            //Getting Company Types
            let companyTypes = AppDelegate.sharedAppDelegate().licOptions!["types"] as! [Dictionary<String, String>]
            for dict in companyTypes {
                
                companyTypesArray.append(dict["title"]!)
            }
        }
        
        //Company Type View
        dropDown.anchorView = companyTypeView
        dropDown.dataSource = companyTypesArray
        
        DropDown.startListeningToKeyboard()
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            self.companyTypeFT.text = item
            self.willEnableLoginButton("update")
        }
        
        //Adding tap gesture to container view
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        tap.delegate = self
        container.addGestureRecognizer(tap)
    }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer) {
        // handling code
        self.view.endEditing(true)
    }
    
    //IBActions
    @IBAction func requestTapped(_ sender: Any) {
        
        self.view.endEditing(true)
        requestForSendInquiry()
    }
    
    @IBAction func selectPackageTapped(_ sender: Any) {
        
        self.view.endEditing(true)
        AppDelegate.sharedAppDelegate().appController?.loadPackagesView(controller: self, isFromPackages: true)
    }
    
    @IBAction func companyTypeTapped(_ sender: Any) {
        
        dropDown.show()
    }
    
    @IBAction func activitySelectionTapped(_ sender: Any) {
        
        AppDelegate.sharedAppDelegate().appController?.loadActivitiesView(controller: self)
        
        return
        
        //
        //        // Show menu with datasource array - SelectionType = Multiple, CellType = SubTitle & SearchBar
        //
        //        let selectionMenu =  RSSelectionMenu(selectionType: .Multiple, dataSource: activitiesArray, cellType: .Basic) { (cell, object, indexPath) in
        //
        //            // set firstname in title and lastname as subTitle
        //
        //            let firstName = object
        //            //            let lastName = object.components(separatedBy: " ").last
        //
        //            cell.textLabel?.text = firstName
        //            //            cell.detailTextLabel?.text = lastName
        //        }
        //
        //        selectionMenu.setSelectedItems(items: selectedActivitiesArray) { (text, selected, selectedItems) in
        //            self.selectedActivitiesArray = selectedItems
        //
        //            let stringRepresentation = selectedItems.joined(separator: ", ")
        //            self.activitySelectionView.text = stringRepresentation
        //        }
        //
        //        // To show first row as All, when dropdown as All selected by default
        //        // Here you'll get Text and isSelected when user selects first row
        //
        //        selectionMenu.addFirstRowAs(rowType: .None, showSelected: self.firstRowSelected) { (text, isSelected) in
        //            self.firstRowSelected = isSelected
        //        }
        //
        //        // show searchbar
        //        // Here you'll get search text - when user types in seachbar
        //        selectionMenu.showSearchBar { (searchtext) -> ([String]) in
        //            return self.activitiesArray.filter({ $0.lowercased().hasPrefix(searchtext.lowercased()) })
        //        }
        //
        //        let textAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white]
        //
        //        // right barbutton title
        //        selectionMenu.rightBarButtonTitle = "Done"
        //
        //        // set navigationbar theme
        //        selectionMenu.setNavigationBar(title: "Select Activities", attributes: textAttributes, barTintColor: .BORDER_COLOR, tintColor: UIColor.white)
        //
        //        // show as default
        //        selectionMenu.show(from: self)
    }
    
    //MARK: Server Side Handling
    func postRequestForNameCheck() {
        
        self.errorHintLabel.isHidden = true
        
        //Post
        let parameters: Parameters = [
            .name : companyNameTF.text ?? "test"]
        
        // Move to a background thread to do some long running work
        DispatchQueue.global(qos: .userInitiated).async {
            
            WebServicesClient.postRequest(parameters: parameters, methodName: .COMP_NAME_CHECK, controller: .Index) { (_ jsonResponse:JSON?) in
                
                // Bounce back to the main thread to update the UI
                DispatchQueue.main.async {
                    
                    guard jsonResponse != nil else {
                        self.simpleAlertWithMessage(message: "OOPS! Something went wrong")
                        return
                    }
                    
                    if jsonResponse?["error"].int == 0 {
                        
                        guard let data = jsonResponse?["response"].dictionaryObject else {
                            return
                        }
                        guard let summary = data["summary"] as? String else {
                            
                            return
                        }
                        
                        if summary == "notavailable" {
                            
                            self.errorHintLabel.text = "Oh snap! Name not Available"
                            self.errorHintLabel.isHidden = false
                            self.errorHintLabel.textColor = UIColor(red: 238/255, green: 223/255, blue: 222/255, alpha: 1.0)
                            self.companyNameOuterView.layer.borderColor = UIColor(red: 238/255, green: 223/255, blue: 222/255, alpha: 1.0).cgColor
                            
                        } else if summary == "cancelled-now" {
                            
                            self.errorHintLabel.text = "Success! Company is already cancelled. You can use the name now."
                            self.errorHintLabel.isHidden = false
                            self.errorHintLabel.textColor = UIColor(red: 75/255, green: 116/255, blue: 67/255, alpha: 1.0)
                            self.companyNameOuterView.layer.borderColor = UIColor(red: 75/255, green: 116/255, blue: 67/255, alpha: 1.0).cgColor
                            
                        }  else if summary == "cancelled-date" {
                            
                            self.errorHintLabel.text = "Warning! Company is already cancelled. You can use this name after date \(String(describing: data["date"]))"
                            self.errorHintLabel.isHidden = false
                            self.errorHintLabel.textColor = UIColor(red: 133/255, green: 110/255, blue: 66/255, alpha: 1.0)
                            self.companyNameOuterView.layer.borderColor = UIColor(red: 133/255, green: 110/255, blue: 66/255, alpha: 1.0).cgColor
                            
                            
                        } else if summary == "restricted" {
                            
                            self.errorHintLabel.text = "Oh snap! Restricted name found! You cannot use the name."
                            self.errorHintLabel.isHidden = false
                            self.errorHintLabel.textColor = UIColor(red: 156/255, green: 74/255, blue: 70/255, alpha: 1.0)
                            self.companyNameOuterView.layer.borderColor = UIColor(red: 156/255, green: 74/255, blue: 70/255, alpha: 1.0).cgColor
                            
                        } else if summary == "non-action" {
                            
                            self.errorHintLabel.text = "Warning! Company name is in non-action! You can use this name after some approvales."
                            self.errorHintLabel.isHidden = false
                            self.errorHintLabel.textColor = UIColor(red: 133/255, green: 110/255, blue: 66/255, alpha: 1.0)
                            self.companyNameOuterView.layer.borderColor = UIColor(red: 133/255, green: 110/255, blue: 66/255, alpha: 1.0).cgColor
                            
                            
                        } else if summary == "available" {
                            
                            self.errorHintLabel.text = "Success! Name is available."
                            self.errorHintLabel.isHidden = false
                            self.errorHintLabel.textColor = UIColor(red: 75/255, green: 116/255, blue: 67/255, alpha: 1.0)
                            self.companyNameOuterView.layer.borderColor = UIColor(red: 75/255, green: 116/255, blue: 67/255, alpha: 1.0).cgColor
                            
                        } else if summary == "similar" {
                            
                            self.errorHintLabel.text = "Warning! We found some similar names."
                            self.errorHintLabel.isHidden = false
                            self.errorHintLabel.textColor = UIColor(red: 133/255, green: 110/255, blue: 66/255, alpha: 1.0)
                            self.companyNameOuterView.layer.borderColor = UIColor(red: 133/255, green: 110/255, blue: 66/255, alpha: 1.0).cgColor
                            
                            guard let similarValues = data["similar"] as? [String] else {
                                
                                return
                            }
                        }
                        
                        if self.companyNameTF.text == "" {
                            self.errorHintLabel.isHidden = true
                        }
                    }
                    else
                    {
                        print(jsonResponse ?? "Error")
                    }
                }
            }
        }
    }
    
    //Form Validation
    func willEnableLoginButton(_ updatedString: String?) {
        
        var isActive:Bool = false
        
        for tfTextField: FloatLabelTextField in textFieldArray {
            
            if (tfTextField.text?.isEmpty)! {
                
                isActive = false
                
            } else {
                
                isActive = true
                if (updatedString?.isEmpty)! {
                    
                    isActive = false
                }
            }
        }
        
        if isActive == false {
            requestButton.isEnabled = false
            requestButton.backgroundColor = .gray
        } else {
            requestButton.isEnabled = true
            requestButton.backgroundColor = .BORDER_COLOR
        }
    }
    
    //MARK: Server Handling
    
    func requestForSendInquiry() {
        
        if !Reachability.isConnectedToNetwork(){
            self.simpleAlertWithMessage(message: "OOPS! Please check your internet connection properly.")
            return
        }
        
        showProgress(title: "Sending...")
        
        let firstName = UtilHelper.getValueFromNSUserDefaultForSignUp(forkey: "firstName") as? String
        let lastName = UtilHelper.getValueFromNSUserDefaultForSignUp(forkey: "LastName") as? String
        let email = UtilHelper.getValueFromNSUserDefaultForSignUp(forkey: "email") as? String
        let mobile = UtilHelper.getValueFromNSUserDefaultForSignUp(forkey: "mobile") as? String
        let nationality = UtilHelper.getValueFromNSUserDefaultForSignUp(forkey: "nationality") as? String
        
        //Params
        let parameters: Parameters = [
            "firstName": firstName ?? "test",
            "lastName": lastName ?? "test",
            "email": email ?? "test",
            "mobile": mobile ?? "test",
            "nationality": nationality ?? "test",
            "companyName": companyNameTF.text ?? "test",
            "companyType": companyTypeFT.text ?? "test",
            "package": selectPackageTF.text ?? "test",
            "activities": activitySelectionView.text ?? "test",
            "summary": commentsView.text ?? "test"]
        
        // Move to a background thread to do some long running work
        DispatchQueue.global(qos: .userInitiated).async {
            
            WebServicesClient.signUp(parameters: parameters, completion: { (_ jsonResponse:JSON?) in
                
                DispatchQueue.main.async {
                    
                    self.dismissProgress()
                    
                    guard jsonResponse != nil else {
                        self.simpleAlertWithMessage(message: "OOPS! Something went wrong")
                        return
                    }
                    //                    print(jsonResponse ?? "Dictionary is nil")
                    
                    if jsonResponse?["error"].int == 0 {
                        
                        guard let data = jsonResponse?["response"].string else {
                            self.simpleAlertWithMessage(message: "OOPS! Something went wrong")
                            return
                        }
                        print(data)
                        
                        let alertController =  UIAlertController(title: "Creative City", message: "Thank you! Your request has been sent successfully. We will contact you soon.", preferredStyle: .alert)
                        alertController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { _ in
                            
                            let id = UtilHelper.getValueFromNSUserDefault(forkey: "ID") as? String
                            
                            if id == "-1" {
                                
                                for controller in self.navigationController!.viewControllers as Array {
                                    if controller.isKind(of: StartupView.self) {
                                        self.navigationController!.popToViewController(controller, animated: true)
                                        break
                                    }
                                }
                            }else {
                                
                                self.navigationController?.popViewController(animated: true)
                            }
                        }))
                        self.present(alertController, animated: true, completion: nil)
                    }
                    else
                    {
                        print(jsonResponse ?? "Error")
                        self.simpleAlertWithMessage(message: "OOPS! Please enter correct credentials")
                    }
                }
            })
        }
    }
}

//Extention For TextField Delegates
extension CompanyRequestView: UITextFieldDelegate {
    
    //TextField Delegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let updatedString = (textField.text as NSString?)?.replacingCharacters(in: range, with: string)
        willEnableLoginButton(updatedString);
        
        if textField == companyNameTF {
            
            postRequestForNameCheck()
            
            if updatedString == "" {
                self.errorHintLabel.isHidden = true
            }
        }
        return true
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        
        requestButton.isEnabled = false
        requestButton.backgroundColor = .gray
        
        return true
    }
}

extension CompanyRequestView: HADropDownDelegate {
    func didSelectItem(dropDown: HADropDown, at index: Int) {
        print("Item selected at index \(index)")
    }
}

extension CompanyRequestView:PackagesViewDelegate {
    func getPackageName(_ name: String?) {
        selectPackageTF.text = name
        willEnableLoginButton("update")
    }
}

extension CompanyRequestView:ActivitiesViewDelegate {
    
    func getActivitiesName(_ activities: [String]?) {
        
        if (activities?.count)! > 0 {
            
            let stringRepresentation = activities?.joined(separator: ", ")
            self.activitySelectionView.text = stringRepresentation
        }
    }
}
