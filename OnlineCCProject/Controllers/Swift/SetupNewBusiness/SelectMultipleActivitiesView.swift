//
//  SelectMultipleActivitiesView.swift
//  OnlineCCProject
//
//  Created by apple on 5/30/18.
//  Copyright © 2018 CheapSender. All rights reserved.
//

import UIKit

//MARK: step 1 Add Protocol here.
protocol ActivitiesViewDelegate: class {
    
    func getActivitiesName(_ activities: [String]?)
}

class SelectMultipleActivitiesView: BaseViewController {
    
    @IBOutlet weak var myTableView: UITableView!
    @IBOutlet weak var searchBox: UISearchBar!
    
    //Search related
    var searchActive : Bool = false
    
    var sections:[String] = []
    var itemActivities:[[Dictionary<String, String>]] = []
    
    var selectedDataArray:NSMutableArray = NSMutableArray()
    
    var filteredItemData:[Dictionary<String, String>] = []
    var filteredSectionData:[String] = []
    
    weak var delegate: ActivitiesViewDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        prepareUI()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        delegate?.getActivitiesName(selectedDataArray as? [String])
    }
    
    func prepareUI()  {
        
        self.navigationController?.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Add", style: .plain, target: self, action: #selector(addTapped))
        
        myTableView.rowHeight = UITableViewAutomaticDimension
        myTableView.estimatedRowHeight = 140
        
        if AppDelegate.sharedAppDelegate().licOptions != nil {
            
            let activities = AppDelegate.sharedAppDelegate().licOptions!["activies"] as! [Dictionary<String, AnyObject>]
            
            for headers in activities {
                
                //                if let child = headers["text"] as String {
                
                sections.append(headers["text"] as! String)
                
                //                }
            }
            
            //            print(sections)
            
            for items in activities {
                
                //                if let child = items["child"] as! [Dictionary<String, String>] {
                
                itemActivities.append(items["child"] as! [Dictionary<String, String>])
                //                print(itemActivities)
                
                
                //                }
            }
            
            myTableView.reloadData()
        }
    }
    
    //MARK: IBActions
    
    @objc func addTapped(_ sender:UIButton) {
        
    }
}

//Extension For UITableView DataSource and Delegate
extension SelectMultipleActivitiesView: UITableViewDelegate, UITableViewDataSource {
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return itemActivities.count
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        return sections[section]
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        let array = itemActivities[section]
        return array.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellIdentifier = "ActivityCell"
        
        guard let tableviewcell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? ActivityCell else {
            
            fatalError("The dequeued cell is not instance of OtherHomeCell.")
        }
        
        tableviewcell.selectionStyle = .none
        
        // Configure the cell...
        let activity = itemActivities[indexPath.section][indexPath.row] as AnyObject
        
        tableviewcell.lblLabel.text = activity["text"] as? String ?? ""
        
        if selectedDataArray.contains((activity["text"] as? String)!) {
            
            tableviewcell.accessoryType = .checkmark
            
        } else {
            
            tableviewcell.accessoryType = .none
        }
        
        return tableviewcell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if let cell = tableView.cellForRow(at: indexPath) {
            
            let activity = itemActivities[indexPath.section][indexPath.row] as AnyObject
            
            if selectedDataArray.contains((activity["text"] as? String)!) {
                
                selectedDataArray.remove(activity["text"] as? String ?? "")
                cell.accessoryType = .none
                
            } else {
                
                if selectedDataArray.count > 2 {
                    
                    self.simpleAlertWithMessage(message: "OOPS! You can select only 3 activities.")
                    return
                }
                cell.accessoryType = .checkmark
                selectedDataArray.add(activity["text"] as? String ?? "")
            }
        }
    }
}

//MARK: - UISearchBar Delegate.................
extension SelectMultipleActivitiesView:UISearchBarDelegate {
    
    //UISearchbox Delegates
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        
        searchActive = true;
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        
        searchActive = false;
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        
        searchActive = false;
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
        searchActive = false;
        searchBox.resignFirstResponder()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        //        var arrNames:Array = itemActivities
        //
        //        let searchArray = arrNames.filter({(($0["text"] as! [Dictionary<String, String>]).localizedCaseInsensitiveContains(searchText))!}
        //
        //        let mallNamePredicate = NSPredicate(format: "text contains %@", searchBox.text!)
        //        let filteredWithPredicate = (itemActivities as NSArray).filtered(using: mallNamePredicate)
        
        // Classical filter example
        
        //        var customerNameDict = ["firstName":"karthi","LastName":"alagu","MiddleName":"prabhu"];
        //        var clientNameDict = ["firstName":"Selva","LastName":"kumar","MiddleName":"m"];
        //        var employeeNameDict = ["firstName":"karthi","LastName":"prabhu","MiddleName":"kp"];
        //
        //        var attributeValue = "karthi";
        //
        //        var arrNames:Array = itemActivities
        //
        //        var namePredicate = NSPredicate(format: "text like %@",searchBox.text!);
        //
        //        filteredItemData = arrNames.filter { namePredicate.evaluateWithObject($0) };
        //
        //        print("names = ,\(filteredItemData)");
        //
        //
        //        let filterdata = arrNames.filter({(($0["text"] as! String).localizedCaseInsensitiveContains(searchBox.text!))!})
        
        //        if(filteredItemData.count == 0){
        //            searchActive = false
        //        }else{
        //            searchActive = true
        //        }
        //
        //        if((searchBox.text?.isEmpty)! || searchBox.text == nil){
        //            searchActive = false;
        //        } else {
        //            searchActive = true;
        //        }
    }
}


