//
//  PersonalInfoView.swift
//  OnlineCCProject
//
//  Created by apple on 5/20/18.
//  Copyright © 2018 CheapSender. All rights reserved.
//

import UIKit
import CountryList

class PersonalInfoView: BaseViewController {
    
    @IBOutlet weak var tfFirstName: FloatLabelTextField!
    @IBOutlet weak var tfLastName: FloatLabelTextField!
    @IBOutlet weak var tfEmailAddress: FloatLabelTextField!
    @IBOutlet weak var tfMobileNo: FloatLabelTextField!
    @IBOutlet weak var tfNationality: FloatLabelTextField!
    
    @IBOutlet weak var emailErrorHint: UILabel!
    @IBOutlet weak var nextButton: UIButton!

    var textFieldArray = [FloatLabelTextField]()
    
    var countryList = CountryList()

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        prepareUI()
    }
    
    func prepareUI()
    {
        
        showNavigationBar()
        
        nextButton.isEnabled = false
        nextButton.backgroundColor = .gray
        
        countryList.delegate = self
        textFieldArray = [tfFirstName, tfLastName, tfEmailAddress, tfMobileNo, tfNationality]
        
        //Getting saved Values
        tfFirstName.text = UtilHelper.getValueFromNSUserDefaultForSignUp(forkey: "firstName") as? String
        tfLastName.text = UtilHelper.getValueFromNSUserDefaultForSignUp(forkey: "LastName") as? String
        tfEmailAddress.text = UtilHelper.getValueFromNSUserDefaultForSignUp(forkey: "email") as? String
        tfMobileNo.text = UtilHelper.getValueFromNSUserDefaultForSignUp(forkey: "mobile") as? String
        tfNationality.text = UtilHelper.getValueFromNSUserDefaultForSignUp(forkey: "nationality") as? String
        
        willEnableLoginButton("updatedString");
    }
    
    @IBAction func nextStepTapped(_ sender: Any) {
        
        if UtilHelper.validateEmail(withEmail: tfEmailAddress.text!) {
            
            //Saving Values
            UtilHelper.saveAndUpdateNSUserDefault(withObject: tfFirstName.text ?? "", forKey: "firstName")
            UtilHelper.saveAndUpdateNSUserDefault(withObject: tfLastName.text ?? "", forKey: "LastName")
            UtilHelper.saveAndUpdateNSUserDefault(withObject: tfEmailAddress.text ?? "", forKey: "email")
            UtilHelper.saveAndUpdateNSUserDefault(withObject: tfMobileNo.text ?? "", forKey: "mobile")
            UtilHelper.saveAndUpdateNSUserDefault(withObject: tfNationality.text ?? "", forKey: "nationality")

            AppDelegate.sharedAppDelegate().appController?.loadCompanyRequestView()
            
        }else {
            
            emailErrorHint.isHidden = false
        }
    }
    
    //MARK: IBActions
    @IBAction func presentCountryList(_ sender: Any) {
        
        let navController = UINavigationController(rootViewController: countryList)
        navController.navigationBar.tintColor = .white
        navController.navigationBar.barTintColor = .BORDER_COLOR
        
        let textAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white]
        navController.navigationBar.titleTextAttributes = textAttributes
        
        self.present(navController, animated: true, completion: nil)
    }
    
    //Validation
    func willEnableLoginButton(_ updatedString: String?) {
        
        var isActive:Bool = false
        
        for tfTextField: FloatLabelTextField in textFieldArray {
            
            if (tfTextField.text?.isEmpty)! {
                
                isActive = false
                
            } else {
                
                isActive = true
                if (updatedString?.isEmpty)! {
                    
                    isActive = false
                }
            }
        }
        
        emailErrorHint.isHidden = true

        if isActive == false {
            nextButton.isEnabled = false
            nextButton.backgroundColor = .gray
        } else {
            nextButton.isEnabled = true
            nextButton.backgroundColor = .BORDER_COLOR
        }
    }
}

//Extention For TextField Delegates
extension PersonalInfoView: UITextFieldDelegate {
    
    //TextField Delegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let updatedString = (textField.text as NSString?)?.replacingCharacters(in: range, with: string)
        willEnableLoginButton(updatedString);
        return true
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        
        nextButton.isEnabled = false
        nextButton.backgroundColor = .gray
        return true
    }
}

extension PersonalInfoView: CountryListDelegate {
    
    func selectedCountry(country: Country) {
        
        tfNationality.text = country.name
        willEnableLoginButton("updatedString");
    }
}
