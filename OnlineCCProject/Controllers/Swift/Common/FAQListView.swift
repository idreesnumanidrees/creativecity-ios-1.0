//
//  FAQListView.swift
//  OnlineCCProject
//
//  Created by apple on 11/20/17.
//  Copyright © 2017 CheapSender. All rights reserved.
//

import UIKit
import SVProgressHUD

class FAQListView: BaseViewController, URLSessionDownloadDelegate {

    @IBOutlet weak var tableView: UITableView!
    
    var downloads: [Any] = []
    
    var downloadTask: URLSessionDownloadTask!
    var backgroundSession: URLSession!
    
    var fileName:String = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 60
        
        downloads = AppDelegate.sharedAppDelegate().licOptions!["downloads"] as! [Any]
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        //MARK: --URLSessionConfiguration
        
        let backgroundSessionConfiguration = URLSessionConfiguration.background(withIdentifier: "DownloadBackgroundSession")
        backgroundSession = Foundation.URLSession(configuration: backgroundSessionConfiguration, delegate: self, delegateQueue: OperationQueue.main)
        showNavigationBar()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        
        backgroundSession.invalidateAndCancel()
    }
    
    //MARK: URLSessionDownloadDelegate
    // 1
    func urlSession(_ session: URLSession,
                    downloadTask: URLSessionDownloadTask,
                    didFinishDownloadingTo location: URL){
        
        dismissProgress()
        let pathOfDirectory:String = FCFileManager.pathForDocumentsDirectory()
        
        let fileManager = FileManager()
        let destinationURLForFile = URL(fileURLWithPath: pathOfDirectory.appendingFormat("/\(fileName)"))
        
        do {
            try fileManager.moveItem(at: location, to: destinationURLForFile)
            // show file
            showFileWithPath(path: destinationURLForFile.path)
        }catch{
            print("An error occurred while moving file to destination url")
        }
    }
}

//MARK: Tableview Data Source and Delegates

extension FAQListView: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return downloads.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellIdentifier = "DownloadCell"
        
        guard let tableviewcell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? DownloadCell else {
            
            fatalError("The dequeued cell is not instance of HomeCell.")
        }
        
        let download = downloads[indexPath.row] as! Dictionary<String, AnyObject>
        tableviewcell.titleLabel.text = download["title"] as? String
        
        return tableviewcell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        tableView.deselectRow(at: indexPath, animated: true)

        let download = downloads[indexPath.row] as! Dictionary<String, AnyObject>
        
        let remotePDFDocumentURLPath = download["pdf"] as? String
        
        fileName = "\((download["title"] as? String)!).pdf"
        
        let path:String = FCFileManager.pathForDocumentsDirectory()
        let destinationURLForFile = URL(fileURLWithPath: path.appendingFormat("/\(fileName)"))
        
        if FCFileManager.existsItem(atPath: destinationURLForFile.path){
            showFileWithPath(path: destinationURLForFile.path)
        }
        else {
            
            showProgress(title: "Downlaoding...")
            
            let url = URL(string: remotePDFDocumentURLPath!)
            downloadTask = backgroundSession.downloadTask(with: url!)
            downloadTask.resume()
        }
    }
}
