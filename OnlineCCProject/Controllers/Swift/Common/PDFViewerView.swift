//
//  PDFViewerView.swift
//  OnlineCCProject
//
//  Created by apple on 5/14/18.
//  Copyright © 2018 CheapSender. All rights reserved.
//

import UIKit

class PDFViewerView: BaseViewController, URLSessionDownloadDelegate {

    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var navBar: UINavigationItem!
    
//    var documentPathToLoad = "https://sys.ccfz.ae:4423//webroot//dynamic//uploads//creativeCity//companies//9384//license//d-2018-05-17-u-12-lic-17-5-2108.pdf"

    var downloadTask: URLSessionDownloadTask!
    var backgroundSession: URLSession!
    
    var folderName:String?
    var fileName:String?
    var documentPathToLoad:String?
    var navTitle:String?

    var isDownload = Bool()
    
    let userId = UtilHelper.getValueFromNSUserDefault(forkey: "ID") as! String
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        prepareUI()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        
        backgroundSession.invalidateAndCancel()
    }
    
    //MARK: Helpers methods
    func prepareUI()  {
       
        showNavigationBar()
        navBar.title = navTitle

//        let url : NSURL? = NSURL(string: documentPathToLoad)
//        webView.loadRequest(URLRequest(url: url! as URL))
        
        //MARK: --URLSessionConfiguration
        let backgroundSessionConfiguration = URLSessionConfiguration.background(withIdentifier: "PDFViewewBackgroundSession")
        backgroundSession = Foundation.URLSession(configuration: backgroundSessionConfiguration, delegate: self, delegateQueue: OperationQueue.main)
        
        //Save the pdf document to file system
        savePdf()
    }
    
    func savePdf(){

        isDownload = false
        
        let pathOfDirectory:String = FCFileManager.pathForDocumentsDirectory()
        let directoryPath = pathOfDirectory.appendingFormat(folderName!)
        
        if !FCFileManager.existsItem(atPath: directoryPath) {
            FCFileManager.createDirectories(forPath: directoryPath)
        }
        
        let destinationURLForFile = URL(fileURLWithPath: pathOfDirectory.appendingFormat(folderName!+"/"+fileName!+".pdf"))
        
        if FCFileManager.existsItem(atPath: destinationURLForFile.path){
            webView.loadRequest(URLRequest(url: destinationURLForFile))
        } else {
          
            showProgress(title: "Downlaoding...")
            let url = URL(string: documentPathToLoad!)
            downloadTask = backgroundSession.downloadTask(with: url!)
            downloadTask.resume()
        }
    }

    //MARK: --IBActions
    @IBAction func shareTapped(_ sender: Any) {
        
//        let text = "This is the text...."
        let fileManager = FileManager.default
        let path:String = FCFileManager.pathForDocumentsDirectory()
        let destinationURLForFile = path.appendingFormat(folderName!+"/"+fileName!+".pdf")
        
        if fileManager.fileExists(atPath: destinationURLForFile){
            
            let url = NSURL.fileURL(withPath: destinationURLForFile, isDirectory: true)
            
            let activityViewController: UIActivityViewController = UIActivityViewController(activityItems: [url], applicationActivities: nil)
            activityViewController.popoverPresentationController?.sourceView=self.view
            present(activityViewController, animated: true, completion: nil)
        }
        else {
            print("Document was not found")
        }
    }
    
    @IBAction func downloadTapped(_ sender: Any) {
        
        isDownload = true
        
        showProgress(title: "Downloading...")
        let url = URL(string: documentPathToLoad!)
        downloadTask = backgroundSession.downloadTask(with: url!)
        downloadTask.resume()
    }
    
    //MARK: URLSessionDownloadDelegate
    // 1
    func urlSession(_ session: URLSession,
                    downloadTask: URLSessionDownloadTask,
                    didFinishDownloadingTo location: URL){
        
        let fileManager = FileManager()
            
            let pathOfDirectory:String = FCFileManager.pathForDocumentsDirectory()
            let destinationURLForFile = URL(fileURLWithPath: pathOfDirectory.appendingFormat(folderName!+"/"+fileName!+".pdf"))
            
            do {
                
                if fileManager.fileExists(atPath: destinationURLForFile.path) {
                    _ = try fileManager.replaceItemAt(destinationURLForFile, withItemAt: location)
                } else {
                    try fileManager.moveItem(at: location, to: destinationURLForFile)
                }
                
                dismissProgress()
                // show file
                
//                if !isDownload {
                    webView.loadRequest(URLRequest(url: destinationURLForFile))
//                }
                
            }catch{
                dismissProgress()
                print("An error occurred while moving file to destination url")
            }
    }
}

extension PDFViewerView:UIWebViewDelegate {
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        if webView.isLoading {
            // still loading
            return
        }
        dismissProgress()
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        dismissProgress()
    }
}

