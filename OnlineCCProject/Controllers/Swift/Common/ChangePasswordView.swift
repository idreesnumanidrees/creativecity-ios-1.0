//
//  ChangePasswordView.swift
//  OnlineCCProject
//
//  Created by apple on 7/17/18.
//  Copyright © 2018 CheapSender. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class ChangePasswordView: BaseViewController {
    
    @IBOutlet weak var tfOldPassword: FloatLabelTextField!
    
    @IBOutlet weak var tfNewPassword: FloatLabelTextField!
    
    @IBOutlet weak var tfConfirmPassword: FloatLabelTextField!
    
    @IBOutlet weak var updatePassButton: UIButton!
    
    var textFieldArray = [FloatLabelTextField]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        prepareUI()
    }
    
    func prepareUI() {
        
        textFieldArray = [tfOldPassword, tfNewPassword, tfConfirmPassword]

        updatePassButton.isEnabled = false
        updatePassButton.backgroundColor = .gray
        
        showNavigationBar()
    }
    
    //MARK: --IBActions
    @IBAction func updatePasswordTapped(_ sender: Any) {
        
        self.view.endEditing(true)
        
        if !Reachability.isConnectedToNetwork(){
            self.simpleAlertWithMessage(message: "OOPS! Please check your internet connection properly.")
            return
        }
        
        let loggedPass = UtilHelper.getValueFromNSUserDefaultForSignUp(forkey: "password") as? String

        guard loggedPass == tfOldPassword.text else {
            
            self.simpleAlertWithMessage(message: "OOPS! Please enter the correct old password.")
            return
        }
        
        guard tfNewPassword.text == tfConfirmPassword.text else {
            
            self.simpleAlertWithMessage(message: "OOPS! New password and confirm password should same.")
            return
        }
        
        requestToUpdatePassword()
    }
    
    //MARK: --TextField Validations
    func willEnableLoginButton(_ updatedString: String?) {
        
        var isActive:Bool = false
        
        for tfTextField: FloatLabelTextField in textFieldArray {
            
            if (tfTextField.text?.isEmpty)! {
                
                isActive = false
                
            } else {
                
                isActive = true
                if (updatedString?.isEmpty)! {
                    
                    isActive = false
                }
            }
        }
        
        if isActive == false {
            updatePassButton.isEnabled = false
            updatePassButton.backgroundColor = .gray
        } else {
            updatePassButton.isEnabled = true
            updatePassButton.backgroundColor = .BORDER_COLOR
        }
    }
    
    //MARK: --Server handling
    
    func requestToUpdatePassword() {
        
        showProgress(title: "Updating...")

        //Params
        let parameters: Parameters = [
            .userPass:tfNewPassword.text ?? ""]
        
        // Move to a background thread to do some long running work
        DispatchQueue.global(qos: .userInitiated).async {
            
            WebServicesClient.postRequest(parameters: parameters, methodName: .PASSWORD, controller: .Account) { (_ jsonResponse:JSON?) in
                
                // Bounce back to the main thread to update the UI
                DispatchQueue.main.async {
                    
                    self.dismissProgress()
                    
                    guard jsonResponse != nil else {
                        self.simpleAlertWithMessage(message: "OOPS! Something went wrong")
                        return
                    }
                    print(jsonResponse ?? "Dictionary is nil")
                    
                    if jsonResponse?["error"].int == 0 {
                        
                        self.alertWithMessage(message: "Password has been successfully updated!")
                        UtilHelper.saveAndUpdateNSUserDefault(withObject: self.tfNewPassword.text ?? "", forKey: "password")
                    }
                    else
                    {
                        print(jsonResponse ?? "Error")
                        self.simpleAlertWithMessage(message: "OOPS! Error while processing your request.")
                    }
                }
            }
        }
    }
    
    //TouchView
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        self.view.endEditing(true)
    }
}

//Extention For TextField Delegates
extension ChangePasswordView: UITextFieldDelegate {
    
    //TextField Delegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let updatedString = (textField.text as NSString?)?.replacingCharacters(in: range, with: string)
        willEnableLoginButton(updatedString);
        return true
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        
        updatePassButton.isEnabled = false
        updatePassButton.backgroundColor = .gray
        return true
    }
}
