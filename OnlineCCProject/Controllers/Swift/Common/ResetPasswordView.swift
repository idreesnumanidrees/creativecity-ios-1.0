//
//  ResetPasswordView.swift
//  OnlineCCProject
//
//  Created by apple on 7/17/18.
//  Copyright © 2018 CheapSender. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class ResetPasswordView: BaseViewController {
    
    @IBOutlet weak var tfEmail: FloatLabelTextField!
    
    @IBOutlet weak var resetPasswordButton: Button!
    
    var textFieldArray = [FloatLabelTextField]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        prepareUI()
    }
    
    func prepareUI() {
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        
        textFieldArray = [tfEmail]
        willEnableLoginButton("nothing")
    }
    
    //MARK: --IBActions
    @IBAction func resetPasswordTapped(_ sender: Any) {
        
        if UtilHelper.validateEmail(withEmail: tfEmail.text!) {
            
            if !Reachability.isConnectedToNetwork(){
                self.simpleAlertWithMessage(message: "OOPS! Please check your internet connection properly.")
                return
            }
            showProgress(title: "Sending...")
            
            //Params
            let parameters: Parameters = [
                .email:tfEmail.text ?? ""]
            
            // Move to a background thread to do some long running work
            DispatchQueue.global(qos: .userInitiated).async {
                
                WebServicesClient.postRequest(parameters: parameters, methodName: .FORGET, controller: .Index) { (_ jsonResponse:JSON?) in
                    
                    // Bounce back to the main thread to update the UI
                    DispatchQueue.main.async {
                        
                        self.dismissProgress()
                        
                        guard jsonResponse != nil else {
                            self.simpleAlertWithMessage(message: "OOPS! Something went wrong")
                            return
                        }
                        print(jsonResponse ?? "Dictionary is nil")
                        
                        if jsonResponse?["error"].int == 0 {
                            
                            self.alertWithMessage(message: "An email is successfully sent to your email address, please check your email to complete with the reset password process.")
                        }
                        else
                        {
                            print(jsonResponse ?? "Error")
                            self.simpleAlertWithMessage(message: "OOPS! Error while processing your request.")
                        }
                    }
                }
            }
            
        } else {
            
            self.simpleAlertWithMessage(message: "OOPS! Please enter correct email.")
        }
    }
    
    //MARK: --TextField Validations
    func willEnableLoginButton(_ updatedString: String?) {
        
        var isActive:Bool = false
        
        for tfTextField: FloatLabelTextField in textFieldArray {
            
            if (tfTextField.text?.isEmpty)! {
                
                isActive = false
                
            } else {
                
                isActive = true
                if (updatedString?.isEmpty)! {
                    
                    isActive = false
                }
            }
        }
        
        if isActive == false {
            resetPasswordButton.isEnabled = false
            resetPasswordButton.backgroundColor = .gray
        } else {
            resetPasswordButton.isEnabled = true
            resetPasswordButton.backgroundColor = .BORDER_COLOR
        }
    }
    
    //TouchView
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        self.view.endEditing(true)
    }
}

//Extention For TextField Delegates
extension ResetPasswordView: UITextFieldDelegate {
    
    //TextField Delegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let updatedString = (textField.text as NSString?)?.replacingCharacters(in: range, with: string)
        willEnableLoginButton(updatedString);
        return true
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        
        resetPasswordButton.isEnabled = false
        resetPasswordButton.backgroundColor = .gray
        return true
    }
}
