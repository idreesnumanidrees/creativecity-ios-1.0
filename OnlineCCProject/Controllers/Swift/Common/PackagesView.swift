//
//  PackagesView.swift
//  OnlineCCProject
//
//  Created by apple on 11/20/17.
//  Copyright © 2017 CheapSender. All rights reserved.
//

import UIKit
import KRPullLoader
import Alamofire
import SVProgressHUD
import SwiftyJSON

//MARK: step 1 Add Protocol here.
protocol PackagesViewDelegate: class {
    
    func getPackageName(_ name: String?)
}

class PackagesView: BaseViewController {
    
    @IBOutlet weak var packagesTableView: UITableView!
    
    let colorHeadingArray = [UIColor(red: 115/255, green: 162/255, blue: 115/255, alpha: 1.0), UIColor(red: 254/255, green: 66/255, blue: 255/255, alpha: 1.0),UIColor(red: 50/255, green: 198/255, blue: 206/255, alpha: 1.0),UIColor(red: 248/255, green: 201/255, blue: 0/255, alpha: 1.0),UIColor(red: 50/255, green: 198/255, blue: 206/255, alpha: 1.0),UIColor(red: 248/255, green: 201/255, blue: 0/255, alpha: 1.0)]
    let colorBodyArray = [UIColor(red: 221/255, green: 235/255, blue: 222/255, alpha: 1.0), UIColor(red: 223/255, green: 183/255, blue: 223/255, alpha: 1.0), UIColor(red: 181/255, green: 205/255, blue: 205/255, alpha: 1.0), UIColor(red: 249/255, green: 243/255, blue: 207/255, alpha: 1.0), UIColor(red: 181/255, green: 205/255, blue: 205/255, alpha: 1.0),UIColor(red: 249/255, green: 243/255, blue: 207/255, alpha: 1.0)]

    var isComingFromPackages:Bool?

    var packages: [Any] = []
    
    weak var delegate: PackagesViewDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        prepareUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        showNavigationBar()
    }
    
    func prepareUI() {
        
        packagesTableView.rowHeight = UITableViewAutomaticDimension
        packagesTableView.estimatedRowHeight = 140
        
        if AppDelegate.sharedAppDelegate().licOptions == nil {

            requestForPackages()

        }else {

            packages = AppDelegate.sharedAppDelegate().licOptions!["packages"] as! [Any]
        }
    }
}

//MARK: Tableview Data Source and Delegates

extension PackagesView: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return packages.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellIdentifier = "PackegesCell"
        
        guard let tableviewcell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? PackegesCell else {
            
            fatalError("The dequeued cell is not instance of HomeCell.")
        }
        
        tableviewcell.selectButton.addTarget(self, action: #selector(self.selectPackage(_:)) , for: .touchUpInside)
        tableviewcell.selectButton.tag = indexPath.row
        
        let package = packages[indexPath.row] as! Dictionary<String, AnyObject>

        tableviewcell.lblPackageName.text = package["title"] as? String
        
        let attributtedData:String = (package["detail"] as? String)!
        tableviewcell.lblPackageDetails.setHTMLFromString(htmlText: attributtedData)
        
        
        if indexPath.row < 5 {
            
            tableviewcell.lblPackageName.backgroundColor = colorHeadingArray[indexPath.row]
//            tableviewcell.selectButton.backgroundColor = colorHeadingArray[indexPath.row]
            tableviewcell.lblPackageDetails.backgroundColor =  colorBodyArray[indexPath.row]

        }

        if isComingFromPackages! {
        tableviewcell.selectButtonHeightConstraint.constant = 35
        }else{
            tableviewcell.selectButtonHeightConstraint.constant = 0
        }
        
        return tableviewcell
    }
    
    @objc func selectPackage(_ sender:UIButton) {
        
        let package = packages[sender.tag] as! Dictionary<String, AnyObject>

        delegate?.getPackageName(package["title"] as? String)
        navigationController?.popViewController(animated: true)
    }
    
    
    //MARK: Server Handling
    func requestForPackages()  {
        
        if !Reachability.isConnectedToNetwork(){
            self.alertWithMessage(message: "OOPS! Please check your internet connection properly.")
            return
        }
        
        showProgress(title: "Loading...")
        
        DispatchQueue.global(qos: .userInitiated).async {
            
            WebServicesClient.getRequest(methodName: "packages", controller:.Index) { (_ jsonResponse:JSON?) in
                
                self.dismissProgress()
                // Bounce back to the main thread to update the UI
                DispatchQueue.main.async {
                    
                    guard jsonResponse != nil else {
                        return
                    }
                    
                    print(jsonResponse ?? "Dictionary is nil")
                    
                    if jsonResponse?["error"].int == 0 {
                        
                        guard let data = jsonResponse?["response"].arrayObject else {
                            return
                        }
                        
                        self.packages = data
                        self.packagesTableView.reloadData()
                    }
                }
            }
        }
    }
}

extension UILabel {
    
    func setHTMLFromString(htmlText: String) {
        let modifiedFont = NSString(format:"<span style=\"font-family:'SFUIText-Regular'; font-size: \(self.font!.pointSize)\">%@</span>" as NSString, htmlText) as String
        
        //process collection values
        let attrStr = try! NSAttributedString(
            data: modifiedFont.data(using: .unicode, allowLossyConversion: true)!,
            options: [NSAttributedString.DocumentReadingOptionKey.documentType:NSAttributedString.DocumentType.html, NSAttributedString.DocumentReadingOptionKey.characterEncoding: String.Encoding.utf8.rawValue],
            documentAttributes: nil)
        
        self.attributedText = attrStr
    }
}


