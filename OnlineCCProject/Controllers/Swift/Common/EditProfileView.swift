//
//  EditProfileView.swift
//  OnlineCCProject
//
//  Created by apple on 7/17/18.
//  Copyright © 2018 CheapSender. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class EditProfileView: BaseViewController {
    
    @IBOutlet weak var tfFirstName: FloatLabelTextField!
    @IBOutlet weak var tfLastName: FloatLabelTextField!
    @IBOutlet weak var tfDisplayName: FloatLabelTextField!
    @IBOutlet weak var tfEmail: FloatLabelTextField!
    
    var textFieldArray = [FloatLabelTextField]()
    
    @IBOutlet weak var updateProfile: Button!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        prepareUI()
    }
    
    func prepareUI() {
        
        showNavigationBar()
        
        textFieldArray = [tfFirstName, tfLastName, tfDisplayName,tfEmail]
        
        tfFirstName.text = UtilHelper.getValueFromNSUserDefault(forkey: "user_fname") as? String
        tfLastName.text = UtilHelper.getValueFromNSUserDefault(forkey: "user_lname") as? String
        tfDisplayName.text = UtilHelper.getValueFromNSUserDefault(forkey: "displayName") as? String
        tfEmail.text = UtilHelper.getValueFromNSUserDefault(forkey: "user_email") as? String
    }
    
    //MARK: IBActions
    @IBAction func updateProfileTapped(_ sender: Any) {
        
        if !Reachability.isConnectedToNetwork(){
            self.simpleAlertWithMessage(message: "OOPS! Please check your internet connection properly.")
            return
        }
        
        requestForEditProfile()
    }
    
    //MARK: --TextField Validations
    func willEnableLoginButton(_ updatedString: String?) {
        
        var isActive:Bool = false
        
        for tfTextField: FloatLabelTextField in textFieldArray {
            
            if (tfTextField.text?.isEmpty)! {
                
                isActive = false
                
            } else {
                
                isActive = true
                if (updatedString?.isEmpty)! {
                    
                    isActive = false
                }
            }
        }
        
        if isActive == false {
            updateProfile.isEnabled = false
            updateProfile.backgroundColor = .gray
        } else {
            updateProfile.isEnabled = true
            updateProfile.backgroundColor = .BORDER_COLOR
        }
    }
    
    //MARK: --Server Handling
    
    func requestForEditProfile() {
        
        showProgress(title: "Updating...")
        
        //Params
        let parameters: Parameters = [
            .userFirstName:tfFirstName.text ?? "",
            .userLastName:tfLastName.text ?? "",
            .userDisplayName:tfDisplayName.text ?? "",
            .userEmail:tfEmail.text ?? ""]
        
        // Move to a background thread to do some long running work
        DispatchQueue.global(qos: .userInitiated).async {
            
            WebServicesClient.postRequest(parameters: parameters, methodName: .PROFILE, controller: .Account) { (_ jsonResponse:JSON?) in
                
                // Bounce back to the main thread to update the UI
                DispatchQueue.main.async {
                    
                    self.dismissProgress()
                    
                    guard jsonResponse != nil else {
                        self.simpleAlertWithMessage(message: "OOPS! Something went wrong")
                        return
                    }
                    print(jsonResponse ?? "Dictionary is nil")
                    
                    if jsonResponse?["error"].int == 0 {
                        
                        self.alertWithMessage(message: "Profile has been successfully updated!")
                        UtilHelper.saveAndUpdateNSUserDefault(withObject: self.tfDisplayName.text ?? "", forKey: "displayName")
                        UtilHelper.saveAndUpdateNSUserDefault(withObject: self.tfFirstName.text ?? "", forKey: "user_fname")
                        UtilHelper.saveAndUpdateNSUserDefault(withObject: self.tfLastName.text ?? "", forKey: "user_lname")
                        UtilHelper.saveAndUpdateNSUserDefault(withObject: self.tfEmail.text ?? "", forKey: "user_email")
                    }
                    else
                    {
                        print(jsonResponse ?? "Error")
                        self.simpleAlertWithMessage(message: "OOPS! Error while processing your request.")
                    }
                }
            }
        }
    }
    
    //TouchView
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        self.view.endEditing(true)
    }
}

//Extention For TextField Delegates
extension EditProfileView: UITextFieldDelegate {
    
    //TextField Delegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let updatedString = (textField.text as NSString?)?.replacingCharacters(in: range, with: string)
        willEnableLoginButton(updatedString);
        return true
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        
        updateProfile.isEnabled = false
        updateProfile.backgroundColor = .gray
        return true
    }
}
