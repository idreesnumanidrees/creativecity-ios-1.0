//
//  CompanyStatusesView.swift
//  OnlineCCProject
//
//  Created by apple on 5/16/18.
//  Copyright © 2018 CheapSender. All rights reserved.
//

import UIKit
import Alamofire
import SVProgressHUD
import SwiftyJSON

class CompanyStatusesView: BaseViewController {
    
    var companyId:String?
    @IBOutlet weak var tableView: UITableView!
    
    var historyArray: [Any] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        prepareUI()
    }
    
    func prepareUI() {
        
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 140
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        requestForStatusesInfo()
    }
    
    //MARK: Server Request
    func requestForStatusesInfo() {
        
        showProgress(title: "")
        
        let requestUrl = String(format: "%@/%@", "apps", companyId!)
        
        // Move to a background thread to do some long running work
        DispatchQueue.global(qos: .userInitiated).async {
            
            WebServicesClient.getRequest(methodName: requestUrl, controller:.Companies) { (_ jsonResponse:JSON?) in
                
                // Bounce back to the main thread to update the UI
                DispatchQueue.main.async {
                    
                    self.dismissProgress()
                    
                    guard jsonResponse != nil else {
                        return
                    }
                    print(jsonResponse ?? "Dictionary is nil")
                    
                    if jsonResponse?["error"].int == 0 {
                        
                        guard let data = jsonResponse?["response"].arrayObject else {
                            return
                        }
                        
                        self.historyArray = data
                        if self.historyArray.count == 0 {
                            
                            self.alertWithMessage(message: "History not found.")
                            
                        } else {
                            
                            self.tableView.reloadData()
                        }
                    }
                    else
                    {
                        print(jsonResponse?.stringValue ?? "Error")
                        self.alertWithMessage(message: "Something went wrong!")
                    }
                }
            }
        }
    }
}

//MARK: Tableview Data Source and Delegates 
extension CompanyStatusesView: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return historyArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellIdentifier = "CompanyStatusesCell"
        
        guard let tableviewcell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? CompanyStatusesCell else {
            
            fatalError("The dequeued cell is not instance of HomeCell.")
        }
        
        let status = historyArray[indexPath.row] as! Dictionary<String, String>
        
        tableviewcell.lblType.text = status["type"]
        tableviewcell.lblActions.text = status["actions"]
        
        let date = UtilHelper.getDateTimeFromString(stringData: status["created_time"]!)
        let dateSting = UtilHelper.getDateStringFromDate(stringDate: date)
        
        tableviewcell.lblTime.text = String(format: "\(status["time"]!) (\(dateSting))")
        
        //Type Background colors
        if tableviewcell.lblType.text == "Renewal" {
            
            tableviewcell.lblType.backgroundColor = .RENEWAL_COLOR
            
        } else if tableviewcell.lblType.text == "Amendment" {
            
            tableviewcell.lblType.backgroundColor = .AMENDMENTS_COLOR
            
        }  else if tableviewcell.lblType.text == "Renew & Amend" {
            
            tableviewcell.lblType.backgroundColor = .AMENDMENTS_COLOR
            
        } else if tableviewcell.lblType.text == "Correction" {
            
            tableviewcell.lblType.backgroundColor = .PROCESSING_COLOR
            
        } else if tableviewcell.lblType.text == "Blocked" {
            
            tableviewcell.lblType.backgroundColor = .BLOCKED_COLOR
            
        } else if tableviewcell.lblType.text == "Cancel" {
            
            tableviewcell.lblType.backgroundColor = .CANCEL_COLOR
        }
        
        tableviewcell.lblType.sizeToFit()
        
        return tableviewcell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}
