//
//  CompanyNOCView.swift
//  OnlineCCProject
//
//  Created by apple on 3/26/19.
//  Copyright © 2019 CheapSender. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class CompanyNOCView: BaseViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate, UISearchBarDelegate {
    
    var companyId:String?

    var isLoadMore:Bool = false

    @IBOutlet weak var nocTableView: UITableView!
    
    var tableCellheight = 382.0
    
    var NOCSArray : NSMutableArray = []
    
    //MAKR: Search related
    var searchedFiles:Array<Any>?
    var isSearchActive : Bool? = false
    @IBOutlet weak var searchBar: UISearchBar!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        prepareUI()
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
    func prepareUI() {
        
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        
        showProgress(title: "")
        requestForNOC()
    }
    
    //UItableView datasource and delegate
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return 8.0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if isSearchActive == true {
            
            return searchedFiles?.count ?? 0
            
        } else {
            
            return NOCSArray.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellIdentifier = "CompanyNOCCell"
        
        guard let tableviewcell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? CompanyNOCCell else {
            
            fatalError("The dequeued cell is not instance of HomeCell.")
        }
        
        var nocs = NOCSArray[indexPath.row] as! Dictionary<String, AnyObject>
        
        if isSearchActive == true {
            nocs = searchedFiles?[indexPath.row] as! Dictionary<String, AnyObject>
        }
        
        tableCellheight = 382.0

        //If passport is not found
        let passport = nocs["emp_passport"] as? String
        if (passport?.isEmpty)! {
            
            tableCellheight -= 18
        }
        
        //If name is not found
        let name = nocs["emp_name"] as? String
        if (name?.isEmpty)! {
            
            tableCellheight -= 18
        }
        
        tableviewcell.setupForInitial(tag: indexPath.row, NOCObject: nocs)
        return tableviewcell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return CGFloat(tableCellheight)
    }
    
//    func scrollViewDidScroll(_ scrollView: UIScrollView) {
//
//        let offsetY:CGFloat = scrollView.contentOffset.y
//        let contentHeight = scrollView.contentSize.height
//
//        if offsetY > contentHeight - 2000 {
//
//            if isLoadMore {
//
//                isLoadMore = false
//                requestForNOC()
//            }
//        }
//    }
    
    //TextField Delegates
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return true
    }
    
    //SearchBar Delegates
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        
        isSearchActive = false
        nocTableView.reloadData()
        searchBar.text = ""
        searchedFiles?.removeAll()
        searchBar.resignFirstResponder()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
        if (searchBar.text?.isEmpty)! {
            isSearchActive = false
            nocTableView.reloadData()
        }
        searchBar.resignFirstResponder()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        if ((searchBar.text?.count)! > 1) {

            
            let searchPredicate = NSPredicate(format: "title contains[c] %@ OR emp_name contains[c] %@", argumentArray: [searchBar.text!, searchBar.text!])
            
            searchedFiles = (NOCSArray as NSArray).filtered(using: searchPredicate)
            nocTableView.reloadData()
        }
    }
    
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        
        isSearchActive = true
        return true
    }
    
    //MARK: Server Request
    func requestForNOC() {
        
        let methodName = String(format: "/noc/%@", companyId!)
        
        //Params
        let parameters: Parameters = [.offset: NOCSArray.count, .limit: -1]
        
        // Move to a background thread to do some long running work
        DispatchQueue.global(qos: .userInitiated).async {
            
            WebServicesClient.postRequest(parameters: parameters, methodName: methodName, controller: .COMPANIES) { (_ jsonResponse:JSON?) in
                
                // Bounce back to the main thread to update the UI
                DispatchQueue.main.async {
                    
                    self.dismissProgress()
                    
                    guard jsonResponse != nil else {
                        self.simpleAlertWithMessage(message: "OOPS! Something went wrong")
                        return
                    }
                    
                    print(jsonResponse ?? "Dictionary is nil")
                    
                    if jsonResponse?["error"].int == 0 {
                        
                        guard let data = jsonResponse?["response"].arrayObject else {
                            return
                        }
                        
                        if data.count > 0 {
                            
                            self.isLoadMore = true

                            self.NOCSArray.addObjects(from: data)
                            self.nocTableView.reloadData()

                        } else {
                            
                            if self.NOCSArray.count == 0 {
                                
                                self.alertWithMessage(message: "Record not found.")
                            }
                            self.isLoadMore = false
                        }
                    }
                    else
                    {
                        print(jsonResponse?.stringValue ?? "Error")
                        self.alertWithMessage(message: "Something went wrong!")
                    }
                    
                }
            }
        }
    }
    
}
