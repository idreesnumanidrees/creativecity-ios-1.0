//
//  CompanyApplicationsView.swift
//  OnlineCCProject
//
//  Created by apple on 9/17/18.
//  Copyright © 2018 CheapSender. All rights reserved.
//

import UIKit

struct Application {
    var type: String
    var status: String
    var time: String
    var items: [String]
    
    init(type: String, status: String, time: String, items: [String]) {
        self.type = type
        self.status = status
        self.time = time
        self.items = items
    }
}

class CompanyApplicationsView: BaseViewController {
    
    static fileprivate let kTableViewCellReuseIdentifier = "TableViewCellReuseIdentifier"
    @IBOutlet weak var tableView: FZAccordionTableView!
    
    @IBOutlet weak var btbNewApplication: UIButton!
    
    var applicationArray = [Application]()
    
    var companyId:String?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        prepareUI()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    func prepareUI() {
        
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        
        tableView.allowMultipleSectionsOpen = true
        tableView.register(UITableViewCell.classForCoder(), forCellReuseIdentifier: CompanyApplicationsView.kTableViewCellReuseIdentifier)
        tableView.register(UINib(nibName: "AccordionHeaderView", bundle: nil), forHeaderFooterViewReuseIdentifier: AccordionHeaderView.kAccordionHeaderViewReuseIdentifier)
        
        //Dummy data
        applicationArray = [Application(type: "Renew & Amendment", status: "Pending", time: "1 month ago", items: ["Renewal","Renewal & Amendments","Amendments"]),
                            Application(type: "Renewal", status: "Done", time: "6 months ago", items: ["Preview","Documents"]),
                            Application(type: "Amendment", status: "Done", time: "1 year ago", items: ["Preview","Documents"]),
                            Application(type: "Renew & Amendment", status: "Pending", time: "1 month ago", items: ["Renewal","Renewal & Amendments","Amendments"]),
                            Application(type: "Renewal", status: "Done", time: "6 months ago", items: ["Preview","Documents"])]
    }
    
    //MARK: IBActions
    @IBAction func newApplicationTapped(_ sender: Any) {
        
        //Create the AlertController and add Its action like button in Actionsheet
        let actionSheetControllerIOS8: UIAlertController = UIAlertController(title:nil, message: nil, preferredStyle: .actionSheet)
        
        let cancelActionButton = UIAlertAction(title: "Renewal", style: .destructive) { _ in
            print("Renewal")
            AppDelegate.sharedAppDelegate().appController?.loadRenewalCompStep1()
        }
        actionSheetControllerIOS8.addAction(cancelActionButton)
        
        let saveActionButton = UIAlertAction(title: "Renewal & Amendmets", style: .destructive)
        { _ in
            print("Renewal & Amendmets")
        }
        actionSheetControllerIOS8.addAction(saveActionButton)
        
        let deleteActionButton = UIAlertAction(title: "Amendmets", style: .destructive)
        { _ in
            print("Amendmets")
        }
        actionSheetControllerIOS8.addAction(deleteActionButton)

        let cancelledActionButton = UIAlertAction(title: "Cancel", style: .cancel)
        { _ in
            print("Cancel")
        }
        actionSheetControllerIOS8.addAction(cancelledActionButton)
        self.present(actionSheetControllerIOS8, animated: true, completion: nil)
    }
}

//MARK: Tableview Data Source and Delegates
// MARK: - <UITableViewDataSource> / <UITableViewDelegate> -

extension CompanyApplicationsView : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return applicationArray[section].items.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return applicationArray.count;
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50.0;
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return AccordionHeaderView.kDefaultAccordionHeaderViewHeight
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.tableView(tableView, heightForRowAt: indexPath)
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForHeaderInSection section: Int) -> CGFloat {
        return self.tableView(tableView, heightForHeaderInSection:section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CompanyApplicationsView.kTableViewCellReuseIdentifier, for: indexPath)
        cell.textLabel!.text = applicationArray[indexPath.section].items[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: AccordionHeaderView.kAccordionHeaderViewReuseIdentifier) as? AccordionHeaderView
        
        header?.lblType.text = applicationArray[section].type
        header?.lblStatus.text = applicationArray[section].status
        header?.lblTime.text = applicationArray[section].time
        
        return header
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        print(indexPath.row)
    }
}

// MARK: - <FZAccordionTableViewDelegate> -

extension CompanyApplicationsView : FZAccordionTableViewDelegate {
    
    func tableView(_ tableView: FZAccordionTableView, willOpenSection section: Int, withHeader header: UITableViewHeaderFooterView?) {
        
    }
    
    func tableView(_ tableView: FZAccordionTableView, didOpenSection section: Int, withHeader header: UITableViewHeaderFooterView?) {
        
    }
    
    func tableView(_ tableView: FZAccordionTableView, willCloseSection section: Int, withHeader header: UITableViewHeaderFooterView?) {
        
    }
    
    func tableView(_ tableView: FZAccordionTableView, didCloseSection section: Int, withHeader header: UITableViewHeaderFooterView?) {
        
    }
    
    func tableView(_ tableView: FZAccordionTableView, canInteractWithHeaderAtSection section: Int) -> Bool {
        return true
    }
}
