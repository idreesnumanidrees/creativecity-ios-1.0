//
//  CompanyViewLicense.swift
//  OnlineCCProject
//
//  Created by apple on 5/15/18.
//  Copyright © 2018 CheapSender. All rights reserved.
//

import UIKit
import Alamofire
import SVProgressHUD
import SwiftyJSON

class CompanyViewLicense: BaseViewController {
    
    var companyId:String?
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var codeLbl: UILabel!
    
    //MARK: EN Label Outlets
    @IBOutlet weak var licNoEn: UILabel!
    @IBOutlet weak var operatingNameEn: UILabel!
    @IBOutlet weak var ownerEn: UILabel!
    @IBOutlet weak var placeOfIncEn: UILabel!
    @IBOutlet weak var legalStatusEn: UILabel!
    @IBOutlet weak var activityEn: UILabel!
    @IBOutlet weak var addressEn: UILabel!
    @IBOutlet weak var personInchargeEn: UILabel!
    @IBOutlet weak var positionEn: UILabel!
    @IBOutlet weak var estDateEn: UILabel!
    @IBOutlet weak var issueDateEn: UILabel!
    @IBOutlet weak var expiryDateEn: UILabel!
    
    //MARK: Ar Label Outlets
    @IBOutlet weak var licNoAr: UILabel!
    @IBOutlet weak var operatingNameAr: UILabel!
    @IBOutlet weak var ownerAr: UILabel!
    @IBOutlet weak var placeOfIncAr: UILabel!
    @IBOutlet weak var legalStatusAr: UILabel!
    @IBOutlet weak var activityAr: UILabel!
    @IBOutlet weak var addressAr: UILabel!
    @IBOutlet weak var personInchargeAr: UILabel!
    @IBOutlet weak var positionAr: UILabel!
    @IBOutlet weak var estDateAr: UILabel!
    @IBOutlet weak var issueDateAr: UILabel!
    @IBOutlet weak var expiryDateAr: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        prepareUI()
    }
    
    func prepareUI() {
        
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        
        scrollView.isHidden = true
        requestForLicenseInfo()
    }
    
    //MARK: Server Request
    func requestForLicenseInfo() {
        
        showProgress(title: "")
        
        let requestUrl = String(format: "%@/%@", "lic", companyId!)
        
        // Move to a background thread to do some long running work
        DispatchQueue.global(qos: .userInitiated).async {
            
            WebServicesClient.getRequest(methodName: requestUrl, controller:.Companies) { (_ jsonResponse:JSON?) in
                
                // Bounce back to the main thread to update the UI
                DispatchQueue.main.async {
                    
                    self.dismissProgress()
                    
                    guard jsonResponse != nil else {
                        return
                    }
                    print(jsonResponse ?? "Dictionary is nil")
                    
                    if jsonResponse?["error"].int == 0 {
                        
                        guard let data = jsonResponse?["response"].dictionaryObject else {
                            return
                        }
                        
                        self.populatingData(licInfo: data)
                        self.scrollView.isHidden = false
                    }
                    else
                    {
                        print(jsonResponse?.stringValue ?? "Error")
                        self.alertWithMessage(message: "Something went wrong!")
                    }
                }
            }
        }
    }
    
    func populatingData(licInfo:Dictionary<String, Any>) {
        
        codeLbl.text = licInfo["code"] as? String
        
        licNoEn.text = licInfo["license_no"] as? String
        licNoAr.text = licInfo["license_no"] as? String
        
        operatingNameEn.text = licInfo["org_name_en"] as? String
        operatingNameAr.text = licInfo["name_ar"] as? String
        
        ownerEn.text = licInfo["companiesOwners_en"] as? String
        ownerAr.text = licInfo["companiesOwners_ar"] as? String
        
        placeOfIncEn.text = licInfo["place_en"] as? String
        placeOfIncAr.text = licInfo["place_ar"] as? String
        
        legalStatusEn.text = licInfo["legal_status_en"] as? String
        legalStatusAr.text = licInfo["legal_status_ar"] as? String
        
        activityEn.text = licInfo["companiesActivies_en"] as? String
        activityAr.text = licInfo["companiesActivies_ar"] as? String
        
        addressEn.text = licInfo["address_en"] as? String
        addressAr.text = licInfo["address_ar"] as? String
        
        if let perIncharge:Dictionary<String, String> = licInfo["companiesIncharge"] as? Dictionary<String, String> {
            
            personInchargeEn.text = perIncharge["full_name_en"]
            personInchargeAr.text = perIncharge["full_name_ar"]
            
            positionEn.text = perIncharge["position"]
            positionAr.text = perIncharge["position_ar"]
        }
        
        estDateEn.text = licInfo["establish_date"] as? String
        estDateAr.text = licInfo["establish_date"] as? String
        
        issueDateEn.text = licInfo["issue_date"] as? String
        issueDateAr.text = licInfo["issue_date"] as? String
        
        expiryDateEn.text = licInfo["expiry_date"] as? String
        expiryDateAr.text = licInfo["expiry_date"] as? String
    }
}
