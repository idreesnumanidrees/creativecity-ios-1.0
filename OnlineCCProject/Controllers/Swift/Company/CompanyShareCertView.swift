//
//  CompanyShareCertView.swift
//  OnlineCCProject
//
//  Created by apple on 5/16/18.
//  Copyright © 2018 CheapSender. All rights reserved.
//

import UIKit
import Alamofire
import SVProgressHUD
import SwiftyJSON

class CompanyShareCertView: BaseViewController {

    var companyId:String?
    @IBOutlet weak var tableView: UITableView!
    
    var shareCertArray: [Any] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        prepareUI()
    }
    
    func prepareUI() {
        
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 140
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        
        requestForShareCerts()
    }
    
    //MARK: Server Request
    func requestForShareCerts() {
        
        showProgress(title: "")
        
        let requestUrl = String(format: "%@/%@", "certShares", companyId!)
        
        // Move to a background thread to do some long running work
        DispatchQueue.global(qos: .userInitiated).async {
            
            WebServicesClient.getRequest(methodName: requestUrl, controller:.Companies) { (_ jsonResponse:JSON?) in
                
                // Bounce back to the main thread to update the UI
                DispatchQueue.main.async {
                    
                    self.dismissProgress()
                    
                    guard jsonResponse != nil else {
                        return
                    }
                    print(jsonResponse ?? "Dictionary is nil")
                    
                    if jsonResponse?["error"].int == 0 {
                        
                        guard let data = jsonResponse?["response"].arrayObject else {
                            return
                        }
                        
                        self.shareCertArray = data
                        if self.shareCertArray.count == 0 {
                            
                            self.alertWithMessage(message: "There are no shared certificates.")

                        }else{
                            self.tableView.reloadData()
                        }
                    }
                    else
                    {
                        print(jsonResponse?.stringValue ?? "Error")
                        self.alertWithMessage(message: "Something went wrong!")
                    }
                }
            }
        }
    }
}

extension CompanyShareCertView: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return shareCertArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellIdentifier = "CompanyShareCertsCell"
        
        guard let tableviewcell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? CompanyShareCertsCell else {
            
            fatalError("The dequeued cell is not instance of HomeCell.")
        }
        
        tableviewcell.selectionStyle = .none
        
        let status = shareCertArray[indexPath.row] as! Dictionary<String, String>
        
        let date = UtilHelper.getDateTimeFromString(stringData: status["created_time"]!)
        let dateSting = UtilHelper.getDateStringFromDate(stringDate: date)
        
        tableviewcell.dateTimeLbl.text = String(format: "\(status["time"]!) (\(dateSting))")
        tableviewcell.verionLbl.text = String(format: "Version: \(status["version"]!)")
        
        return tableviewcell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let status = shareCertArray[indexPath.row] as! Dictionary<String, String>
        
        let remotePDFDocumentURLPath = status["pdf"]
        if let remotePDFDocumentURL = URL(string: remotePDFDocumentURLPath!), let doc = document(remotePDFDocumentURL) {
            showDocument(doc, navTitle: "Share Certificate")
        } else {
            print("Document named \(String(describing: remotePDFDocumentURLPath)) not found")
        }

    }
}

