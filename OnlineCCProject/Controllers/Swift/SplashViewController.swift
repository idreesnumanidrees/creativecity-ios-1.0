//
//  SplashViewController.swift
//  OnlineCCProject
//
//  Created by Idrees on 2017/10/10.
//  Copyright © 2017 CheapSender. All rights reserved.
//

import UIKit

class SplashViewController: BaseViewController {
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.        
        UIApplication.shared.statusBarStyle = .default
        
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
        if !Reachability.isConnectedToNetwork(){
            self.simpleAlertWithMessage(message: "OOPS! Please check your internet connection properly.")
            return
        }
        
        if AppDelegate.sharedAppDelegate().licOptions == nil {
            
            self.requestForLicOptions()
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
            
            // Dispatch queue main
            AppDelegate.sharedAppDelegate().appController?.loadStartUpView()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
