//
//  GlobalConstant.swift
//  OnlineCCProject
//
//  Created by apple on 1/9/18.
//  Copyright © 2018 CheapSender. All rights reserved.
//

import UIKit

class GlobalConstant: NSObject {}

extension String {
    
    //******************************************** RestClient Constants ***********************************
    //Server URL
    public static let BASE_URL:String = "https://sys.ccfz.ae:4423/api/"
    
    //Controllers
    public static let Index:String = "index/"
    public static let Companies:String = "companies/"
    public static let Account:String = "account/"

    //Api Method Names
    public static let LOGIN:String = "login"
    public static let COMP_NAME_CHECK: String = "compNameCheck"
    public static let COMPANIES: String = "companies"
    public static let VIEW_LICENSE: String = "lic"
    public static let VISA: String = "visa"
    
    //SignUp
    public static let SEARCH_LIC: String = "searchLic"
    public static let CHECK_USER: String = "checkUser"
    
    public static let SIGN_UP: String = "SignUp"
    public static let REQUEST_USER_EMAIL: String = "reqUsername"
    public static let SIGNUP_EMAIL_FOR_ACTIVE: String = "signupEmail"

    //Accounts Methods
    public static let PASSWORD: String = "password"
    public static let PROFILE: String = "profile"
    public static let FORGET: String = "forget"


    //Login Credentials
    public static let username:String = "username"
    public static let password:String = "password"
    public static let deviceToken:String = "deviceToken"
    public static let appVersion:String = "appVersion"
    public static let deviceType:String = "deviceType"

    //Name check parameters
    public static let name:String = "name"

    //Request parameters
    public static let companyName:String = "companyName"
    
    //Request parameters for ChangePassword
    public static let userPass = "user_pass"
    
    //Request parameters for Forgot
    public static let email = "email"
    
    //Request parameters for Search License Number
    public static let licNo = "licno"
    
    //Request parameters for Profile
    public static let userEmail = "user_email"
    public static let userFirstName = "user_fname"
    public static let userLastName = "user_lname"
    public static let userDisplayName = "display_name"
    
    //Request parameters for SignUp
    public static let firstName = "firstName"
    public static let lastName = "lastName"
    public static let userName = "username"
    public static let mobile = "mobile"
    public static let compId = "compId"
    
    //Request parameters for Company NOC
    public static let offset = "offset"
    public static let limit = "limit"

    //******************************************** Response Constants ***********************************
    
    public static let ID = "ID"
    public static let USER_EMAIL = "user_email"
    public static let SESSION_ID = "mrSessId"
    public static let DISPLAY_NAME = "display_name"
    public static let USER_LOGIN = "user_login"
    public static let USER_STATUS = "user_status"
    public static let USER_FIRST_NAME = "user_fname"
    public static let USER_LAST_NAME = "user_lname"

}

//General Constants
extension String {
    
    //Creative City Name
    public static let APP_NAME:String = "Creative City"
    
    //Empty String
    public static let EMPTY_STRING:String = ""
}

extension UIColor {
    
    //App colors
    public static let BORDER_COLOR:UIColor = UIColor(red: 224/255.0, green: 17/255.0, blue: 95/255.0, alpha: 1.0)
    public static let EMPTY_COLOR:UIColor = UIColor(red: 56/255, green: 98/255, blue: 172/255, alpha: 0.0)
    public static let TIMER_BAR_GREEN_COLOR:UIColor = UIColor(red: 10/255, green: 117/255, blue: 16/255, alpha: 1.0)
    public static let TIMER_BAR_RED_COLOR:UIColor = UIColor(red: 255/255, green: 0/255, blue: 0/255, alpha: 1.0)
    
    public static let PROCESSING_COLOR:UIColor = UIColor(red: 91/255, green: 192/255, blue: 222/255, alpha: 1.0)
    public static let ACTIVE_COLOR:UIColor = UIColor(red: 10/255, green: 117/255, blue: 16/255, alpha: 1.0)
    
    public static let RENEWAL_COLOR:UIColor = UIColor(red: 50/255, green: 122/255, blue: 183/255, alpha: 1.0)
    public static let AMENDMENTS_COLOR:UIColor = UIColor(red: 172/255, green: 91/255, blue: 182/255, alpha: 1.0)
    
    public static let BLOCKED_COLOR:UIColor = UIColor(red: 224/255, green: 17/255, blue: 95/255, alpha: 1.0)
    public static let CANCEL_COLOR:UIColor = UIColor(red: 255/255, green: 1/255, blue: 0/255, alpha: 1.0)
    
    public static let PACK_HEADER_1:UIColor = UIColor(red: 115/255, green: 162/255, blue: 115/255, alpha: 1.0)
    public static let PACK_BODY_1:UIColor = UIColor(red: 221/255, green: 235/255, blue: 222/255, alpha: 1.0)
    
    public static let PACK_HEADER_2:UIColor = UIColor(red: 254/255, green: 66/255, blue: 255/255, alpha: 1.0)
    public static let PACK_BODY_2:UIColor = UIColor(red: 223/255, green: 183/255, blue: 223/255, alpha: 1.0)
    
    public static let PACK_HEADER_3:UIColor = UIColor(red: 50/255, green: 198/255, blue: 206/255, alpha: 1.0)
    public static let PACK_BODY_3:UIColor = UIColor(red: 181/255, green: 205/255, blue: 205/255, alpha: 1.0)
    
    public static let PACK_HEADER_4:UIColor = UIColor(red: 248/255, green: 201/255, blue: 0/255, alpha: 1.0)
    public static let PACK_BODY_4:UIColor = UIColor(red: 249/255, green: 243/255, blue: 207/255, alpha: 1.0)
    
    public static let PACK_HEADER_5:UIColor = UIColor(red: 50/255, green: 198/255, blue: 206/255, alpha: 1.0)
    public static let PACK_BODY_5:UIColor = UIColor(red: 181/255, green: 205/255, blue: 205/255, alpha: 1.0)
    
    public static let PACK_HEADER_6:UIColor = UIColor(red: 248/255, green: 201/255, blue: 0/255, alpha: 1.0)
    public static let PACK_BODY_6:UIColor = UIColor(red: 249/255, green: 243/255, blue: 207/255, alpha: 1.0)
    
    
    public static let BUTTON_DISABLE_COLOR:UIColor = UIColor(red: 227/255.0, green: 52/255.0, blue: 110/255.0, alpha: 1.0)



}

extension NSAttributedString {
    
       static func setFontWithSize(_ fontSize: CGFloat, withWebFontText webFontString: UnsafePointer<Int8>) -> NSAttributedString {
        var attributesDictionary = [NSAttributedStringKey: Any]()
        attributesDictionary[NSAttributedStringKey.font] = UIFont(name: "Material-Design-Icons", size: fontSize)
        let attributedString = NSAttributedString(string: String(utf8String: webFontString) ?? "", attributes: attributesDictionary)
        return attributedString
    }
}

//MARK: String Html convert extention Can be used for another screen
extension String {
    func htmlAttributed(family: String?, size: CGFloat, color: UIColor, align: String?) -> NSAttributedString? {
        do {
            let htmlCSSString = "<style>" +
                "html *" +
                "{" +
                "font-size: \(size)pt !important;" +
                "text-align:\(align ?? "right");" +
                "font-family: \(family ?? "Helvetica"), Helvetica !important;" +
            "}</style> \(self)"
            
            guard let data = htmlCSSString.data(using: String.Encoding.utf8) else {
                return nil
            }
            
            return try NSAttributedString(data: data,
                                          options: [.documentType: NSAttributedString.DocumentType.html,
                                                    .characterEncoding: String.Encoding.utf8.rawValue],
                                          
                                          documentAttributes: nil)
        } catch {
            print("error: ", error)
            return nil
        }
    }
}




