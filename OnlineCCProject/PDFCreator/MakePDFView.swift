//
//  MakePDFView.swift
//  FileManagerCustom
//
//  Created by Idrees on 2017/10/17.
//  Copyright © 2017 CheapSender. All rights reserved.
//

import UIKit
import OpalImagePicker
import Photos
import DocumentScanner
import IGRPhotoTweaks

protocol MakePDFViewDelegate {
    
    func reloadFilesView()
}

class MakePDFView: BaseViewController, UIImagePickerControllerDelegate,UINavigationControllerDelegate,UICollectionViewDataSource, UICollectionViewDelegate {
    
    let reuserIdentifier = "ImageCollectionCell"

    var folderName:String?
    var selectedImages:[UIImage] = []
    var pdfViews:[UIView] = []
    
    var delegate:MakePDFViewDelegate?

    @IBOutlet weak var galleryTapped: UIButton!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var alertMessagelbl: UILabel!
    @IBOutlet weak var btnMake: UIButton!
    
    //Constraint outlet
    @IBOutlet weak var createBottomConstraint: NSLayoutConstraint!
    
    let userId = UtilHelper.getValueFromNSUserDefault(forkey: "ID") as! String
    
    var scannedImage: UIImage?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    // tell the collection view how many cells to make
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        alertMessagelbl.isHidden = false
        if selectedImages.count > 0 {
            alertMessagelbl.isHidden = true
        }
        
        return selectedImages.count
    }
    
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        // get a reference to our storyboard cell
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuserIdentifier, for: indexPath as IndexPath) as! ImageCollectionCell
        
        // Use the outlet in our custom class to get a reference to the UILabel in the cell
        
            cell.imageView.image = selectedImages[indexPath.item]
        
        //Delete Target
        //Adds Delete button target
        cell.deleteButton.tag = indexPath.item
        cell.deleteButton.addTarget(self, action: #selector(deleteTapped(_:)), for: .touchUpInside)
        return cell
    }
    
    @objc func deleteTapped(_ sender: UIButton) {

        selectedImages.remove(at: sender.tag)
        pdfViews.remove(at: sender.tag)
        collectionView.reloadData()
        
        if selectedImages.count == 0 {
            createBottomConstraint.constant = -50
            btnMake.isHidden = true
            animationBottomToTop()
        }
    }
    
    //IBActions
    @IBAction func cameraTapped(_ sender: Any) {
        
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
//            let imagePicker = UIImagePickerController()
//            imagePicker.delegate = self
//            imagePicker.sourceType = .camera;
//            imagePicker.allowsEditing = false
//            self.present(imagePicker, animated: true, completion: nil)
            
            let scanner = ScannerViewController()
            scanner.delegate = self
            navigationController?.pushViewController(scanner, animated: true)
        }
    }
    
    @IBAction func galleryTapped(_ sender: Any) {
        
//        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
//            let imagePicker = UIImagePickerController()
//            imagePicker.delegate = self
//            imagePicker.sourceType = .photoLibrary;
//            self.present(imagePicker, animated: true, completion: nil)
//        }
        
        guard UIImagePickerController.isSourceTypeAvailable(.photoLibrary) else {
            //Show error to user?
            return
        }
        
        //Example Instantiating OpalImagePickerController with Closures
        let imagePicker = OpalImagePickerController()
        imagePicker.allowedMediaTypes = Set([PHAssetMediaType.image])
        imagePicker.imagePickerDelegate = self
        
//        imagePicker.navigationBar.barTintColor = .BORDER_COLOR
//        imagePicker.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        
        present(imagePicker, animated: true) {}
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        let image = info[UIImagePickerControllerOriginalImage] as! UIImage
        
        selectedImages.append(image)
        
        let PDFPage = Bundle.main.loadNibNamed(NSStringFromClass(PageOneView.self), owner: self, options: nil)?.last as? PageOneView
        PDFPage?.imageView.image = image
        pdfViews.append(PDFPage!)
        
        createBottomConstraint.constant = 0
        btnMake.isHidden = false
        animationBottomToTop()
        
        collectionView.reloadData()
        dismiss(animated:true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated:true, completion: nil)
    }
    
    @IBAction func makeTapped(_ sender: Any) {
        
        let alertController = UIAlertController(title: "Name Of File", message: "", preferredStyle: UIAlertControllerStyle.alert)
        
        let saveAction = UIAlertAction(title: "Make", style: UIAlertActionStyle.default, handler: {
            alert -> Void in
            
            let firstTextField = alertController.textFields![0] as UITextField
            
            if !(firstTextField.text?.isEmpty)! {
                
                let pathOfDirectory = FCFileManager.pathForDocumentsDirectory()
                var subPath:String = "/creativecity/\(self.userId)/\(self.folderName ?? "Temp")/"
                subPath = subPath.appending(firstTextField.text!)
                
                PDFCreator.generatePDF(withPages: self.pdfViews, path:pathOfDirectory?.appending(subPath), fileName: firstTextField.text!)
                
                self.delegate?.reloadFilesView()
                self.navigationController?.popViewController(animated: true)
            }
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default, handler: {
            (action : UIAlertAction!) -> Void in
        })
        
        alertController.addAction(cancelAction)
        alertController.addAction(saveAction)
        
        alertController.addTextField { (textField : UITextField!) -> Void in
            textField.placeholder = "Enter file name"
            textField.becomeFirstResponder()
        }
     
        self.present(alertController, animated: true, completion: nil)
    }
    
    //View Animation
    func animationBottomToTop() {
        UIView.animate(withDuration: 0.5, delay: 0.2, options: .allowUserInteraction, animations: {() -> Void in
            self.view.layoutIfNeeded()
        }, completion: {(_ finished: Bool) -> Void in
        })
    }
}

extension MakePDFView:OpalImagePickerControllerDelegate {
    
    func imagePicker(_ picker: OpalImagePickerController, didFinishPickingImages images: [UIImage]) {
        
        for image in images {
            
            selectedImages.append(image)
            let PDFPage = Bundle.main.loadNibNamed(NSStringFromClass(PageOneView.self), owner: self, options: nil)?.last as? PageOneView
            PDFPage?.imageView.image = image
            pdfViews.append(PDFPage!)
        }
        
        createBottomConstraint.constant = 0
        btnMake.isHidden = false
        animationBottomToTop()
        collectionView.reloadData()
        dismiss(animated:true, completion: nil)
    }
}

extension MakePDFView: ScannerViewControllerDelegate {
    
    func scanner(_ scanner: ScannerViewController, didCaptureImage image: UIImage) {
        
         navigationController?.popViewController(animated: false)
        
        let cropper:CropViewController = AppDelegate.sharedAppDelegate().appController!.mainStroyBoard?.instantiateViewController(withIdentifier: "CropViewController") as! CropViewController
        cropper.image = image
        cropper.delegate = self
        navigationController?.pushViewController(cropper, animated: true)
    }
}

extension MakePDFView: IGRPhotoTweakViewControllerDelegate {
    
    func photoTweaksController(_ controller: IGRPhotoTweakViewController, didFinishWithCroppedImage croppedImage: UIImage) {
        
        selectedImages.append(croppedImage)
        
        let PDFPage = Bundle.main.loadNibNamed(NSStringFromClass(PageOneView.self), owner: self, options: nil)?.last as? PageOneView
        PDFPage?.imageView.image = croppedImage
        pdfViews.append(PDFPage!)
        
        createBottomConstraint.constant = 0
        btnMake.isHidden = false
        animationBottomToTop()
        collectionView.reloadData()
        navigationController?.popViewController(animated: true)
    }
    
    func photoTweaksControllerDidCancel(_ controller: IGRPhotoTweakViewController) {
        _ = controller.navigationController?.popViewController(animated: true)
    }
}
