//
//  PDFViewController.swift
//  FileManagerCustom
//
//  Created by Idrees on 2017/10/17.
//  Copyright © 2017 CheapSender. All rights reserved.
//

import UIKit

class PDFViewerController: BaseViewController {
    
    var fileName:String?

    @IBOutlet weak var webView: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationController?.setNavigationBarHidden(false, animated: true)

    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        let escapedAddress = fileName?.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)

        let Url = URL.init(string: escapedAddress!)
        
        let request = URLRequest.init(url: Url!)
        webView.loadRequest(request)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func shareTapped(_ sender: Any) {
        
        let url = NSURL.fileURL(withPath: fileName!, isDirectory: true)
        
        let activityViewController: UIActivityViewController = UIActivityViewController(activityItems: [url], applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView=self.view
        present(activityViewController, animated: true, completion: nil)
    }

}
