//
//  FileManager.swift
//  OnlineCCProject
//
//  Created by apple on 6/25/18.
//  Copyright © 2018 CheapSender. All rights reserved.
//

import UIKit

class FileManagerFolders: BaseViewController, UICollectionViewDataSource, UICollectionViewDelegate, UITextFieldDelegate, UISearchBarDelegate {

    let reuserIdentifier = "FolderCollectionCell"
    var folders:Array<Any>?
    var searchedFolders:Array<Any>?
    var isSearchActive : Bool? = false
    
    var folderName:String?
    
    //Outlets
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var addButton: UIButton!
    @IBOutlet weak var editButton: UIButton!
    @IBOutlet weak var lblNoFolderFound: UILabel!
    
    let userId = UtilHelper.getValueFromNSUserDefault(forkey: "ID") as! String
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        prepareUI()
    }
    
    func prepareUI()  {

        searchBar.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        hideNavigationBar()
        getDirectories()
    }
    
    //Helpers
    func getDirectories()
    {
        let pathOfDirectory = FCFileManager.pathForDocumentsDirectory()
        folders = FCFileManager.list(ofDirectoryFolders: pathOfDirectory?.appending("/creativecity/\(userId)/"), deep: false)
        
        collectionView.reloadData()
    }
    
    //IBActions
    @IBAction func addFolder(_ sender: Any) {
        
        let alertController = UIAlertController(title: "Add Folder", message: "", preferredStyle: UIAlertControllerStyle.alert)
        
        let saveAction = UIAlertAction(title: "Create Folder", style: UIAlertActionStyle.default, handler: {
            alert -> Void in
            
            let firstTextField = alertController.textFields![0] as UITextField
            
            let pathOfDirectory = FCFileManager.pathForDocumentsDirectory()
            
            if !(firstTextField.text?.isEmpty)! {
                
                var subPath:String = "/creativecity/\(self.userId)/"
                subPath = subPath.appending(firstTextField.text!)
                FCFileManager.createDirectories(forPath: pathOfDirectory?.appending(subPath))
                self.getDirectories()
            }
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default, handler: {
            (action : UIAlertAction!) -> Void in
        })
        
        alertController.addTextField { (textField : UITextField!) -> Void in
            textField.placeholder = "Enter Folder Name"
            textField.becomeFirstResponder()
        }
        alertController.addAction(cancelAction)
        alertController.addAction(saveAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    @IBAction func editButtonTapped(_ sender: Any) {
        
        editButton.isSelected = !editButton.isSelected
        collectionView.reloadData()
    }
    
    // MARK: - UICollectionViewDataSource protocol
    
    // tell the collection view how many cells to make
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        var count = 0
        
        if isSearchActive == true {
            
            if (searchedFolders?.count) != nil {
                count = searchedFolders!.count
            }
        } else {
            
            if (folders?.count) != nil {
                
                count = folders!.count
            }
        }
        
        if count == 0 {
            
            lblNoFolderFound.isHidden = false
        } else {
            
            lblNoFolderFound.isHidden = true
            
        }
        
        return count
    }
    
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        // get a reference to our storyboard cell
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuserIdentifier, for: indexPath as IndexPath) as! FolderCollectionCell
        
        // Use the outlet in our custom class to get a reference to the UILabel in the cell
        
        var folderName = ""
        if isSearchActive == true {
            folderName = searchedFolders?[indexPath.item] as! String
        } else {
            folderName = folders?[indexPath.item] as! String
        }
        
        let pathOfDirectory = FCFileManager.pathForDocumentsDirectory()
        let files = FCFileManager.list(ofDirectoryFiles: pathOfDirectory?.appending("/creativecity/\(userId)/\(folderName )/"), deep: true)

        cell.folderName.text = String(format: "%@ (%i)", folderName, files?.count ?? 0)
        
        if editButton.isSelected {
            
            cell.folderImageViewLeftConstraint.constant = 40
            cell.deleteButton.isHidden = false
        } else {
            
            cell.folderImageViewLeftConstraint.constant = 5
            cell.deleteButton.isHidden = true
            
        }
        
        //Adds Delete button target
        cell.deleteButton.tag = indexPath.item
        cell.deleteButton.addTarget(self, action: #selector(deleteTapped(_:)), for: .touchUpInside)
        return cell
    }
    
    @objc func deleteTapped(_ sender: UIButton) {
        
        let alertController = UIAlertController(title: "Creative City", message: "Are you sure you want to delete this folder", preferredStyle: UIAlertControllerStyle.alert)
        
        let saveAction = UIAlertAction(title: "Delete", style: UIAlertActionStyle.default, handler: {
            alert -> Void in
            
            var folderName = ""
            if self.isSearchActive == true {
                folderName = self.searchedFolders?[sender.tag] as! String
            } else {
                folderName = self.folders?[sender.tag] as! String
            }
            
            let pathOfDirectory = FCFileManager.pathForDocumentsDirectory()
            FCFileManager.removeItem(atPath: pathOfDirectory?.appending("/creativecity/\(self.userId)/\(folderName)"))
            
            if self.isSearchActive == true {
                self.searchedFolders?.remove(at: sender.tag)
            } else {
                self.folders?.remove(at: sender.tag)
            }
            
            self.getDirectories()
            self.collectionView.reloadData()
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default, handler: {
            (action : UIAlertAction!) -> Void in
        })
        
        alertController.addAction(cancelAction)
        alertController.addAction(saveAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    // MARK: - UICollectionViewDelegate protocol
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        // handle tap events
        print("You selected cell #\(indexPath.item)!")
        if isSearchActive == true {
            self.folderName = searchedFolders?[indexPath.item] as? String
        } else {
            self.folderName = folders?[indexPath.item] as? String
        }
        
        let myVC = storyboard?.instantiateViewController(withIdentifier: "FilesFView") as! FilesFView
        myVC.folderName = folderName
        navigationController?.pushViewController(myVC, animated: true)
    }
    
    //TextField Delegates
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return true
    }
    
    //SearchBar Delegates
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        
        isSearchActive = false
        collectionView.reloadData()
        addButton.isHidden = false
        searchBar.text = ""
        searchedFolders?.removeAll()
        searchBar.resignFirstResponder()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
        if (searchBar.text?.isEmpty)! {
            isSearchActive = false
            collectionView.reloadData()
            addButton.isHidden = false
        }
        searchBar.resignFirstResponder()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        let predicate = NSPredicate(format: "SELF CONTAINS %@", searchBar.text!)
        searchedFolders = (folders! as NSArray).filtered(using: predicate)
        collectionView.reloadData()
    }
    
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        
        addButton.isHidden = true
        isSearchActive = true
        
        return true
    }
}
