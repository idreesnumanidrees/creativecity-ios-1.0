//
//  FilesFView.swift
//  FileManagerCustom
//
//  Created by Idrees on 2017/10/16.
//  Copyright © 2017 CheapSender. All rights reserved.
//

import UIKit

class FilesFView: BaseViewController, UICollectionViewDataSource, UICollectionViewDelegate, UITextFieldDelegate, UISearchBarDelegate, MakePDFViewDelegate {
    
    let reuserIdentifier = "FileCollectionCell"
    var files:Array<Any>?
    var searchedFiles:Array<Any>?
    var isSearchActive : Bool? = false
    
    var folderName:String?
    
    @IBOutlet weak var editButton: UIButton!
    
    //Outlets
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var lblNoFolderFound: UILabel!

    let userId = UtilHelper.getValueFromNSUserDefault(forkey: "ID") as! String

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
       showNavigationBar()
        
        getFiles()
    }
    
    //Helpers
    func getFiles()
    {
        let pathOfDirectory = FCFileManager.pathForDocumentsDirectory()
        
        files = FCFileManager.list(ofDirectoryFiles: pathOfDirectory?.appending("/creativecity/\(userId)/\(folderName ?? "Application")/"), deep: true)
        
        collectionView.reloadData()
    }
    
    //IBActions
    @IBAction func makePDFTapped(_ sender: Any) {
        
        let makePDFVC = storyboard?.instantiateViewController(withIdentifier: "MakePDFView") as! MakePDFView
        makePDFVC.folderName = folderName
        makePDFVC.delegate = self
        navigationController?.pushViewController(makePDFVC, animated: true)
    }
    
    @IBAction func editTapped(_ sender: Any) {
        
        editButton.isSelected = !editButton.isSelected
        
        if editButton.isSelected {
            
            editButton.setTitle(NSLocalizedString("Done", comment: "Localization"), for: .normal)
            
        } else {
            
            editButton.setTitle(NSLocalizedString("Edit", comment: "Localization"), for: .normal)
        }
        
        collectionView.reloadData()
    }
    
    // tell the collection view how many cells to make
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        var count = 0
        
        if isSearchActive == true {
            
            if (searchedFiles?.count) != nil {
            count = searchedFiles!.count
            }
        } else {
            
            if (files?.count) != nil {
                
                count = files!.count
            }
        }
        
        if count == 0 {
            
            lblNoFolderFound.isHidden = false
        } else {
            
            lblNoFolderFound.isHidden = true

        }
        return count
    }
    
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        // get a reference to our storyboard cell
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuserIdentifier, for: indexPath as IndexPath) as! FileCollectionCell
        
        // Use the outlet in our custom class to get a reference to the UILabel in the cell
        var fileName = ""
        if isSearchActive == true {
            fileName = searchedFiles?[indexPath.item] as! String
        } else {
            fileName = files?[indexPath.item] as! String
        }
        
        var pathOfDirectory = FCFileManager.pathForDocumentsDirectory()
        pathOfDirectory = pathOfDirectory?.appending("/creativecity/\(userId)/\(folderName ?? "Application")/")
        pathOfDirectory = pathOfDirectory?.appending(fileName)
        
        cell.fileName.text = fileName
        cell.fileImageView.image = PDFCreator.buildThumbnailImage(pathOfDirectory)
        
        if editButton.isSelected {
            
            cell.imgLeftConstraint.constant = 40
            cell.deleteButton.isHidden = false
        } else {
            cell.imgLeftConstraint.constant = 5
            cell.deleteButton.isHidden = true
        }
        
        //Adds Delete button target
        cell.deleteButton.tag = indexPath.item
        cell.deleteButton.addTarget(self, action: #selector(deleteTapped(_:)), for: .touchUpInside)
        return cell
    }
    
    // MARK: - UICollectionViewDelegate protocol
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        // handle tap events
        print("You selected cell #\(indexPath.item)!")
        
        var pathOfDirectory = FCFileManager.pathForDocumentsDirectory()
        let subPath:String = "/creativecity/\(userId)/\(self.folderName ?? "Temp")/"
        
        var fileName = ""
        if isSearchActive == true {
            fileName = searchedFiles?[indexPath.item] as! String
        } else {
            fileName = files?[indexPath.item] as! String
        }
        
        pathOfDirectory = pathOfDirectory?.appending(subPath)
        pathOfDirectory = pathOfDirectory?.appending(fileName)

        let myVC = storyboard?.instantiateViewController(withIdentifier: "PDFViewerController") as! PDFViewerController
        myVC.fileName = pathOfDirectory
        navigationController?.pushViewController(myVC, animated: true)
    }
    
    @objc func deleteTapped(_ sender: UIButton) {
        
        let alertController = UIAlertController(title: "Creative City", message: "Are you sure you want to delete this file", preferredStyle: UIAlertControllerStyle.alert)
        
        let saveAction = UIAlertAction(title: "Delete", style: UIAlertActionStyle.default, handler: {
            alert -> Void in
            
            var fileName = ""
            if self.isSearchActive == true {
                fileName = self.searchedFiles?[sender.tag] as! String
            } else {
                fileName = self.files?[sender.tag] as! String
            }
            
            var pathOfDirectory = FCFileManager.pathForDocumentsDirectory()
            pathOfDirectory = pathOfDirectory?.appending("/creativecity/\(self.userId)/\(self.folderName ?? "warning")")
            FCFileManager.removeItem(atPath: pathOfDirectory?.appending("/\(fileName)"))
            
            if self.isSearchActive == true {
                self.searchedFiles?.remove(at: sender.tag)
            } else {
                self.files?.remove(at: sender.tag)
            }
            
            self.getFiles()
            self.collectionView.reloadData()
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default, handler: {
            (action : UIAlertAction!) -> Void in
        })
        
        alertController.addAction(cancelAction)
        alertController.addAction(saveAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    //TextField Delegates
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return true
    }
    
    //SearchBar Delegates
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        
        isSearchActive = false
        collectionView.reloadData()
        searchBar.text = ""
        searchedFiles?.removeAll()
        searchBar.resignFirstResponder()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
        if (searchBar.text?.isEmpty)! {
            isSearchActive = false
            collectionView.reloadData()
        }
        searchBar.resignFirstResponder()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        if ((files?.count) != nil) {
           
            let predicate = NSPredicate(format: "SELF CONTAINS %@", searchBar.text!)
            searchedFiles = (files! as NSArray).filtered(using: predicate)
            collectionView.reloadData()
        }
    }
    
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        
        isSearchActive = true
        return true
    }
    
    //Delegate
    func reloadFilesView() {
        
        getFiles()
    }
}
